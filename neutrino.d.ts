import EventEmitter = NodeJS.EventEmitter;
import { ILog } from "./src/log/interface";


interface IServerRunConfiguration {
  http?: {
    port: number;
  };
  https?: {
    port: number;
    privateKey?: string|Buffer;
    passphrase?: string;
    x509: string|Buffer
  };
}

export interface IContext {

  getQueryParam(param: string): string;

  getBodyParam(param: string): string;

  getHeaderParam(param: string): string;

  getCookieParam(param: string): string;

  getRouteParam(param: string): string;
}

interface IApplication {

  run(config: IServerRunConfiguration): IApplication;

  exec(argv: any): IApplication;

  $onInit?(): Promise<any>;

  $onReady?(): Promise<any>;

  $onError?(err: Error|HttpError, context?: IContext): void;

  setLog(log: ILog): IApplication;

  setLogFromFactory(logClass: ILog, factory: any): IApplication;

  getLog(): ILog;
}

export interface HttpError {
  code: number;
  message: string;
}

export interface IApplicationLifecycle {
  $onError?: (err: Error, context?: IContext) =>  void;
  $onInit?(): Promise<any>;
  $onReady?(): Promise<any>;
}

interface IModuleOptions {
  name: string;
  controllers?: Array<Function>;
  providers?: Array<Function>;
  export?: Array<Function|symbol>;
  import?: Array<any>;
  namespacePrefix?: string;
}

export interface IApplicationOptions {
  modules: Array<Function>;
}

export function Module(options: IModuleOptions): Function;

export function Application(options: IApplicationOptions): Function;


export class DefaultApplicationLog {
  /**
   * Log an information level event
   * @param {string} line The line to log
   */
  info(line: string): DefaultApplicationLog;
  /**
   * Log a debug level event
   * @param {string} line The line to log
   */
  debug(line: string): DefaultApplicationLog;
  /**
   * Log an error level event
   * @param {string} line The line to log
   */
  error(line: string): DefaultApplicationLog;
  /**
   * Log a warn level event
   * @param {string} line The line to log
   */
  warn(line: string): DefaultApplicationLog;
}

export function applicationBootstrap<T>(application: T): IApplication;

declare class MetadataStorage {

  /**
   * Store some metadata against the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {any} metadata The metadata to set
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  set(key: string | symbol,
      metadata: any,
      target: FunctionConstructor|Function|symbol,
      targetKey?: string | symbol): void;

  /**
   * Check if the target has any metadata attributed to it
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  has(key: string | symbol,
      target: FunctionConstructor|Function|symbol,
      targetKey?: string | symbol): boolean;

  /**
   * Get any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  get(key: string | symbol,
      target: FunctionConstructor|Function,
      targetKey?: string | symbol): any;
}

export class ServiceManager {

  peeringManager: ServiceManager;

  constructor(metadata: MetadataStorage);


  has(target: any, ignoreDI?: boolean): boolean;

  set(target: any, instance: any): ServiceManager;

  get<T>(target: any, options?: any): T ;

  setPeeringManager(manager: ServiceManager): ServiceManager;

  /**
   * Flag an instance to be shared
   *
   * @param {Function} target The Object to be shared when instantiated
   * @param {boolean} shared The boolean flag indicating the share status of an instance
   * @returns {Manager}
   */
  setShared(target: Function, shared?: boolean): ServiceManager;

  /**
   * Registers a factory for an instance
   * @param {FunctionConstructor} target The target instance prototype
   * @param {IServiceFactory} factory The factory function
   * @returns {Manager}
   */
  registerFactory(target: any, factory: FunctionConstructor|Function): ServiceManager;
}

export interface IFactory {
  createService<T>(serviceLocator: ServiceManager, options?: any): T;
}

export namespace Decorator {
  export namespace Terminal {

    interface Request { }

    interface Response { }

    interface ITerminalMiddleware {
      (request: Request, response: Response, next: (err?: Error) => void): void;
    }

    interface IControllerOptions {
      /**
       * Route path from the controller
       */
      route?: string;
      /**
       * Middleware that acts upon the controller
       */
      middleware?: Array<ITerminalMiddleware|FunctionConstructor>|
        ITerminalMiddleware|FunctionConstructor;
    }

    interface IRouteOptions {
      /**
       * Route path of the action
       */
      route: string|RegExp;
      /**
       * The template string or a Promise resolving to a template string
       */
      template?: string|Promise<string>;
      /**
       * The path of the template on the file system
       */
      templatePath?: string;
      /**
       * Middleware that acts upon the action
       */
      middleware?: Array<any>|any;
    }
    export function Controller(options?: IControllerOptions): Function;

    export function Route(options: IRouteOptions): Function;
  }


  export namespace Http {
    interface IControllerOptions {
      route: string;
      middleware?: Array<any>|any;
    }

    export function Controller(options?: IControllerOptions): Function;

    interface IVerbOptions {
      path?: string;
      middleware?: Array<any>|any;
    }

    export function Get(options?: IVerbOptions): Function;

    export function Put(options?: IVerbOptions): Function;

    export function Patch(options?: IVerbOptions): Function;

    export function Post(options?: IVerbOptions): Function;

    export function Delete(options?: IVerbOptions): Function;

    export function Head(options?: IVerbOptions): Function;

    export function Options(options?: IVerbOptions): Function;

    export function Checkout(options?: IVerbOptions): Function;

    export function Copy(options?: IVerbOptions): Function;

    export function Lock(options?: IVerbOptions): Function;

    export function Merge(options?: IVerbOptions): Function;

    export function Mkactivity(options?: IVerbOptions): Function;

    export function Mkcol(options?: IVerbOptions): Function;

    export function Move(options?: IVerbOptions): Function;

    export function MSearch(options?: IVerbOptions): Function;

    export function Notify(options?: IVerbOptions): Function;

    export function Purge(options?: IVerbOptions): Function;

    export function Report(options?: IVerbOptions): Function;

    export function Search(options?: IVerbOptions): Function;

    export function Subscribe(options?: IVerbOptions): Function;

    export function Trace(options?: IVerbOptions): Function;

    export function Unlock(options?: IVerbOptions): Function;

    export function Unsubscribe(options?: IVerbOptions): Function;
  }

  export namespace Socket {
    interface IControllerOptions {
      namespace: string;
      middleware?: Array<any>|any;
    }

    interface IEventListenerOptions {
      event: string;
      middleware?: Array<any>|any;
    }

    export function Controller(options: IControllerOptions): Function;

    export function EventListener(options: IEventListenerOptions): Function;
  }


  export namespace Argument {

    export function Body(expression: string): Function;

    export function Route(expression: string|number): Function;

    export function Header(expression: string): Function;

    export function Query(expression: string): Function;

    export function Cookie(expression: string): Function;

    export function AddOn(expression: string): Function;

    export function Context(): Function;

    export function Form(target: Function): Function;

    export function Timer(): Function;
  }

  interface IFactoryOptions {
    exported?: boolean;
    shared?: boolean;
  }

  export function Factory(taget: any, options?: IFactoryOptions): Function;

  export function Injectable(options?: IFactoryOptions): Function;

  export enum EInputSource {
    bodyParam,
    queryParam,
    headerParam,
    cookieParam,
    routeParam,
    addOnParam
  }

  interface IInputOptions {
    source: EInputSource;
    options?: any;
    inputName?: string|number;
    validators?: Array<Form.Input.Validator.IValidator|Function>;
    filters?: Array<Form.Input.Filter.IFilter|Function>;
  }

  export function Input(options: IInputOptions): Function;

  export function Form(): Function;
}


export namespace Form {
  export namespace Input {

    export enum EInputSource {
      bodyParam,
      queryParam,
      headerParam,
      cookieParam,
      routeParam,
      addOnParam
    }

    export interface IInput {
      property: string;
      inputName: string;
      isValid(value: any, context?: any): Promise<Response>;
    }

    export class Response {
      public readonly input: IInput;
      public readonly raw: string;
      public readonly clean: any;
      public readonly messages: any;
      public readonly isValid: boolean;
    }

    class Input implements IInput {
      public readonly property;
      public readonly inputName;
      isValid(value: any, context?: any): Promise<Response>;
    }

    export namespace Validator {
      export interface IValidator {
        name: string;
        validate(value: string|any, context: any): Promise<boolean|string>|boolean|string;
      }

      export class NotEmpty implements IValidator {
        name: string;
        validate(value: string, context: Array<any>): Promise<boolean|string>|boolean|string;
      }
    }

    export namespace Filter {
      export interface IFilter {
        filter(value: string|any, context?: any): Promise<any>|any;
      }

      export class StripTags implements IFilter {
        filter(value: string): Promise<any>|any;
      }

      export class TrimString implements IFilter {
        filter(value: string): Promise<any>|any;
      }
    }
  }
}

export namespace Request {
  export interface IContext {

    getQueryParam(param: string): string;

    getBodyParam(param: string): string;

    getHeaderParam(param: string): string;

    getCookieParam(param: string): string;

    getRouteParam(param: string): string;
  }

  export class Terminal implements IContext {

    getQueryParam(param: string): string;

    getBodyParam(param: string): string;

    getHeaderParam(param: string): string;

    getCookieParam(param: string): string;

    getRouteParam(param: string): string;
  }

  export class Http implements IContext {

    getQueryParam(param: string): string;

    getBodyParam(param: string): string;

    getHeaderParam(param: string): string;

    getCookieParam(param: string): string;

    getRouteParam(param: string): string;
  }

  export class Socket implements IContext {

    getQueryParam(param: string): string;

    getBodyParam(param: string): string;

    getHeaderParam(param: string): string;

    getCookieParam(param: string): string;

    getRouteParam(param: string): string;
  }
}

export declare class Timer {
  mark(event: string);
}
