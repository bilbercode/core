import { IFilter } from "./filter";
import { isNullOrUndefined } from "util";
import { Injectable } from "../../../decorator/injectable";

@Injectable()
export class StripTags implements IFilter {

  filter(value: string): Promise<any>|any {
    if (isNullOrUndefined(value)) {
      return value;
    }

    return String(value).replace(/<[^>]+>/gi, "");
  }
}
