

export interface IFilter {
  filter(value: string|any, context?: any): Promise<any>|any;
}
