

export interface IValidator {
  name: string;
  validate(value: string|any, context: any): Promise<boolean|string>|boolean|string;
}
