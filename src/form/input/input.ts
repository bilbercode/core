import { IFilter } from "./filter/filter";
import { IValidator } from "./validator/validator";
import { Response } from "./response";
import { isNullOrUndefined } from "util";
import { CRJSON } from "neutrino-crjson";

export enum EInputSource {
  bodyParam,
  queryParam,
  headerParam,
  cookieParam,
  routeParam,
  addOnParam
};

export interface IInput {
  source: EInputSource;
  property: string;
  inputName: string;
  isValid(value: any, context?: any): Promise<Response>;
}

export class Input implements IInput {


  constructor(public readonly source: EInputSource,
              public readonly property: string,
              public readonly inputName: string,
              private readonly filters: Array<IFilter>,
              private readonly validators: Array<IValidator>) { }


  isValid(value: any, context?: any): Promise<Response> {

    let messages: {[k: string]: string} = {};

    let currentValue = null;

    let response = true;

    return new Promise((resolve, reject) => {
      const VALIDATE = (valueToValidate: any, idx = 0) => {
        if (this.validators[idx]) {

          const VALIDATOR = this.validators[idx];
          return Promise.resolve()
            .then(() => this.validators[idx].validate(valueToValidate, context))
            .then(PASS => {
              if (PASS !== true) {
                 response = false;
                 messages[this.validators[idx].name] = <string>PASS;
              }
              VALIDATE(valueToValidate, idx + 1);
            })
            .catch(reject);
        }
        currentValue = valueToValidate;
        return resolve(new Response(this, value, valueToValidate, messages, response));
      };

      const FILTER = (valueToFilter: any, idx = 0) => {
        if (this.filters[idx]) {
          return Promise.resolve()
            .then(() => this.filters[idx].filter(valueToFilter, context))
            .then(CLEAN => FILTER(CLEAN, idx + 1))
            .catch(reject);
        }

        return VALIDATE(valueToFilter);
      };

      const CLONE = isNullOrUndefined(value) ? null :
        CRJSON.parse(CRJSON.stringify(value));

      FILTER(CLONE);
    });
  }
}
