import { IInput, EInputSource } from "./input/input";
import { Response } from "./input/response";
import { Context as TerminalContext } from "../request/terminal/context";
import { Context as HttpContext } from "../request/http/context";
import { IContext } from "../request/context";




function isTerminalRequest(value: any): value is TerminalContext {
  return !!(value instanceof TerminalContext);
}

function isHttpRequest(value: any): value is HttpContext {
  return !!(value instanceof HttpContext);
}

export class Form {


  public inputs: Array<IInput> = [];
  private boundInstance: any;

  constructor(boundInstance: any) {
    this.boundInstance = boundInstance;
  }


  isValid(requestContext: any): Promise<boolean|any> {

    let inputContext: any = {};

    this.inputs.forEach(INPUT => {
      inputContext[INPUT.property] = this.getContext(
        requestContext,
        INPUT.source,
        INPUT.inputName
      );
    });

    const VALIDATION_CHAIN = this.inputs
      .map(INPUT => INPUT.isValid(
        this.getContext(requestContext, INPUT.source, INPUT.inputName),
        inputContext
      ));

    return Promise.all(VALIDATION_CHAIN)
      .then(RESULTS => {
        let valid = true;
        let messages: any = {};

        RESULTS.forEach((RESULT: Response) => {
          if (!RESULT.isValid) {
            valid = false;
            messages[RESULT.input.inputName] = RESULT.messages;
          }
        });

        if (valid) {
          RESULTS.forEach(RESULT => {
            this.boundInstance[RESULT.input.property] = RESULT.clean;
          });
          return true;
        }

        return messages;
      });
  }

  public getObject<T>(): T {
    return this.boundInstance;
  }

  private getContext(requestContext: IContext, inputContext: EInputSource, inputName: string): any {

    switch (inputContext) {
      case EInputSource.routeParam:
        return requestContext.getRouteParam(inputName);
      case EInputSource.queryParam:
        return requestContext.getQueryParam(inputName);
      case EInputSource.bodyParam:
        return requestContext.getBodyParam(inputName);
      case EInputSource.cookieParam:
        return requestContext.getCookieParam(inputName);
      case EInputSource.addOnParam:
        return requestContext.get(inputName);
      default:
        return requestContext.getHeaderParam(inputName);
    }
  }
}
