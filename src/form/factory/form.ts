

import { IFactory } from "../../service/factory";
import { Form } from "./../form";
import { ServiceManager } from "../../service/manager";
import { MetadataStorage } from "../../metadata/storage";
import { INPUT, FORM_INPUT, DI_INJECTABLE } from "../../metadata/constants";
import { IInput, Input } from "./../input/input";
import { IInputOptions } from "../../decorator/input";


interface FormFactoryOptions {
  model: Function;
}

export class FormFactory implements IFactory {


  createService<T>(serviceLocator: ServiceManager, options?: Function, model?: any): Form {
    if (!model) {
      return null;
    }

    const MODEL_DEPS = (MetadataStorage.has(DI_INJECTABLE, model) ?
      MetadataStorage.get<Array<any>>(DI_INJECTABLE, model) : [])
      .map(DEP => {
        return serviceLocator.get(DEP);
      });

    const BOUND_INSTANCE = new model(...MODEL_DEPS);

    const FORM = new Form(BOUND_INSTANCE);

    const META: MetadataStorage = serviceLocator.get<MetadataStorage>(MetadataStorage);

    const FORM_INPUTS = META.has(FORM_INPUT, model) ?
      META.get<Array<string>>(FORM_INPUT, model) : [];

    FORM_INPUTS
      .forEach(PROP => {
        if (META.has(INPUT, model, PROP)) {
          const OPTIONS = META.get<IInputOptions>(INPUT, model, PROP);

          const FORM_INPUT: IInput = serviceLocator.get<IInput>(Input, {
            source: OPTIONS.source,
            property: PROP,
            inputName: OPTIONS.inputName || PROP,
            options: OPTIONS.options,
            filters: OPTIONS.filters,
            validators: OPTIONS.validators
          });

          FORM.inputs.push(FORM_INPUT);
        }
      });

    return FORM;
  }
}
