import { IFactory } from "../../service/factory";
import { Factory } from "../../decorator/factory";
import { Input, EInputSource } from "../input/input";
import { ServiceManager } from "../../service/manager";
import { IFilter } from "../input/filter/filter";
import { IValidator } from "../input/validator/validator";


interface IInputOptions {
  source: EInputSource;
  property: string;
  options?: any;
  inputName?: string;
  validators?: Array<IValidator|Function>;
  filters?: Array<IFilter|Function>;
}



export class InputFactory implements IFactory {
  createService(serviceLocator: ServiceManager, options: IInputOptions): Input {
    return new Input(
      options.source,
      options.property,
      options.inputName,
      this.locate<IFilter>(serviceLocator, options.filters || []),
      this.locate<IValidator>(serviceLocator, options.validators || [])
    );
  }

  private locate<T>(serviceLocator: ServiceManager, items: Array<any>): Array<T> {
    return items.map(ITEM => {
      if (serviceLocator.has(ITEM)) {
        return serviceLocator.get<T>(ITEM);
      }
      return ITEM;
    });
  }
}
