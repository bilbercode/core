import { MetadataStorage } from "../../metadata/storage";
import {
  CONTROLLER, CONTROLLER_SOCKET, DI_INJECTABLE,
  DESIGN_PARAM_TYPES
} from "../../metadata/constants";
import { isArray } from "util";

interface IControllerOptions {
  namespace: string;
  middleware?: Array<any>|any;
}

export function Controller(options?: IControllerOptions): Function {

  return (target: Function) => {

    if (MetadataStorage.has(CONTROLLER_SOCKET, target)) {
      throw new Error("@Socket can only be declared once per controller");
    }

    const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, target) ?
      MetadataStorage.get(DESIGN_PARAM_TYPES, target) : [];

    if (!options.middleware) {
      options.middleware = [];
    } else if (!isArray(options.middleware)) {
      options.middleware = [options.middleware];
    }

    MetadataStorage.set(CONTROLLER, true, target);
    MetadataStorage.set(CONTROLLER_SOCKET, options || false, target);
    MetadataStorage.set(DI_INJECTABLE, PARAMS, target);

  };
}
