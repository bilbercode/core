import { MetadataStorage } from "../../metadata/storage";
import { CONTROLLER_SOCKET_EVENT } from "../../metadata/constants";

export interface IEventListenerOptions {
  event: string;
  middleware?: Array<any>|any;
}

export function EventListener(options?: IEventListenerOptions): Function {

  return <T>(target: any, method: string|symbol,
             descriptor: TypedPropertyDescriptor<T>): TypedPropertyDescriptor<T> =>  {

    if (!options.middleware) {
      options.middleware = [];
    } else if (!Array.isArray(options.middleware)) {
      options.middleware = [ options.middleware ];
    }

    MetadataStorage.set(CONTROLLER_SOCKET_EVENT, options, target, method);
    return descriptor;
  };
}
