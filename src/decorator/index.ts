import { Injectable as InjectorDecorator } from "./injectable";
import { Factory as FactoryDecorator } from "./factory";
import * as HttpDecorators from "./http";
import * as TerminalDecorators from "./terminal";
import * as SocketDecorators from "./socket";
import { Form as FormDecorator } from "./form";
import * as ArgumentDecorators from "./argument";
import { Input as InputDecorator } from "./input";

/* tslint:disable */
export namespace Decorator {

  export const Injectable: Function = InjectorDecorator;

  export const Factory: Function = FactoryDecorator;

  export const Http: any = HttpDecorators;

  export const Terminal: any = TerminalDecorators;

  export const Socket: any = SocketDecorators;

  export const Form: any = FormDecorator;

  export const Argument: any = ArgumentDecorators;

  export const Input: any = InputDecorator;
}
/* tslint:enable */

