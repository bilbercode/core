import { MetadataStorage } from "../../metadata/storage";
import {
  CONTROLLER_TERMINAL, CONTROLLER, DESIGN_PARAM_TYPES,
  DI_INJECTABLE
} from "../../metadata/constants";
import { IControllerOptions } from "../../dispatcher/terminal/controller";


export function Controller(options?: IControllerOptions): Function {

  return (target: Function) => {

    if (MetadataStorage.has(CONTROLLER_TERMINAL, target)) {
      throw new Error("@Terminal can only be declared once per controller");
    }

    const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, target) ?
      MetadataStorage.get(DESIGN_PARAM_TYPES, target) : [];

    MetadataStorage.set(CONTROLLER, true, target);
    MetadataStorage.set(CONTROLLER_TERMINAL, options || false, target);
    MetadataStorage.set(DI_INJECTABLE, PARAMS, target);
  };
}
