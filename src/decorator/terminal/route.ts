import { MetadataStorage } from "../../metadata/storage";
import { CONTROLLER_TERMINAL_ROUTE } from "../../metadata/constants";
import * as Fs from "fs";

interface IRouteOptions {
  route: string|RegExp;
  template?: string|Promise<string>;
  templatePath?: string;
  middleware?: Array<any>|any;
}

export function Route(options?: IRouteOptions): Function {

  return <T>(target: any, method: string|symbol,
             descriptor: TypedPropertyDescriptor<T>): TypedPropertyDescriptor<T> => {

    if (!options.template && !options.templatePath) {
      throw new Error("@Route() options require either the template or templatePath parameter" +
        " present!");
    }

    if (options.templatePath && !Fs.existsSync(options.templatePath)) {
      throw new Error(`Terminal template ${options.templatePath} ` + "does not exist or is not" +
        " readable");
    }

    MetadataStorage.set(CONTROLLER_TERMINAL_ROUTE, options, target, method);

    return descriptor;
  };
}
