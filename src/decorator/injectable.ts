import { MetadataStorage } from "../metadata/storage";
import { INJECTABLE } from "../metadata/constants";

export interface IInjectableOptions {
  shared?: boolean;
}

export function Injectable(options?: IInjectableOptions): Function {

  return (target) => {

    if (MetadataStorage.has(INJECTABLE, target)) {
      throw new Error("@Injectable can only be declared once per class");
    }

    if (!options) {
      options = {
        shared: false
      };
    }

    MetadataStorage.set(INJECTABLE, options, target);
  };
}
