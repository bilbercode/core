import { MetadataStorage } from "../metadata/storage";
import { FORM } from "../metadata/constants";

export function Form(): Function {

  return (target: any) => {

    if (MetadataStorage.has(FORM, target)) {
      throw new Error("@Form decorator can only be declared once per class");
    }

    MetadataStorage.set(FORM, true, target);
  };
}
