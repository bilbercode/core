import { requestContext } from "./request-context";
import { ARGUMENT_ROUTE } from "../../metadata/constants";

export function Route(expression: string|number): Function {
  return requestContext(ARGUMENT_ROUTE, expression);
}
