import { requestContext } from "./request-context";
import { ARGUMENT_QUERY } from "../../metadata/constants";

export function Query(expression: string): Function {
  return requestContext(ARGUMENT_QUERY, expression);
}
