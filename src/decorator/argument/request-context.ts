import { MetadataStorage } from "../../metadata/storage";
import { ARGUMENT } from "../../metadata/constants";
import { Context } from "../../request/argument/context";

export function requestContext(context: symbol, expression = null): Function {
  return (target: any, method: string, i: number) => {

    const FUNCTION_ARGUMENTS = MetadataStorage.has(ARGUMENT, target, method) ?
      MetadataStorage.get(ARGUMENT, target, method) : [];

    FUNCTION_ARGUMENTS[i] = new Context(context, expression);

    MetadataStorage.set(ARGUMENT, FUNCTION_ARGUMENTS, target, method);
  };
}
