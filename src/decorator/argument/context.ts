import { requestContext } from "./request-context";
import { ARGUMENT_CONTEXT } from "../../metadata/constants";

export function Context(): Function {
  return requestContext(ARGUMENT_CONTEXT);
}
