import { requestContext } from "./request-context";
import { ARGUMENT_ADDON } from "../../metadata/constants";

export function AddOn(expression: string): Function {
  return requestContext(ARGUMENT_ADDON, expression);
}
