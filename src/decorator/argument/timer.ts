import { requestContext } from "./request-context";
import { ARGUMENT_TIMER } from "../../metadata/constants";

export function Timer(): Function {
  return requestContext(ARGUMENT_TIMER);
}
