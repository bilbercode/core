import { requestContext } from "./request-context";
import { ARGUMENT_COOKIE } from "../../metadata/constants";

export function Cookie(expression: string): Function {
  return requestContext(ARGUMENT_COOKIE, expression);
}
