import { requestContext } from "./request-context";
import { ARGUMENT_BODY } from "../../metadata/constants";

export function Body(expression: string): Function {
  return requestContext(ARGUMENT_BODY, expression);
}
