import { MetadataStorage } from "../../metadata/storage";
import { ARGUMENT } from "../../metadata/constants";
import { Form as FormArgument } from "../../request/argument/form";


export function Form(form): Function {

  return (target: any, method: string, i: number) => {

    const FUNCTION_ARGUMENTS = MetadataStorage.has(ARGUMENT, target, method) ?
      MetadataStorage.get(ARGUMENT, target, method) : [];

    FUNCTION_ARGUMENTS[i] = new FormArgument(form);

    MetadataStorage.set(ARGUMENT, FUNCTION_ARGUMENTS, target, method);
  };
}
