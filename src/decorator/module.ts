import "source-map-support/register";
import { ServiceManager } from "../service/manager";
import { MetadataStorage } from "../metadata/storage";
import {
  CONTROLLER, CONTROLLER_TERMINAL, FACTORY, FACTORY_OPTIONS,
  CONTROLLER_HTTP, CONTROLLER_SOCKET, INJECTABLE, DESIGN_PARAM_TYPES, DI_INJECTABLE,
  APPLICATION_MODULE, FORM
} from "../metadata/constants";
import { Router as TerminalRouter } from "../request/terminal/router";
import {
  IControllerDispatcherFactoryOptions
} from "../application/factory/terminal/controller";
import { Controller as TerminalControllerDispatcher } from "./../dispatcher/terminal/controller";
import { Router as HttpRouter } from "../request/http/router";
import { Controller as HttpControllerDispatcher } from "./../dispatcher/http/controller";
import { Controller as SocketControllerDispatcher } from "./../dispatcher/socket/controller";
import { isNullOrUndefined } from "util";
import { WhitelistedServiceManager } from "../service/whitelisted-manager";
import { EventEmitter } from "events";
import * as Util from "util";
import { FormFactory } from "../form/factory/form";

export interface IModuleOptions {
  name: string;
  controllers?: Array<Function>;
  providers?: Array<Function>;
  export?: Array<Function|symbol>;
  import?: Array<any>;
  namespacePrefix?: string;
  __id?: symbol;
}


export function Module(options: IModuleOptions): Function {

  return (target: any) => {

    let exportedServiceLocator: WhitelistedServiceManager =
      new WhitelistedServiceManager(MetadataStorage);

    function ModuleClassDecorator<T extends {new (...args: any[]): {}}>(constructor: T) {

      const NEW_CLASS = class Module extends constructor {

        public name: string = options.name;

        public controllers: Array<Function> = options.controllers;

        public import: Array<Module> = options.import;

        public namespacePrefix: string = options.namespacePrefix;

        public providers: Array<Function> = options.providers;

        static get exportedServiceLocator(): WhitelistedServiceManager {
          return exportedServiceLocator;
        }

        static get export(): Array<Function|symbol> {
          return options.export;
        }

        get export(): Array<Function|symbol> {
          return options.export;
        }

        private set exportedServiceLocator(serviceLocator: WhitelistedServiceManager) {
          exportedServiceLocator = serviceLocator;
        }

        private get exportedServiceLocator(): WhitelistedServiceManager {
          return exportedServiceLocator;
        }

        get httpRouter(): HttpRouter {
          if (isNullOrUndefined(this._httpRouter)) {
            this._httpRouter = this.serviceLocator.get<HttpRouter>(HttpRouter);
          }

          return this._httpRouter;
        }

        public socketServer: SocketIO.Server;

        private _httpRouter: HttpRouter;

        private serviceLocator: ServiceManager;

        private meta: MetadataStorage;

        constructor(...args: any[]) {
          super(...args.slice(3));
          this.serviceLocator = (<ServiceManager>args[0]);
          this.meta = (<MetadataStorage>args[1]);
          this.socketServer = (<SocketIO.Server>args[2]);
          this.exportedServiceLocator.setWhitelist(this.export);
          this.exportedServiceLocator.setPeeringManager(this.serviceLocator);
        }

        init(terminalOnly = false): Promise<Module> {

          if (this.controllers) {
            this.controllers
              .forEach(COMPONENT => {
                /**
                 * Register controllers with their respective dispatcher
                 */
                if (this.meta.has(CONTROLLER, COMPONENT)) {

                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];

                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);

                  if (this.meta.has(CONTROLLER_TERMINAL, COMPONENT)) {

                    this.serviceLocator.setShared(COMPONENT, true);

                    // Register a Terminal controller
                    const ROUTER = this.serviceLocator.get<TerminalRouter>(TerminalRouter);

                    const FACTORY_OPTIONS: IControllerDispatcherFactoryOptions = {
                      dispatchable: COMPONENT,
                      confidence: 0,
                      controllerOptions: this.meta.get(CONTROLLER_TERMINAL, COMPONENT)
                    };

                    if (FACTORY_OPTIONS.controllerOptions &&
                      FACTORY_OPTIONS.controllerOptions.route) {
                      FACTORY_OPTIONS.confidence += 1;
                    }

                    if (this.namespacePrefix) {
                      FACTORY_OPTIONS.confidence++;
                      if (FACTORY_OPTIONS.controllerOptions.route) {
                        FACTORY_OPTIONS.controllerOptions.route =
                          `${this.namespacePrefix} ${FACTORY_OPTIONS.controllerOptions.route}`;
                      } else if (this.namespacePrefix) {
                        FACTORY_OPTIONS.controllerOptions = {
                          route: this.namespacePrefix
                        };
                      }
                    }

                    const DISPATCHER = this.serviceLocator
                      .get<TerminalControllerDispatcher>(
                        TerminalControllerDispatcher, FACTORY_OPTIONS);

                    DISPATCHER.on("error", (msg) => {
                      (<any>this).emit("error", msg);
                    });

                    ROUTER.registerDispatcher(DISPATCHER);
                  }

                  if (this.meta.has(CONTROLLER_HTTP, COMPONENT) && !terminalOnly) {

                    this.serviceLocator.setShared(COMPONENT, true);


                    const CONTROLLER = this.serviceLocator
                      .get<HttpControllerDispatcher>(HttpControllerDispatcher, {
                        dispatchable: COMPONENT
                      });

                    CONTROLLER.on("error", (msg) => {
                      (<any>this).emit("error", msg);
                    });

                    if (this.namespacePrefix) {
                      this.httpRouter.use(this.namespacePrefix, CONTROLLER.router);
                    } else {
                      this.httpRouter.use(CONTROLLER.router);
                    }
                  }

                  if (this.meta.has(CONTROLLER_SOCKET, COMPONENT)) {
                    const CONTROLLER = this.serviceLocator
                      .get<SocketControllerDispatcher>(SocketControllerDispatcher, {
                        controller: COMPONENT,
                        server: this.socketServer
                      });
                    this.serviceLocator.setShared(COMPONENT, true);

                    CONTROLLER.on("error", (msg) => {
                      (<any>this).emit("error", msg);
                    });
                  }
                }
              });
          }
          return Promise.resolve(this);
        }

        initProviders(): Promise<Module> {

          if (this.import) {
            this.import.forEach(MODULE => {
              this.serviceLocator.addPeeringManager(MODULE.exportedServiceLocator);
            });
          }


          if (this.providers) {
            this.providers
              .forEach(COMPONENT => {
                if (this.meta.has(FORM, COMPONENT)) {
                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];

                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);
                  this.serviceLocator.registerFactory(COMPONENT, FormFactory);
                }

                if (this.meta.has(FACTORY, COMPONENT)) {

                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];

                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);

                  const FACTORY_CONFIG = this.meta.has(FACTORY_OPTIONS, COMPONENT) ?
                    this.meta.get<any>(FACTORY_OPTIONS, COMPONENT) : {
                      shared: false,
                      exported: false
                    };

                  const SERVICE_LOCATOR = this.serviceLocator;

                  SERVICE_LOCATOR
                    .registerFactory(this.meta.get(FACTORY, COMPONENT), COMPONENT);

                  SERVICE_LOCATOR.setShared(
                    this.meta.get<any>(FACTORY, COMPONENT), FACTORY_CONFIG.shared || false);
                }

                if (this.meta.has(INJECTABLE, COMPONENT)) {

                  const INJ_OPT = this.meta.get<{ shared: boolean }>(INJECTABLE, COMPONENT);
                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];
                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);

                  if (INJ_OPT.shared) {
                    this.serviceLocator.setShared(COMPONENT, true);
                  }
                }
              });
          }
          return Promise.resolve(this);
        }
      };


      MetadataStorage.set(INJECTABLE, options, target);

      const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, target) ?
        MetadataStorage.get<Array<any>>(DESIGN_PARAM_TYPES, target) : [];

      PARAMS.unshift("SocketIO.Server - Placeholder");
      PARAMS.unshift(MetadataStorage);
      PARAMS.unshift(ServiceManager);

      MetadataStorage.set(DI_INJECTABLE, PARAMS, target);
      MetadataStorage.set(APPLICATION_MODULE, target, NEW_CLASS);

      return NEW_CLASS;
    }

    function decorateEventEmitter<T extends {new (...args: any[]): {}}>(constructor: T) {

      const ORIGIN = constructor.constructor;

      constructor.constructor = function (...args: any[]) {
        ORIGIN.apply(this, args);
        EventEmitter.call(this);
      };

      Util.inherits(constructor, EventEmitter);

      return constructor;
    }

    return ModuleClassDecorator(decorateEventEmitter(target));
  };
}

