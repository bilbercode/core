import { MetadataStorage } from "../metadata/storage";
import { INPUT, FORM_INPUT } from "../metadata/constants";
import { IInput } from "../form/input/input";
import { isFunction, isNullOrUndefined } from "util";
import { EInputSource } from "../form/input/input";
import { IFilter } from "../form/input/filter/filter";
import { IValidator } from "../form/input/validator/validator";

function isInput(value: any): value is IInput {
  if (isNullOrUndefined(value)) return false;
  return isFunction(value);
}

export interface IInputOptions {
  source: EInputSource;
  options?: any;
  inputName?: string|number;
  validators?: Array<IValidator|Function>;
  filters?: Array<IFilter|Function>;
};

export function Input(options: IInputOptions): Function {

  return <T>(target: any, propertyKey: string,
             descriptor: TypedPropertyDescriptor<T>): TypedPropertyDescriptor<T> => {

    if (MetadataStorage.has(INPUT, target, propertyKey)) {
      throw new Error("@Input() decorator can only be declared once per property");
    }

    MetadataStorage.set(INPUT, options, target, propertyKey);

    const FORM_INPUTS = MetadataStorage.has(FORM_INPUT, target) ?
      MetadataStorage.get<Array<any>>(FORM_INPUT, target) : [];

    FORM_INPUTS.push(propertyKey);

    MetadataStorage.set(FORM_INPUT, FORM_INPUTS, target);


    return descriptor;
  };
}
