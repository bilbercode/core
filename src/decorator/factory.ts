import { MetadataStorage } from "../metadata/storage";
import { FACTORY, FACTORY_OPTIONS } from "../metadata/constants";

export interface IFactoryOptions {
  exported?: boolean;
  shared?: boolean;
}

export function Factory(target: any, options?: IFactoryOptions): Function {

  return (factory: any) => {

    if (("createService" in factory.prototype) === false) {
      throw new Error("@Factory decorator can only be declared on a class that implements a" +
        " createService method!");
    }


    MetadataStorage.set(FACTORY, target, factory);

    if (options) {
      MetadataStorage.set(FACTORY_OPTIONS, options, factory);
    }

  };
}
