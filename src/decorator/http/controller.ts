import { MetadataStorage } from "../../metadata/storage";
import {
  CONTROLLER, CONTROLLER_HTTP, DI_INJECTABLE,
  DESIGN_PARAM_TYPES
} from "../../metadata/constants";
import { IControllerOptions } from "../../dispatcher/http/controller";


export function Controller(options?: IControllerOptions): Function {

  return (target: Function) => {

    if (MetadataStorage.has(CONTROLLER_HTTP, target)) {
      throw new Error("@Http can only be declared once per controller");
    }

    const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, target) ?
      MetadataStorage.get(DESIGN_PARAM_TYPES, target) : [];

    MetadataStorage.set(CONTROLLER, true, target);
    MetadataStorage.set(CONTROLLER_HTTP, options || false, target);
    MetadataStorage.set(DI_INJECTABLE, PARAMS, target);

  };
}
