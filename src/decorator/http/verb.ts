import { CONTROLLER_HTTP_VERB } from "../../metadata/constants";
import { MetadataStorage } from "../../metadata/storage";
import { isNullOrUndefined } from "util";

export interface IVerbOptions {
  path?: string;
  verb?: string;
  middleware?: Array<any>|any;
}

function getMethodDecorator(method: string, options?: IVerbOptions): Function {

  return <T>(target: FunctionConstructor,
             methodName: string|symbol,
             descriptor: TypedPropertyDescriptor<T>): TypedPropertyDescriptor<T>  => {

    options = isNullOrUndefined(options) ? {} : options;

    options.verb = method;
    MetadataStorage.set(CONTROLLER_HTTP_VERB, options, target, methodName);

    return descriptor;
  };
}

export function Get(options?: IVerbOptions): Function {
  return getMethodDecorator("get", options);
}


export function Put(options?: IVerbOptions): Function {
  return getMethodDecorator("put", options);
}

export function Patch(options?: IVerbOptions): Function {
  return getMethodDecorator("patch", options);
}

export function Post(options?: IVerbOptions): Function {
  return getMethodDecorator("post", options);
}

export function Delete(options?: IVerbOptions): Function {
  return getMethodDecorator("delete", options);
}

export function Head(options?: IVerbOptions): Function {
  return getMethodDecorator("head", options);
}

export function Options(options?: IVerbOptions): Function {
  return getMethodDecorator("options", options);
}

export function Checkout(options?: IVerbOptions): Function {
  return getMethodDecorator("checkout", options);
}

export function Copy(options?: IVerbOptions): Function {
  return getMethodDecorator("copy", options);
}

export function Lock(options?: IVerbOptions): Function {
  return getMethodDecorator("lock", options);
}

export function Merge(options?: IVerbOptions): Function {
  return getMethodDecorator("merge", options);
}

export function Mkactivity(options?: IVerbOptions): Function {
  return getMethodDecorator("mkactivity", options);
}

export function Mkcol(options?: IVerbOptions): Function {
  return getMethodDecorator("mkcol", options);
}

export function Move(options?: IVerbOptions): Function {
  return getMethodDecorator("move", options);
}

export function MSearch(options?: IVerbOptions): Function {
  return getMethodDecorator("m-search", options);
}

export function Notify(options?: IVerbOptions): Function {
  return getMethodDecorator("notify", options);
}

export function Purge(options?: IVerbOptions): Function {
  return getMethodDecorator("purge", options);
}

export function Report(options?: IVerbOptions): Function {
  return getMethodDecorator("report", options);
}

export function Search(options?: IVerbOptions): Function {
  return getMethodDecorator("search", options);
}

export function Subscribe(options?: IVerbOptions): Function {
  return getMethodDecorator("subscribe", options);
}

export function Trace(options?: IVerbOptions): Function {
  return getMethodDecorator("trace", options);
}

export function Unlock(options?: IVerbOptions): Function {
  return getMethodDecorator("unlock", options);
}

export function Unsubscribe(options?: IVerbOptions): Function {
  return getMethodDecorator("unsubscribe", options);
}
