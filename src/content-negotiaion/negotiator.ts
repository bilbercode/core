import { Request, Response, NextFunction } from "express";
import { Media } from "./accept/media";
import { Charset } from "./accept/charset";
import { Language } from "./accept/language";
import { Quality } from "./accept/interface/quality";
import { Specificity } from "./accept/interface/specificity";
import { Interface as MediaTypeInterface } from "./media-type/interface";
import { Handler } from "./handler";
import { Translator } from "../i18n/translator";
import * as iconv from "iconv-lite";
import { Json } from "./media-type/json";
import { Javascript } from "./media-type/javascript";
import { I18n } from "./i18n";
import { Injectable } from "../decorator/injectable";
import { Error as HttpError} from "../request/http/error";

/**
 * Content Negotiator [engine]
 *
 * Handles content-negotiation on requests and responses to and
 * from the server respectivly.
 */

@Injectable()
export class Negotiator {
  /**
   * The Default Language that will be used by the server.
   */
  public static DEFAULT_LANGUAGE = "en";
  /**
   * The default charset that will be used by the server
   */
  public static DEFAULT_CHARSET = "utf-8";
  /**
   * The default media-types that will be used by the server to perform
   * content-negotiation on the Accept header.
   *
   * Defaults;
   * * JSON
   * * Javascript (JSONP)
   *
   */
  public static DEFAULT_MEDIA = [
    new Json(),
    new Javascript()
  ];
  /**
   * A list of user defined media types supported by the server
   */
  private media: Array<MediaTypeInterface>;

  /**
   * Supported media by the server
   */
  get supportedMedia(): Array<MediaTypeInterface> {
    if (this.media) {
      return this.media;
    }

    return Negotiator.DEFAULT_MEDIA;
  }

  /**
   * Supported languages on the server
   */
  get supportedLanguages(): Array<string> {
    if (this.translator) {
      return this.translator.languages;
    }

    return [Negotiator.DEFAULT_LANGUAGE];
  }

  /**
   * Translator that will be used to translate responses
   */
  private translator: Translator;

  constructor() {}

  /**
   * Validates the incoming request and adds the Hander instance to the
   * response object.
   *
   * If the content-negotiator is unable to handle a response correctly this
   * middleware will interrupt the request and automatically return a 406
   * response.
   *
   * Unlike expressjs default behaviour, this does the content-negotiation
   * pre-route dispatch and not post process. This way if the client receives
   * a 406 it can be sure that no action has already been taken by the
   * server.
   */
  validate(request: Request, response: Response, next: NextFunction): void {


    /**
     * Filter and sort the Accept header until were
     * left with only supported media types in the
     * correct order.
     */
    const ACCEPT_MEDIA = (request.get("accept") || "*/*")
      .split(",")
      .map(MIME => {
        return new Media(MIME.trim());
      })
      .sort(this.sortPreferred.bind(this))
      .sort(this.sortSpecificity.bind(this))
      .sort(this.sortQuality.bind(this))
      .filter(this.filterMedia.bind(this));

    /**
     * As above by with charsets
     */
    const ACCEPT_CHARSET = (request.get("accept-charset") || "utf-8")
      .split(",")
      .map(META => {
        return new Charset(META.trim());
      })
      .sort(this.sortQuality.bind(this))
      .filter(this.filterCharset.bind(this));

    /**
     * As above but with languages
     */
    const ACCEPT_LANGUAGE = (request.get("accept-language") || Negotiator.DEFAULT_LANGUAGE)
      .split(",")
      .map(META => {
        return new Language(META.trim());
      })
      .sort(this.sortSpecificity.bind(this))
      .sort(this.sortQuality.bind(this))
      .filter(this.filterLanguage.bind(this));


    if (ACCEPT_MEDIA.length === 0 ||
      ACCEPT_CHARSET.length === 0 ||
      ACCEPT_LANGUAGE.length === 0) {
      /**
       * Were not able to support something specified by the
       * client, let them know with a big fat 406 ;)
       */
      return next(new HttpError("Not Acceptable", 406));
    }

    // The first will now be the best match
    const ACCEPT = ACCEPT_MEDIA.shift();

    function getMediaType(media: Array<MediaTypeInterface>): MediaTypeInterface {
      for (let i = 0; i < media.length; i++) {
        if (media[i].match(ACCEPT.toString(), ACCEPT.params)) {
          return media[i];
        }
      }
    }
    // Get the corresponding media-type with the accept
    let MEDIA_TYPE = getMediaType(this.supportedMedia);


    /**
     * Extract or matched language
     */
    let LANGUAGE;
    if (ACCEPT_LANGUAGE[0].internationalization === "*") {
      LANGUAGE = Negotiator.DEFAULT_LANGUAGE;
    } else {
      LANGUAGE = ACCEPT_LANGUAGE[0].toString();
    }
    /**
     * Add the translator to the response object
     */
    response["i18n"] = new I18n(this.translator, LANGUAGE);

    /**
     * Match the charset
     */
    let CHARSET = ACCEPT_CHARSET[0].charset === "*" ?
      Negotiator.DEFAULT_CHARSET : ACCEPT_CHARSET[0].charset;

    /**
     * Add the response handler instance to the response. This way
     * we can simply call send later and the rest is taken care of.
     */
    response["contentNegotiation"] = new Handler(
      ACCEPT,
      MEDIA_TYPE,
      CHARSET
    );

    // Allow the request to progress to other middleware and the controller action
    return next();

  }

  /**
   * Gives priority to the CN's first choice of media-type
   */
  private sortPreferred(item1, item2): number {
    const DEFAULT = this.supportedMedia[0];

    let match1 = DEFAULT.match(item1.toString());
    let match2 = DEFAULT.match(item2.toString());

    if (match1 === match2) {
      return 0;
    }

    return match1 === true ? -1 : 1;
  }

  /**
   * Sorts an accept header by it specificity
   */
  private sortSpecificity(item1: Specificity, item2: Specificity): number {
    if (item1.weight === item2.weight) {
      return 0;
    }
    return item1.weight > item2.weight ? -1 : 1;
  }

  private sortQuality(item1: Quality, item2: Quality): number {
    if (item1.quality === item2.quality) {
      return 0;
    }

    return item1.quality > item2.quality ? -1 : 1;
  }

  /**
   * Removes media types requested by the client that is not supported by
   * the server
   */
  private filterMedia(media: Media): boolean {

    const S_MEDIA = this.supportedMedia;
    let MIME = `${media.type}/${media.subtype}`;

    for (let i = 0; i < S_MEDIA.length; i++) {
      if (S_MEDIA[i].match(MIME, media.params)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Removes charset encoding that were not capable of responding with
   */
  private filterCharset(charset: Charset, idx: number, list: Array<Charset>): boolean {

    if (charset.charset === "utf-8") {
      /**
       * ICONV-LITE does not have utf8
       */
      return true;
    }

    if (iconv.encodingExists(charset.charset) || charset.charset === "*") {
      return true;
    }
    return false;
  }

  /**
   * Removes unsupported languages.
   */
  private filterLanguage(language: Language, idx: number, list: Array<Language>): boolean {

    const I18N_L10N = this.supportedLanguages;

    for (let i = 0; i < I18N_L10N.length; i++) {
      let parts = I18N_L10N[i].split("-");

      if (language.internationalization === parts[0] && !(parts[1])) {
        return true;
      } else if (language.internationalization === parts[0] &&
        language.localization === parts[1]) {
        return true;
      }
    }

    return language.internationalization === "*" ? true : false;
  }
}
