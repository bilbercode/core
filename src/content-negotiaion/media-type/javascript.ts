import { Interface } from "./interface";
import { Request } from "express";
/**
 * Javascript
 *
 * The JSONP media-type used to respond to JSONP requests. This
 * is not a recommended way of interacting with the API as it can
 * introduce security concerns and completely circumvents any CORS policy
 * you have setup
 */
export class Javascript implements Interface {

  readonly MIME: string = "application/javascript";

  match(mediaType: string, params?: any): boolean {

    switch (mediaType) {
      case "*/*":
      case "application/*":
      case "application/javascript":
        return true;
      default:
        return false;
    }
  }

  format(content: any, request: Request): string {

    let body = JSON.stringify(content, null, 2);

    const callback = (request.query.callback || "callback")
      .replace(/[^\[\]\w$.]/g, "");

    // replace chars not allowed in JavaScript that are in JSON
    body = body
      .replace(/\u2028/g, "\\u2028")
      .replace(/\u2029/g, "\\u2029");

    // the /**/ is a specific security mitigation for "Rosetta Flash JSONP abuse"
    // the typeof check is just to reduce client error noise
    return `/**/ typeof ${callback} === 'function' &&  ${callback}(${body});`;
  }
}
