import { Interface } from "./interface";
import { Request } from "express";
import { CRJSON } from "neutrino-crjson";
/**
 * JSON
 *
 * Json media type, used to send JSON responses to the
 * client. This handler will pretty format the response
 * automatically so its easier to read. The readbility
 * gained far outstrips the size of the packet ( which
 * is nominal when compression is enabled)
 */
export class Json implements Interface {


  readonly MIME: string = "application/json";

  match(mediaType: string, params?: any): boolean {

    switch (mediaType) {
      case "*/*":
      case "application/*":
      case "application/json":
        return true;
      default:
        return false;
    }
  }

  format(content: any, request: Request): string {

    return CRJSON.stringify(content, null, 2);
  }
}
