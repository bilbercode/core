import  { Request } from "express";

/**
 * Media Type Interface
 *
 * A common interface that is expected of a media-type handler that
 * is used be the content-negotiation engine
 */
export interface Interface {
  /**
   * The MIME without any params or extensions
   */
  MIME: string;
  /**
   * Matches a MIME string and params from the client. If the client
   * MIME matches the media-type TRUE is returned. If the MIME does
   * not match FALSE is returned and you should not use it to respond.
   */
  match(mediaType: string, params?: any): boolean;
  /**
   * Format a response into a Buffer or string that will be returned to
   * the client.
   */
  format(content: any, request: Request, params?: any): string|Buffer;
}
