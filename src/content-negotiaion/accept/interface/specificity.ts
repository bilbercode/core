/**
 * Specificity Interface
 *
 * Provides the content-negotiation engine with
 * a weight rating on how specified the content-type
 * is. Directives with grater specificity have higher
 * priority
 */
export interface Specificity {
  /**
   * The specificity weight of the directive
   */
  readonly weight: number;
}
