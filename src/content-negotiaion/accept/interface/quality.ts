/**
 * Quality interface
 *
 * Used to provide a quality rating to the
 * content-negotiation engine
 */
export interface Quality {
  /**
   * The quality of the response as defined
   * by the client
   */
  readonly quality: number;
}
