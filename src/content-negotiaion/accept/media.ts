import { Quality } from "./interface/quality";
import { Specificity } from "./interface/specificity";
/**
 * Media
 *
 * Represents a Accept-Language directive supplied by the
 * client
 */
export class Media implements Quality, Specificity {
  /**
   * The Media type [category]
   */
  public readonly type: string;
  /**
   * The media subtype
   */
  public readonly subtype: string;
  /**
   * The quality of the response
   */
  public readonly quality: number;
  /**
   * A set of parameters that can be included alongside the MIME
   * as defined when the MIME was registered with IANA
   */
  public readonly params: any = {};
  /**
   * The specificity of the media type
   */
  public readonly weight: number;

  constructor(MIME: string) {

    let WEIGHT = 0;
    let parts = MIME.split(";")
      .map(p => {
        return p.trim();
      });

    const MEDIA_RANGE = parts
      .shift()
      .split("/")
      .map(p => {
        if (p !== "*") {
          WEIGHT++;
        }
        return p;
      });

    this.type = MEDIA_RANGE[0];
    this.subtype = MEDIA_RANGE[1];

    let quality;
    let params: any = {};

    if (parts.length > 0) {

      parts
        .map(p => p.trim())
        .forEach(part => {
          const PARAM = part.split("=");
          if (PARAM[0] === "q") {
            quality = parseFloat(PARAM[1]);
            return;
          }

          WEIGHT++;

          params[PARAM[0]] = PARAM[1];
        });

      this.quality = quality;
      this.params = params;
    }

    this.weight = WEIGHT;
  }

  /**
   * Returns the MIME without quality or params
   */
  toString(): string {
    return `${this.type}/${this.subtype}`;
  }
}
