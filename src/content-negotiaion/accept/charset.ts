import { Quality } from "./interface/quality";
/**
 * Charset
 *
 * Represents a Accept-Charset directive supplied by the
 * client
 */
export class Charset implements Quality {
  /**
   * The name of the charset
   */
  public readonly charset: string;
  /**
   * The quality of the response
   */
  public readonly quality: number;

  constructor(META: string) {

    const PARTS = META.split(";");

    this.charset = PARTS[0];

    if (PARTS[1]) {
      const QUALITY = PARTS[1].trim().replace(/^q=(0\.\d|1)/, "$1");
      this.quality = parseFloat(QUALITY);
    }
  }
}
