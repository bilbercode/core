import { Quality } from "./interface/quality";
import { Specificity } from "./interface/specificity";

/**
 * Language
 *
 * Represents a Accept-Language directive supplied by the
 * client
 */
export class Language implements Quality, Specificity {

  /**
   * The internationalization component of the language [I18n]
   */
  public readonly internationalization: string;
  /**
   * The localization component of the language [l10n]
   */
  public readonly localization: string;
  /**
   * The quality of the response
   */
  public readonly quality: number;
  /**
   * The specificity
   */
  public readonly weight: number;

  constructor(META: string) {

    let WEIGHT = 0;

    const PARTS = META.split(";");

    const I18N_L10N = PARTS[0]
      .trim()
      .toLowerCase()
      .match(/^([a-z]{2}|\*)(?:-([a-z]{2}))?/);

    this.internationalization = I18N_L10N[1];
    if (I18N_L10N[2]) {
      this.localization = I18N_L10N[2];
      WEIGHT++;
    }

    this.weight = WEIGHT;

    if (PARTS[1]) {
      const QUALITY = PARTS[1].trim().replace(/^q=(0\.\d|1)/, "$1");
      this.quality = parseFloat(QUALITY);
    }
  }

  /**
   * Returns a string representation of the directive without the
   * quality rating
   */
  toString(): string {
    if (this.localization) {
      return `${this.internationalization}-${this.localization}`;
    }

    return this.internationalization;
  }
}