import { Translator } from "../i18n/translator";

/**
 * I18n translation handler
 *
 * Added to the response object by the content negotiation.
 */
export class I18n {

  constructor(private translator: Translator,
              private locale: string) { }

  /**
   * Translates a string according to the current Language requested
   * by the client
   *
   * @example A single translation
   *
   * ```
   * i18n.translate('Hello world');
   * ```
   *
   * @example A pluralised translation
   * ```
   * i18n.translate('%d item', '%d items', 12)
   * ```
   *
   * @param {string} str The string to translate
   * @param {string} plural The plural of the string to translate if not already
   *  defined by the translation files
   * @param {number} count The number to pluralise to
   * @returns {string}
   */
  translate(str: string, plural?: string, count?: number): string {

    if (plural) {
      count = count || 1;
      return this.translator.plural(this.locale, str, plural, count);
    }

    return this.translator.translate(this.locale, str);
  }
}
