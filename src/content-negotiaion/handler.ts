import { Request, Response, NextFunction } from "express";
import { Media } from "./accept/media";
import { Translator } from "../i18n/translator";
import { Interface as MediaTypeInterface } from "./media-type/interface";
import * as iconv from "iconv-lite";

/**
 * Response Handler
 *
 * Is added to the response object to provide content-negotiated responses
 * from the API. If provided with a Translator instance, translation is
 * undertaken automatically.
 */
export class Handler {


  constructor(private accept: Media,
              private mediaType: MediaTypeInterface,
              private charset: string) { }

  /**
   * Sends a CN response from the server.
   *
   * Automates;
   * * Translation
   * * Formatting
   * * Charset encoding
   *
   */
  send(content: any, request: Request, response: Response, next: NextFunction): void {

    const PAYLOAD = this.mediaType.format(content, request, this.accept.params);
    /**
     * Set the content type so express doesn't set bin as the content type
     * since were passing it a buffer.
     */
    response.contentType(`${this.mediaType.MIME}; charset=${this.charset}`);

    if (!Buffer.isBuffer(PAYLOAD)) {
      /**
       * The media type returned a string, assume its utf-8 and convert it if required
       */

      if (this.charset === "utf-8") {
        response.send(new Buffer(PAYLOAD));
      } else {
        response.send(iconv.encode(PAYLOAD, this.charset));
      }
    } else {
      /**
       * Since the payload is already in a Buffer we assume that the charset
       * is already in the correct format
       */
      response.send(PAYLOAD);
    }
  }


}
