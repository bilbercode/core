
import { isNullOrUndefined } from "util";
import { Request } from "./request";
import { Response } from "./response";
import { Next } from "./next";

export class Error {

  constructor(public readonly message: string,
              public readonly code = 500) { }
}

export function errorHandler(err: Error, request: Request, response: Response, next: Next): void {

  const CODE = isNullOrUndefined(err.code) ? 500 : err.code;
  const MESSAGE = isNullOrUndefined(err.message) ? "Server Error" : err.message;

  response.status(CODE);

  if (response["contentNegotiation"]) {
    response["contentNegotiation"].send({
      code: CODE,
      message: MESSAGE
    });
  } else {
    response.send({
      code: CODE,
      message: MESSAGE
    });
  }
  next(err);
}
