
import { Request } from "./request";
import { Response } from "./response";
import { Next } from "./next";

import { AbstractContext, IContext } from "../context";
import { Timer } from "../timer/timer";

export class Context extends AbstractContext implements IContext{

  public readonly type = "http";

  constructor(public readonly request: Request,
              public readonly response: Response,
              public readonly next: Next) { super(); }


  getQueryParam(param: string): string {
    return this.request.query[param] || null;
  }

  getBodyParam(param: string): string {
    return this.eval(this.request.body, param);
  }

  getHeaderParam(param: string): string {
    return this.request.headers[param] || null;
  }

  getCookieParam(param: string): string {
    return this.request.cookies[param] || null;
  }

  getRouteParam(param: string): string {
    return this.request.params[param] || null;
  }

  get(param: string): any {
    return this.eval(this.request, param);
  }

  getTimer(): Timer {
    return this.timer;
  }
}
