

export class Event {
  name: string;
  time: Date;
  elapsed: number;

  constructor(name: string, elapsed: number) {
    this.name = name;
    this.time = new Date();
    this.elapsed = elapsed;
  }
}
