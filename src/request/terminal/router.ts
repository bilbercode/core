import { Controller } from "../../dispatcher/terminal/controller";
import { ServiceManager } from "../../service/manager";
import { Request } from "./request";
import { Response } from "./response";


export class Router {

  private dispatchers: Array<Controller> = [];


  constructor(private serviceLocator: ServiceManager) { }

  registerDispatcher(dispatcher: Controller): Router {
    if (this.dispatchers.indexOf(dispatcher) < 0) {
      this.dispatchers.push(dispatcher);
    }
    return this;
  }

  dispatch(argv: Array<string>): Promise<any> {

    const REQUEST = this.serviceLocator.get<Request>(Request, {argv: argv});
    return new Promise((resolve, reject) => {

      let dispatcher: Controller;
      for (let i = 0; i < this.dispatchers.length; i++) {
        if (this.dispatchers[i].canDispatch(REQUEST)) {
          dispatcher = this.dispatchers[i];
        }
      }

      if (dispatcher) {
        const RESPONSE = this.serviceLocator.get<Response>(Response);
        return dispatcher.dispatch(REQUEST, RESPONSE)
          .then(resolve)
          .catch(reject);
      }

      reject(new Error("Unable to dispatch request"));
    });
  }
}
