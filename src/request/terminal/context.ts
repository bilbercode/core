import { Request } from "./request";
import { Response } from "./response";
import { IContext, AbstractContext } from "../context";
import { isNumber } from "util";
import { Timer } from "../timer/timer";

export class Context extends AbstractContext implements IContext {

  public readonly type = "terminal";

  constructor(public readonly routeParams: {[idx: number]: string},
              public readonly routeRegExp: RegExp,
              public readonly request: Request,
              public readonly response: Response) { super(); }


  getQueryParam(param: string): string {
    return this.request.query[param];
  }

  getBodyParam(param: string): string {
    return this.eval(this.request.body, param);
  }

  getHeaderParam(param: string): string {
    return this.eval(this.request.headers, param);
  }

  getCookieParam(param: string): string {
    return null;
  }

  get(param: string): any {
    return this.eval(this.request, param);
  }

  getRouteParam(param: string|number): string {
    this.routeRegExp.lastIndex = 0;

    const ROUTE_MATCHES = this
      .routeRegExp.exec(this.request.raw.split(/\s+--?.*/)[0]);

    if (isNumber(param)) {
      return ROUTE_MATCHES[param] || null;
    }

    const IDXS = Object.keys(this.routeParams);

    for (let i = 0; i < IDXS.length; i++) {
      if (this.routeParams[IDXS[i]] === param) {
        return ROUTE_MATCHES[IDXS[i]];
      }
    }

    return null;
  }

  getTimer(): Timer {
    return this.timer;
  }
}
