import { Writable } from "stream";
import { WritableOptions } from "stream";
import WritableStream = NodeJS.WritableStream;


export class Response extends Writable {

  public responseCode: number = 0;

  private buffer = new Buffer([]);

  private stdout: WritableStream;

  private stderr: WritableStream;

  constructor(opts?: WritableOptions, stdout?: WritableStream, stderr?: WritableStream) {
    super(opts || {});
    this.stdout = stdout || process.stdout;
    this.stderr = stderr || process.stderr;
    this.on("finish", () => {
      this.flush();
    });
  }

  flush(): void {
    if (this.responseCode !== 0) {
      this.stderr.write(this.buffer);
    } else {
      this.stdout.write(this.buffer);
    }
    this.buffer = new Buffer([]);
  }

  _write(chunk: any, encoding: string, callback: Function): void {
    if (Buffer.isBuffer(chunk)) {
      this.buffer = Buffer.concat([this.buffer, chunk]);
    } else {
      this.buffer = Buffer.concat([this.buffer, new Buffer(chunk, encoding)]);
    }

    if (callback) {
      return callback();
    }
  }
}
