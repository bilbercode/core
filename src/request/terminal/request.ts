
export class Request {

  public confidence: number = 0;

  public controller: any;

  public method: any;

  public params: { [k: string]: string } = {};

  public body: any;

  public headers: {
    [header: string]: string|any;
  } = {};

  constructor(public readonly query: {[k: string]: string}, public readonly raw: string) { }
}
