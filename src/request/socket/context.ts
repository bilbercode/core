import { IContext, AbstractContext } from "../context";
import { Timer } from "../timer/timer";

export class Context extends AbstractContext implements IContext {


  public readonly type = "socket";

  constructor(public readonly connectionID: string,
              public readonly namespace: SocketIO.Namespace,
              public readonly socket: SocketIO.Socket,
              public readonly payload: any) {
    super();
  }

  getQueryParam(param: string): string {
    return this.eval(this.socket.handshake.query, param);
  }

  getBodyParam(param: string): string {
    return this.eval(this.payload, param);
  }

  getHeaderParam(param: string): string {
    return this.eval(this.socket.handshake.headers, param);
  }

  getCookieParam(param: string): string {
    return this.eval(this.socket.handshake.headers, param); // TODO revliadate
  }

  getRouteParam(param: string): string {
    return this.eval(this.payload, param);
  }

  get(param: string): any {
    return this.eval(this.socket, param);
  }

  getTimer(): Timer {
    return this.timer;
  }
}
