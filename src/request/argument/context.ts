export class Context {

  public readonly name: string;

  constructor(public readonly service: symbol,
              public readonly expression?: string) {
    this.name = service.toString().replace(/^Symbol\((.*)\)$/, "$1");
  }
}
