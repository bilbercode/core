import { isNullOrUndefined } from "util";
import { Timer } from "./timer/timer";

export interface IContext {

  type: string;

  getQueryParam(param: string): string;

  getBodyParam(param: string): string;

  getHeaderParam(param: string): string;

  getCookieParam(param: string): string;

  getRouteParam(param: string): string;

  getTimer(): Timer;

  get(param: string): any;
}


export abstract class AbstractContext {

  protected timer: Timer;

  constructor() {
    this.timer = new Timer();
  }


  protected eval(object: any, expression: string): any {
    let clone = object;

    let pointers = expression.split(/\./g);

    while ((clone = clone[pointers.shift()]) && pointers.length);

    return isNullOrUndefined(clone) ? null : clone;
  }
}
