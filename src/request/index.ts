import { Context as TerminalContext } from "./terminal/context";
import { Context as HttpContext } from "./http/context";
import { Context as SocketContext } from "./socket/context";

export namespace Request {
  export const Terminal = TerminalContext;
  export const Http = HttpContext;
  export const Socket = SocketContext;
}
