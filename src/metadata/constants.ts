
export const DESIGN_PARAM_TYPES = "design:paramtypes";

export const DI_INJECTABLE = "bilbercode:injectable";
export const INJECTABLE = "bilbercode:injectable:options";
export const FACTORY = "bilbercode:factory";
export const FACTORY_OPTIONS = "bilbercode:factory:options";

export const CONTROLLER = "bilbercode:controller";
export const CONTROLLER_TERMINAL = "bilbercode:controller:terminal";
export const CONTROLLER_SOCKET = "bilbercode:controller:socket";
export const CONTROLLER_HTTP = "bilbercode:controller:http";

export const CONTROLLER_HTTP_VERB = "bilbercode:controller:http:verb";
export const CONTROLLER_TERMINAL_ROUTE = "bilbercode:controller:terminal:route";
export const CONTROLLER_SOCKET_EVENT = "bilbercode:controller:socket:event";

export const FORM = "bilbercode:form";

export const INPUT = "bilbercode:input";

export const FORM_INPUT = "bilbercode:form:input";

export const ARGUMENT = "bilbercode:argument";
export const ARGUMENT_BODY = Symbol("body");
export const ARGUMENT_ROUTE = Symbol("route");
export const ARGUMENT_HEADER = Symbol("header");
export const ARGUMENT_QUERY = Symbol("query");
export const ARGUMENT_COOKIE = Symbol("cookie");
export const ARGUMENT_CONTEXT = Symbol("context");
export const ARGUMENT_ADDON = Symbol("addon");
export const ARGUMENT_TIMER = Symbol("timer");

export const APPLICATION_MODULE = "bilbercode:application:module";
export const APPLICATION_MODULE_CLASS = "bilbercode:application:module:class";

export const MODULE_SERVICE_LOCATOR = Symbol("module-service-locator");

export const SHARED_SERVICE = "bilbercode:sharedservice";
