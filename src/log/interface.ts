export interface ILog {

  /**
   * Log an information level event
   * @param {string} line The line to log
   */
  info(line: string): ILog;
  /**
   * Log a debug level event
   * @param {string} line The line to log
   */
  debug(line: string): ILog;
  /**
   * Log an error level event
   * @param {string} line The line to log
   */
  error(line: string): ILog;
  /**
   * Log a warn level event
   * @param {string} line The line to log
   */
  warn(line: string): ILog;
}
