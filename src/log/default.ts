import "source-map-support/register";
import { ILog as LogInterface } from "./interface";
import { Injectable } from "../decorator/injectable";

@Injectable()
export class DefaultLog implements LogInterface {

  info(line: string): LogInterface {
    process.stdout.write(`[INFO ] ${line}\n`);
    return this;
  }

  debug(line: string): LogInterface {
    process.stdout.write(`[DEBUG] ${line}\n`);
    return this;
  }

  error(line: string): LogInterface {
    process.stderr.write(`[ERROR] ${line}\n`);
    return this;
  }

  warn(line: string): LogInterface {
    process.stdout.write(`[WARN ] ${line}\n`);
    return this;
  }
}
