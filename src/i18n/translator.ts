import { Interface as AdapterInterface } from "./adapter/interface";

export class Translator {


  readonly languages: Array<string>;

  constructor(private adapter: AdapterInterface) { }

  translate(domain: string, str: string): string {

   return this.adapter.translate(domain, str);
  }

  plural(domain: string, str: string, plural: string, count: number): string {

    return this.adapter.plural(domain, str, plural, count);
  }
}
