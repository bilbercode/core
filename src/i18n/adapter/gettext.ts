import * as  NodeGettext from "node-gettext";
import { Interface } from "./interface";
import * as Fs from "fs";


export class Gettext implements Interface {

  protected client: NodeGettext.Gettext;

  protected locales: Array<string> = [];

  constructor(fileGlob) {

    this.client = NodeGettext();

    const files: Array<string> = require("glob").sync(fileGlob);
    files.forEach(file => {

      let match = file.match(/(\w{2}(?:-\w{2})?).mo/);


      if (match[1]) {
        this.locales.push(match[1]);
        const content = Fs.readFileSync(file);
        this.client.addTextDomain(match[1], content);
      }
    });
  }

  translate(domain: string, str: string): string {

    return this.client.dgettex(domain, str);
  }

  plural(domain: string, str: string, plural: string, count: number): string {

    return this.client.dngettext(domain, str, plural, count);
  }
}
