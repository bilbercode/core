
export interface Interface {

  translate(domain: string, str: string): string;

  plural(domain: string, str: string, plural: string, count: number): string;

}