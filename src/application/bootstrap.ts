import { IApplication } from "./application";
import { MetadataStorage } from "../metadata/storage";
import { ServiceManager } from "../service/manager";
import { ApplicationFactory } from "./factory/application";
import { DI_INJECTABLE } from "../metadata/constants";

export function applicationBootstrap(application): IApplication {

  const APPLICATION_SERVICE_MANAGER = new ServiceManager(MetadataStorage);

  MetadataStorage.set(DI_INJECTABLE, [], ApplicationFactory, APPLICATION_SERVICE_MANAGER.id);

  APPLICATION_SERVICE_MANAGER.registerFactory(application, ApplicationFactory);

  return APPLICATION_SERVICE_MANAGER.get<IApplication>(application);
}
