
import { EventEmitter } from "events";
import { MetadataStorage } from "../metadata/storage";
import { Router as TerminalRouter } from "../request/terminal/router";
import { Router as HttpRouter } from "../request/http/router";
import { ServiceManager } from "../service/manager";
import * as Express from "express";
import * as Http from "http";
import * as Https from "https";
import { DefaultLog } from "../log/default";
import { ILog } from "../log/interface";
import { IContext } from "../request/context";
import { Context as HttpContext } from "../request/http/context";
import { Error as HttpError, errorHandler } from "../request/http/error";

import * as SocketIOStatic from "socket.io";
import {
  DESIGN_PARAM_TYPES, DI_INJECTABLE
} from "../metadata/constants";

import * as Util from "util";
import { ModuleFactory } from "./factory/module";


interface IApplicationOptions {
  modules: Array<Function>;
}

export interface IApplicationLifecycle {
  $onError?: (err: Error, context?: IContext) =>  void;
  $onInit?(): Promise<any>;
  $onReady?(): Promise<any>;
}

interface IServerRunConfiguration {
  http?: {
    port: number;
  };
  https?: {
    port: number;
    privateKey?: string|Buffer;
    passphrase?: string;
    x509: string|Buffer
  };
}


export interface IApplication {

  modules?: Array<any>;

  run(config: IServerRunConfiguration): IApplication;

  exec(argv: any): IApplication;

  $onInit?(): Promise<any>;

  $onReady?(): Promise<any>;

  $onError?(err: Error|HttpError, context?: IContext): void;

  setLog(log: ILog): IApplication;

  setLogFromFactory(logClass: ILog, factory: any): IApplication

  getLog(): ILog;
}

export function Application(options: IApplicationOptions): Function {

  return (target: any) => {

    function applicationDecorator<T extends {new (...args: any[]): {}}>(constructor: T) {

      class NeutrinoApplication extends constructor {

        get modules(): Array<any> {
          return options.modules;
        }

        set modules(modules: Array<any>) {
          options.modules = modules;
        }

        private http: Http.Server;

        private https: Https.Server;

        private socketServer: SocketIO.Server;

        private express: Express.Application;

        private expressRouter: HttpRouter;

        private serviceLocator: ServiceManager;

        private terminalRouter: TerminalRouter;

        private log: DefaultLog;

        constructor(...args: any[]) {
          super(...args.slice(3));
          this.serviceLocator = (<ServiceManager>args[0]);
          this.terminalRouter = (<TerminalRouter>args[1]);
          this.log = (<DefaultLog>args[2]);

          (<any>this).on("error", (e, context) => {
            if ("$onError" in this) {
              (<any>this).$onError(e, context);
            }
          });
        }

        setLog(log: ILog): IApplication {
          this.log = log;
          return this;
        }

        setLogFromFactory(logClass: ILog, factory: any): IApplication {
          this.serviceLocator.registerFactory(logClass, factory);
          const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, factory) ?
            MetadataStorage.get(DESIGN_PARAM_TYPES, factory) : [];

          MetadataStorage.set(DI_INJECTABLE, PARAMS, factory, this.serviceLocator.id);

          this.setLog(this.serviceLocator.get<ILog>(logClass));
          return this;
        }

        getLog() {
          return this.log;
        }

        init(): Promise<any> {

          this.modules = this.modules.map(MODULE => {
            this.serviceLocator.registerFactory(MODULE, ModuleFactory);
            return this.serviceLocator.get<any>(MODULE, {
              socketServer: this.socketServer,
              terminalRouter: this.terminalRouter
            });
          });

          return Promise.resolve(this);
        }

        run(config: IServerRunConfiguration): IApplication {

          this.socketServer = SocketIOStatic();

          Promise.resolve()
            .then(() => "$onInit" in this ? (<any>this).$onInit.call(this) : null)
            .then(() => this.init())
            .then(() => this.initialiseModules())
            .then(() => this.startServers(config))
            .then(() => "$onReady" in this ? (<any>this).$onReady.call(this) : null)
            .then(() => this.initHttpErrorHandling())
            .catch((e) => (<any>this).emit("error", e));

          return this;
        }

        exec(argv: Array<string>): IApplication {

          Promise.resolve()
            .then(() => "$onInit" in this ? (<any>this).$onInit() : null)
            .then(() => this.init())
            .then(() => this.initialiseModules(true))
            .then(() => "$onReady" in this ? (<any>this).$onReady() : null)
            .then(() => this.terminalRouter.dispatch(argv))
            .catch((e) => (<any>this).emit("error", e));

          return this;
        }

        private startServers(config: IServerRunConfiguration): Promise<any> {

          let servers: Array<Promise<any>> = [];

          if (config.http) {
            this.http = Http.createServer(this.express);
            this.socketServer.listen(this.http);
            this.http.listen(config.http.port);
            servers.push(new Promise((success, failure) => {

              this.http.on("listening", () => {

                this.log.info(`HTTP server started and listening on port ${config.http.port}`);
                return success(this.http);
              })
                .on("error", (e) => {
                  failure(e);
                });
            }));
          }


          if (config.https) {

            let configOptions: any = {};

            if (config.https.privateKey && config.https.x509) {
              configOptions.cert = config.https.x509;
              configOptions.key = config.https.privateKey;

              if (config.https.passphrase) {
                configOptions.passphrase = config.https.passphrase;
              }
            }

            this.https = Https.createServer(configOptions, this.express);
            this.socketServer.listen(this.https);
            this.https.listen(config.https.port);
            servers.push(new Promise((success, failure) => {
              this.https.on("listening", () => {

                // this.log.info(`HTTPS server started and listening on port ${this.httpsPort}`);

                return success(this.https);
              })
                .on("error", (e) => {
                  failure(e);
                });
            }));
          }


          return Promise.all<any>(servers);
        }



        private initHttpErrorHandling() {
          this.expressRouter.use(errorHandler);
          this.expressRouter.use((err: any, request, response, next) => {

            const CONTEXT = this.serviceLocator.get<HttpContext>(HttpContext, {
              request: request,
              response: response,
              next: next
            });

            (<any>this).emit("error", err, CONTEXT);
          });
        }

        private initialiseModules(terminalOnly = false): Promise<any> {

          if (!terminalOnly) {
            this.express = Express();

            this.expressRouter = this.serviceLocator.get<HttpRouter>(HttpRouter);

            this.express.use(this.expressRouter);
          }

          const MODULES = this.modules.map<Promise<any>>(M => {
            (<any>M).on("error", (msg) => {
              (<any>this).emit("error", msg);
            });
            return M.initProviders(terminalOnly);
          });

          return Promise.all(MODULES)
            .then(MODULE_LIST => {
              return Promise.all(MODULE_LIST.map(M => M.init(terminalOnly)))
                .then(() => {
                  if (!terminalOnly) {
                    MODULE_LIST.forEach(MODULE => {
                      this.expressRouter.use(MODULE.httpRouter);
                    });
                  }
                  return MODULES;
                });
            });

        }
      }


      const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, target) ?
        MetadataStorage.get<Array<any>>(DESIGN_PARAM_TYPES, target) : [];

      PARAMS.unshift(DefaultLog);
      PARAMS.unshift(TerminalRouter);
      PARAMS.unshift(ServiceManager);

      MetadataStorage.set(DI_INJECTABLE, PARAMS, NeutrinoApplication);

      return NeutrinoApplication;
    }

    function decorateEventEmitter<T extends {new (...args: any[]): {}}>(constructor: T) {

      const ORIGIN = constructor.constructor;

      constructor.constructor = function (...args: any[]) {
        ORIGIN.apply(this, args);
        EventEmitter.call(this);
      };

      Util.inherits(constructor, EventEmitter);

      return constructor;
    }


    return applicationDecorator(decorateEventEmitter(target));
  };
}
