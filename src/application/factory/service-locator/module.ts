import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { MetadataStorage } from "../../../metadata/storage";

import { ControllerFactory as HttpControllerFactory } from "./../http/controller";
import { Controller as HttpController } from "../../../dispatcher/http/controller";
import { MethodFactory as HttpMethodFactory } from "./../http/method";
import { Method as HttpMethod } from "../../../dispatcher/http/method";
import { ContextFactory as HttpContextFactory } from "./../http/request-context";
import { Context as HttpContext } from "../../../request/http/context";
import { RouterFactory as HttpRouterFactory } from "./../http/router";
import { Router as HttpRouter } from "../../../request/http/router";

import { ControllerFactory as TerminalControllerFactory } from "./../terminal/controller";
import { Controller as TerminalController } from "../../../dispatcher/terminal/controller";
import { MethodFactory as TerminalMethodFactory } from "./../terminal/method";
import { Method as TerminalMethod } from "../../../dispatcher/terminal/method";
import { ContextFactory as TerminalContextFactory } from "./../terminal/request-context";
import { Context as TerminalContext } from "../../../request/terminal/context";
import { RouterFactory as TerminalRouterFactory } from "./../terminal/router";
import { Router as TerminalRouter } from "../../../request/terminal/router";


import { ControllerFactory as SocketControllerFactory } from "./../socket/controller";
import { Controller as SocketController } from "../../../dispatcher/socket/controller";
import { MethodFactory as SocketMethodFactory } from "./../socket/method";
import { Method as SocketMethod } from "../../../dispatcher/socket/method";
import { ContextFactory as SocketContextFactory } from "./../socket/request-context";
import { Context as SocketContext } from "../../../request/socket/context";

import { WhitelistServiceLocatorFactory } from "./whitelist";
import { WhitelistedServiceManager } from "../../../service/whitelisted-manager";



import { DI_INJECTABLE, DESIGN_PARAM_TYPES } from "../../../metadata/constants";


import { Negotiator } from "../../../content-negotiaion/negotiator";
import { TrimString } from "../../../form/input/filter/trim-string";
import { StripTags } from "../../../form/input/filter/strip-tags";
import { NotEmpty } from "../../../form/input/validator/not-empty";
import { DefaultLog } from "../../../log/default";
import { Form } from "../../../form/form";
import { FormFactory } from "../../../form/factory/form";
import { Input } from "../../../form/input/input";
import { InputFactory } from "../../../form/factory/input";




export class ModuleServiceLocatorFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options?: any): ServiceManager {

    const SERVICE_LOCATOR = new ServiceManager(MetadataStorage);

    // Socket
    SERVICE_LOCATOR.registerFactory(SocketController, SocketControllerFactory);
    SERVICE_LOCATOR.registerFactory(SocketMethod, SocketMethodFactory);
    SERVICE_LOCATOR.registerFactory(SocketContext, SocketContextFactory);

    // HTTP
    SERVICE_LOCATOR.registerFactory(HttpController, HttpControllerFactory);
    SERVICE_LOCATOR.registerFactory(HttpMethod, HttpMethodFactory);
    SERVICE_LOCATOR.registerFactory(HttpContext, HttpContextFactory);
    SERVICE_LOCATOR.registerFactory(HttpRouter, HttpRouterFactory);

    // Terminal
    SERVICE_LOCATOR.registerFactory(TerminalController, TerminalControllerFactory);
    SERVICE_LOCATOR.registerFactory(TerminalMethod, TerminalMethodFactory);
    SERVICE_LOCATOR.registerFactory(TerminalContext, TerminalContextFactory);
    SERVICE_LOCATOR.registerFactory(TerminalRouter, TerminalRouterFactory);

    SERVICE_LOCATOR.registerFactory(Form, FormFactory);
    SERVICE_LOCATOR.registerFactory(Input, InputFactory);

    SERVICE_LOCATOR.registerFactory(WhitelistedServiceManager, WhitelistServiceLocatorFactory);


    const INJECTABLES = [
      Negotiator,
      TrimString,
      StripTags,
      NotEmpty,
      DefaultLog,
      SocketControllerFactory,
      SocketMethodFactory,
      SocketContextFactory,
      HttpControllerFactory,
      HttpMethodFactory,
      HttpContextFactory,
      HttpRouterFactory,
      TerminalControllerFactory,
      TerminalMethodFactory,
      TerminalContextFactory,
      TerminalRouterFactory,
      WhitelistServiceLocatorFactory,
      FormFactory,
      InputFactory,
    ];

    INJECTABLES.forEach(INJECTABLE => {
      this.registerInjectable(INJECTABLE, SERVICE_LOCATOR.id);
    });

    SERVICE_LOCATOR.set(MetadataStorage, MetadataStorage);

    return SERVICE_LOCATOR;
  }


  private registerInjectable(injectable: Function,
                             slID: symbol) {

    const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, injectable) ?
      MetadataStorage.get(DESIGN_PARAM_TYPES, injectable) : [];

    MetadataStorage.set(
      DI_INJECTABLE,
      PARAMS,
      injectable,
      slID
    );
  }
}
