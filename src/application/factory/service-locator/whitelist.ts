import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { WhitelistedServiceManager } from "../../../service/whitelisted-manager";
import { MetadataStorage } from "../../../metadata/storage";


export class WhitelistServiceLocatorFactory implements IFactory {

  createService(serviceLocator: ServiceManager,
                   options: Array<Function|FunctionConstructor|symbol>): WhitelistedServiceManager {

    const INSTANCE = new WhitelistedServiceManager(MetadataStorage, options);
    INSTANCE.setPeeringManager(serviceLocator);
    return INSTANCE;
  }
}
