import { IFactory } from "../../../service/factory";
import { Router } from "../../../request/http/router";
import { ServiceManager } from "../../../service/manager";

export class RouterFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options?: any): Router {
    return Router(options);
  }
}
