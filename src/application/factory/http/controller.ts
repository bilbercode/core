import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Controller, IControllerOptions } from "../../../dispatcher/http/controller";
import { Router } from "../../../request/http/router";
import { MetadataStorage } from "../../../metadata/storage";
import { CONTROLLER_HTTP, CONTROLLER_HTTP_VERB } from "../../../metadata/constants";
import { IHttpMiddleware, IHttpErrorMiddleware } from "../../../dispatcher/http/middleware";
import { Method } from "../../../dispatcher/http/method";
import { IVerbOptions } from "../../../decorator/http/verb";

interface IControllerFactoryOptions {
  dispatchable: Function;
}


export class ControllerFactory implements IFactory {

  createService<T>(serviceLocator: ServiceManager, options: IControllerFactoryOptions): Controller {

    const METADATA_STORAGE = serviceLocator.get<MetadataStorage>(MetadataStorage);

    const OPTIONS = METADATA_STORAGE.get<IControllerOptions>(CONTROLLER_HTTP, options.dispatchable);

    const MIDDLEWARE: Array<IHttpMiddleware|IHttpErrorMiddleware> = (OPTIONS.middleware || [])
      .map(MIDDLEWARE_FN => {
        const RESULT = serviceLocator.has(MIDDLEWARE_FN) ?
          serviceLocator.get(MIDDLEWARE_FN) : MIDDLEWARE_FN;

        return RESULT;
      });


    const ROUTER = serviceLocator.get<Router>(Router);

    const DISPATCHERS: Array<Method> = [];

    Object.getOwnPropertyNames(options.dispatchable.prototype)
      .forEach(METHOD_NAME => {
        if (METADATA_STORAGE.has(CONTROLLER_HTTP_VERB, options.dispatchable, METHOD_NAME)) {
          const METHOD_OPTIONS = METADATA_STORAGE
            .get<IVerbOptions>(CONTROLLER_HTTP_VERB, options.dispatchable, METHOD_NAME);
          const DISPATCHER = serviceLocator.get<Method>(Method, {
            controller: options.dispatchable,
            method: METHOD_NAME,
            verb: METHOD_OPTIONS.verb,
            middleware: METHOD_OPTIONS.middleware,
            path: METHOD_OPTIONS.path
          });
          DISPATCHERS.push(DISPATCHER);
        }
      });

    return new Controller(
      ROUTER,
      DISPATCHERS,
      OPTIONS.route,
      MIDDLEWARE
    );
  }
}
