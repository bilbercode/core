import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { IHttpMiddleware } from "../../../dispatcher/http/middleware";
import { Method } from "../../../dispatcher/http/method";
import { MetadataStorage } from "../../../metadata/storage";
import { ARGUMENT } from "../../../metadata/constants";
import { Router } from "../../../request/http/router";
import { Context } from "../../../request/argument/context";
import { Form } from "../../../request/argument/form";
import { Negotiator } from "../../../content-negotiaion/negotiator";

interface IMethodOptions {
  path?: string;
  middleware: Array<IHttpMiddleware>;
  verb: string;
  controller: Function;
  method: string;
}

export class MethodFactory implements IFactory {

  createService<T>(serviceLocator: ServiceManager, options: IMethodOptions): Method {

    const METADATA_STORAGE = serviceLocator.get<MetadataStorage>(MetadataStorage);

    const FUNCTION_ARGS: Array<Context|Form> =
      METADATA_STORAGE.get<Array<Context|Form>>(ARGUMENT, options.controller, options.method);

    const ROUTE_PATH = options.path || "";

    const MIDDLEWARE: Array<IHttpMiddleware> = options.middleware || [];

    const CONTENT_NEGOTIATOR = serviceLocator.get<Negotiator>(Negotiator);

    MIDDLEWARE.push(CONTENT_NEGOTIATOR.validate.bind(CONTENT_NEGOTIATOR));

    const ROUTER = serviceLocator.get<Router>(Router);

    return new Method(
      ROUTER,
      ROUTE_PATH,
      options.verb,
      FUNCTION_ARGS || [],
      options.controller,
      options.method,
      MIDDLEWARE,
      serviceLocator
    );
  }
}
