import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Context } from "../../../request/http/context";
import { Request } from "../../../request/http/request";
import { Response } from "../../../request/http/response";
import { Next } from "../../../request/http/next";

interface IHttpContextOptions {
  request: Request;
  response: Response;
  next: Next;
}

export class ContextFactory implements IFactory {


  createService<T>(serviceLocator: ServiceManager, options?: any): Context {

    return new Context(options.request, options.response, options.next);
  }
}
