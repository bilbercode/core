import { IFactory } from "../../service/factory";
import { ServiceManager } from "../../service/manager";
import { MetadataStorage } from "../../metadata/storage";
import {
  DI_INJECTABLE, DESIGN_PARAM_TYPES,
  MODULE_SERVICE_LOCATOR
} from "../../metadata/constants";
import { Router } from "../../request/terminal/router";
import { DefaultLog } from "../../log/default";
import { RouterFactory as TerminalRouterFactory } from "./terminal/router";
import { Router as TerminalRouter } from "../../request/terminal/router";
import { ModuleServiceLocatorFactory } from "./service-locator/module";
import { RequestFactory as TerminalRequestFactory } from "./terminal/request";
import { Request as TerminalRequest } from "./../../request/terminal/request";
import { ResponseFactory as TerminalResponseFactory } from "./terminal/response";
import { Response as TerminalResponse } from "./../../request/terminal/response";
import { RouterFactory as HttpRouterFactory } from "./http/router";
import { ContextFactory as HttpContextFactory } from "./http/request-context";
import { Context as HttpContext } from "./../../request/http/context";
import { Router as HttpRouter } from "../../request/http/router";
import { ModuleFactory } from "./module";


export class ApplicationFactory implements IFactory {


  createService(serviceLocator: ServiceManager,
                   options?: any,
                   target?: FunctionConstructor) {

    const APPLICATION_SERVICE_LOCATOR = new ServiceManager(MetadataStorage);

    MetadataStorage.set(DI_INJECTABLE, [], TerminalRouterFactory, APPLICATION_SERVICE_LOCATOR.id);
    MetadataStorage.set(DI_INJECTABLE, [], TerminalRequestFactory, APPLICATION_SERVICE_LOCATOR.id);
    MetadataStorage.set(DI_INJECTABLE, [], TerminalResponseFactory, APPLICATION_SERVICE_LOCATOR.id);
    MetadataStorage.set(DI_INJECTABLE, [], ModuleServiceLocatorFactory, APPLICATION_SERVICE_LOCATOR.id);
    MetadataStorage.set(DI_INJECTABLE, [], ModuleFactory, APPLICATION_SERVICE_LOCATOR.id);
    MetadataStorage.set(DI_INJECTABLE, [], HttpRouterFactory, APPLICATION_SERVICE_LOCATOR.id);
    MetadataStorage.set(DI_INJECTABLE, [], HttpContextFactory, APPLICATION_SERVICE_LOCATOR.id);

    APPLICATION_SERVICE_LOCATOR.registerFactory(TerminalRouter, TerminalRouterFactory);
    APPLICATION_SERVICE_LOCATOR.registerFactory(
      MODULE_SERVICE_LOCATOR, ModuleServiceLocatorFactory);

    APPLICATION_SERVICE_LOCATOR.registerFactory(TerminalRequest, TerminalRequestFactory);
    APPLICATION_SERVICE_LOCATOR.registerFactory(TerminalResponse, TerminalResponseFactory);

    APPLICATION_SERVICE_LOCATOR.registerFactory(HttpRouter, HttpRouterFactory);
    APPLICATION_SERVICE_LOCATOR.registerFactory(HttpContext, HttpContextFactory);

    this.registerInjectable(DefaultLog, APPLICATION_SERVICE_LOCATOR.id);

    const PARAMS: Array<any> = MetadataStorage.get<Array<any>>(DI_INJECTABLE, target);

    const INJECTIONS = PARAMS.map((PARAM, i) => {
        switch (i) {
          case 0:
            return APPLICATION_SERVICE_LOCATOR;
          case 1:
            return APPLICATION_SERVICE_LOCATOR.get<Router>(Router);
          case 2:
            return APPLICATION_SERVICE_LOCATOR.get<DefaultLog>(DefaultLog);
          default:
            return APPLICATION_SERVICE_LOCATOR.get<any>(PARAM);
        }
      });

    return new target(...INJECTIONS);
  }

  private registerInjectable(injectable: Function,
                             slID: symbol) {

    const PARAMS = MetadataStorage.has(DESIGN_PARAM_TYPES, injectable) ?
      MetadataStorage.get(DESIGN_PARAM_TYPES, injectable) : [];

    MetadataStorage.set(
      DI_INJECTABLE,
      PARAMS,
      injectable,
      slID
    );
  }
}
