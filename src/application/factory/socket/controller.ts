import { ServiceManager } from "../../../service/manager";
import { Controller as SocketController } from "../../../dispatcher/socket/controller";
import { IFactory } from "../../../service/factory";
import { MetadataStorage } from "../../../metadata/storage";
import { ISocketMiddleware } from "../../../dispatcher/socket/middleware";
import { CONTROLLER_SOCKET, CONTROLLER_SOCKET_EVENT } from "../../../metadata/constants";
import { Method } from "../../../dispatcher/socket/method";
import { IEventListenerOptions } from "../../../decorator/socket/event-listener";

interface IControllerOptions {
  controller: Function;
  server: SocketIO.Server;
}

interface IControllerDecoratorOptions  {
  namespace: string;
  middleware: Array<ISocketMiddleware>;
}

export class ControllerFactory implements IFactory {


  createService(serviceLocator: ServiceManager, options: IControllerOptions): SocketController {

    const METADATA_STORAGE = serviceLocator.get<MetadataStorage>(MetadataStorage);

    const DECORATOR_OPTIONS = METADATA_STORAGE
      .get<IControllerDecoratorOptions>(CONTROLLER_SOCKET, options.controller);

    const MIDDLEWARE: Array<ISocketMiddleware> = DECORATOR_OPTIONS.middleware;

    const NAMESPACE = (options.server) ? options.server.of(DECORATOR_OPTIONS.namespace) : null;

    const DISPATCHERS: Array<Method> = [];

    Object.getOwnPropertyNames(options.controller.prototype)
      .forEach(METHOD_NAME => {
        if (METADATA_STORAGE.has(CONTROLLER_SOCKET_EVENT, options.controller, METHOD_NAME)) {
          const METHOD_OPTIONS = METADATA_STORAGE
            .get<IEventListenerOptions>(CONTROLLER_SOCKET_EVENT, options.controller, METHOD_NAME);
          const DISPATCHER = serviceLocator.get<Method>(Method, {
            controller: options.controller,
            methodName: METHOD_NAME,
            event: METHOD_OPTIONS.event,
            middleware: METHOD_OPTIONS.middleware,
          });
          DISPATCHERS.push(DISPATCHER);
        }
      });

    return new SocketController(
      options.controller,
      DECORATOR_OPTIONS.namespace,
      options.server,
      NAMESPACE,
      MIDDLEWARE,
      DISPATCHERS,
      serviceLocator
    );
  }
}
