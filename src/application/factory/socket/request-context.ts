import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Context } from "../../../request/socket/context";

interface ISocketContextOptions {
  connectionID: string;
  namespace: SocketIO.Namespace;
  socket: SocketIO.Socket;
  payload: any;
}

export class ContextFactory implements IFactory {


  createService(serviceLocator: ServiceManager, options: ISocketContextOptions): Context {
    return new Context(options.connectionID, options.namespace, options.socket, options.payload);
  }
}
