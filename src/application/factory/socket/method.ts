import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Method } from "../../../dispatcher/socket/method";
import { MetadataStorage } from "../../../metadata/storage";
import { ARGUMENT } from "../../../metadata/constants";
import { ISocketMiddleware } from "../../../dispatcher/socket/middleware";
import { Context } from "../../../request/argument/context";
import { Form } from "../../../request/argument/form";

interface IMethodOptions {
  event: string;
  controller: Function;
  methodName: string;
  middleware: Array<ISocketMiddleware>;
}

export class MethodFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options: IMethodOptions): Method {

    const METADATA_STORAGE = serviceLocator.get<MetadataStorage>(MetadataStorage);

    const FUNCTION_ARGUMENTS = METADATA_STORAGE
      .has(ARGUMENT, options.controller, options.methodName) ?
      METADATA_STORAGE
        .get<Array<Context|Form>>(ARGUMENT, options.controller, options.methodName) : [];

    return new Method(
      options.event,
      FUNCTION_ARGUMENTS,
      options.middleware,
      options.controller,
      options.methodName,
      serviceLocator
    );
  }
}
