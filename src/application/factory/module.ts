import { IFactory } from "../../service/factory";
import { ServiceManager } from "../../service/manager";
import { MetadataStorage } from "../../metadata/storage";
import { Controller } from "../../dispatcher/terminal/controller";
import { ControllerFactory } from "./terminal/controller";
import { Method } from "../../dispatcher/terminal/method";
import { MethodFactory } from "./terminal/method";
import { Injector } from "../../di/injector";
import {
  FACTORY, FACTORY_OPTIONS, APPLICATION_MODULE,
  DI_INJECTABLE, MODULE_SERVICE_LOCATOR
} from "../../metadata/constants";
import { Router } from "../../request/terminal/router";

interface IModuleOptions {
  socketServer: SocketIO.Server;
  terminalRouter: Router;
}

export class ModuleFactory implements IFactory {

  createService(serviceLocator: ServiceManager,
                options: IModuleOptions,
                target?: FunctionConstructor): any {

    const SERVICE_MANAGER = serviceLocator.get<ServiceManager>(MODULE_SERVICE_LOCATOR);

    MetadataStorage.set(DI_INJECTABLE, [], MetadataStorage, SERVICE_MANAGER.id);


    SERVICE_MANAGER.registerFactory(Controller, ControllerFactory);
    SERVICE_MANAGER.registerFactory(Method, MethodFactory);

    SERVICE_MANAGER.set(Router, options.terminalRouter);

    const METADATA = MetadataStorage;

    const DEPENDENCY_INJECTOR = new Injector(SERVICE_MANAGER, METADATA);

    if ((<any>target).providers) {

      (<any>target).providers.forEach(COMPONENT => {
        if (METADATA.has(FACTORY, COMPONENT)) {

          const FACTORY_CONFIG = METADATA.has(FACTORY_OPTIONS, COMPONENT) ?
            METADATA.get<any>(FACTORY_OPTIONS, COMPONENT) : { shared: false, exported: false};

          SERVICE_MANAGER
            .registerFactory(METADATA.get(FACTORY, COMPONENT), COMPONENT);

          SERVICE_MANAGER.setShared(
            METADATA.get<any>(FACTORY, COMPONENT), FACTORY_CONFIG.shared || false);
        }
      });
    }

    const TARGET = METADATA.get<any>(APPLICATION_MODULE, target);

    const INJ_ARGS = METADATA.has(DI_INJECTABLE, TARGET) ?
      METADATA.get<Array<any>>(DI_INJECTABLE, TARGET) : [];

    const INSTANCE_ARGS = INJ_ARGS.map((dep, i) => {
      switch (i) {
        case 0:
          return SERVICE_MANAGER;
        case 1:
          return METADATA;
        case 2:
          return options.socketServer;
        default:
          return SERVICE_MANAGER.get.call(SERVICE_MANAGER, dep, null);
      }
    });

    const INSTANCE = new target(...INSTANCE_ARGS);

    return INSTANCE;
  }
}
