import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Router } from "../../../request/terminal/router";

export class RouterFactory implements IFactory {

  createService(serviceLocator: ServiceManager): Router {
    serviceLocator.setShared(Router, true);
    return new Router(serviceLocator);
  }
}
