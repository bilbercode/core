import { ServiceManager } from "../../../service/manager";
import { Method, IMethodOptions } from "../../../dispatcher/terminal/method";
import { IFactory } from "../../../service/factory";
import { Factory } from "../../../decorator/factory";
import { Controller } from "../../../dispatcher/terminal/controller";
import { MetadataStorage } from "../../../metadata/storage";

export interface IMethodDispatcherFactoryOptions {
  method: string;
  controllerDispatcher: Controller;
  confidence: number;
  methodOptions: IMethodOptions;
}

export class MethodFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options?: IMethodDispatcherFactoryOptions): Method {

    let confidence = options.confidence + 1;
    return new Method(
      options.controllerDispatcher,
      serviceLocator,
      MetadataStorage,
      options.method,
      options.methodOptions,
      confidence
    );
  }
}
