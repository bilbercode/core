import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Response } from "../../../request/terminal/response";


export class ResponseFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options?: any): Response{
    return new Response();
  }
}
