import * as yargs from "yargs";
import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Request } from "../../../request/terminal/request";

export class RequestFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options?: any): Request {
    return new Request(yargs.argv, options.argv.join(" "));
  }
}
