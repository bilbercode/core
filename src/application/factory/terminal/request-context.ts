import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Context } from "../../../request/terminal/context";

export class ContextFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options?: any): Context {
    return new Context(options.routeParams, options.routeRegExp, options.request, options.response);
  }
}
