import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Controller, IControllerOptions } from "../../../dispatcher/terminal/controller";
import { Factory } from "../../../decorator/factory";
import { MetadataStorage } from "../../../metadata/storage";

export interface IControllerDispatcherFactoryOptions {
  dispatchable: Function;
  confidence: number;
  controllerOptions: IControllerOptions;
}

export class ControllerFactory implements IFactory {

  createService(serviceLocator: ServiceManager,
                options?: IControllerDispatcherFactoryOptions): Controller {

    let confidence = (options.confidence || 0);

    return new Controller(
      options.dispatchable,
      serviceLocator,
      MetadataStorage,
      confidence,
      options.controllerOptions
    );
  }
}
