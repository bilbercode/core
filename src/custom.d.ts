



declare module "node-gettext" {

  function g(): g.Gettext;



  namespace g {

    class Gettext {
      /**
       * Adds a gettext to the domains list. If default textdomain is not set, uses it
       * as default
       */
      addTextDomain(domain: string);
      addTextDomain(domain: string, contents?: Buffer|string): void;
      /**
       * Changes the current default textdomain
       */
      textdomain(domain: string): string|boolean
      /**
       * Translates a string using the default textdomain
       */
      gettext(msgid: string): string;
      /**
       * Translates a string using a specific domain
       */
      dgettex(domain: string, msgid: string): string;
      /**
       * Translates a plural string using the default textdomain
       */
      ngettext(msgid: string, msgidPlural: string, count: number): string;
      /**
       * Translates a plural string using a specific textdomain
       */
      dngettext(domain: string, msgid: string, msgidPlural: string, count: number): string;
      /**
       * Translates a string from a specific context using the default textdomain
       */
      pgettext(mgsctxt: string, msgid: string): string;
      /**
       * Translates a string from a specific context using s specific textdomain
       */
      dpgettext(domain: string, mgsctxt: string, msgid: string): string;
      /**
       * Translates a plural string from a specific context using the default textdomain
       */
      npgettext(msgctxt: string, msgid: string, msgidPlural: string, count: number): string;
      /**
       * Translates a plural string from a specific context using a specific domain
       */
      dnpgettext(domain: string, msgctxt: string, msgid: string,
                 msgidPlural: string, count: number): string;
      /**
       * Retrieves comments object for a translation
       */
      getComment(domain: string, msgctxt: string, msgid: string): string;
    }
  }


  export = g;
}
