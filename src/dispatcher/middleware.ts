import { IContext } from "../request/context";

export interface IMiddleware {
  (requestContext: IContext, next: (err: any) => void): void;
}
