import { ServiceManager } from "../../service/manager";
import { Context as SocketContext } from "../../request/socket/context";
import { Context as RequestContext } from "../../request/argument/context";
import { Form } from "../../request/argument/form";
import { Observable } from "rxjs/Observable";
import { Form as FormInstance } from "../../form/form";
import { EventEmitter } from "events";
import { ISocketMiddleware } from "./middleware";
import { CRJSON } from "neutrino-crjson";


function isRequestContext(value: any): value is RequestContext {
  return !!(value instanceof RequestContext);
}

function isRequestForm(value: any): value is Form {
  return !!(value instanceof Form);
}

function isPromise<T>(value: any): value is Promise<T> {
  return !!(value instanceof Promise);
}

function isObservable<T>(value: any): value is Observable<T> {
  if ("subscribe" in value) {
    return true;
  }

  return false;
}

export class Method extends EventEmitter {


  constructor(public readonly event: string,
              public readonly methodParams: Array<RequestContext|Form>,
              private middleware: Array<ISocketMiddleware>,
              private readonly controller: Function,
              private readonly methodName: string,
              private readonly serviceLocator: ServiceManager) {
    super();
  }

  init(namespace: SocketIO.Namespace, socket: SocketIO.Socket) {
    this.initMiddleware(this.middleware , socket);
    socket.on(this.event, this.dispatch.bind(this, socket, namespace));
  }


  dispatch(socket: SocketIO.Socket,
           namespace: SocketIO.Namespace,
           msg: any,
           ack: (msg?: any) => void): any {

    const REQUEST_CONTEXT = this.serviceLocator.get<SocketContext>(SocketContext, {
      connectionID: socket.id,
      namespace: namespace,
      socket: socket,
      payload: msg
    });


    const ARGS = [];
    return Promise.resolve(this.methodParams)
      .then<any>((PARAMS: any) => {


        const PARAMS_ASYNC = PARAMS.map(PARAM => {
          return Promise.resolve<RequestContext|Form>(PARAM)
            .then(P => {
              if (isRequestForm(P)) {
                const MODEL_PROTOTYPE = P.model;
                const FORM: FormInstance = this.serviceLocator.get<FormInstance>
                (MODEL_PROTOTYPE);

                return FORM.isValid(REQUEST_CONTEXT)
                  .then(OUTCOME => {
                    if (OUTCOME === true) {
                      return FORM.getObject();
                    }

                    throw OUTCOME;
                  });
              }
              if (isRequestContext(P)) {
                switch (P.name) {
                  case "context":
                    return REQUEST_CONTEXT;
                  case "query":
                    return REQUEST_CONTEXT.getQueryParam(P.expression);
                  case "route":
                    return REQUEST_CONTEXT.getRouteParam(P.expression);
                  case "header":
                    return REQUEST_CONTEXT.getHeaderParam(P.expression);
                  case "cookie":
                    return REQUEST_CONTEXT.getCookieParam(P.expression);
                  case "body":
                    return REQUEST_CONTEXT.getBodyParam(P.expression);
                  case "addon":
                    return REQUEST_CONTEXT.get(P.expression);
                  case "timer":
                    return REQUEST_CONTEXT.getTimer();
                  default:
                    // Terminal Controllers are unable to pass headers et.al.
                    return null;
                }
              }

              const CONTROLLER_NAME = this.controller.prototype.constructor.name;

              throw new Error("Unknown request argument in " +
                `"${CONTROLLER_NAME}::${this.methodName}" - ` + P);
            });
        });

        return Promise.all(PARAMS_ASYNC);

      }).then<any>(PARAMS => {

        const CONTROLLER = this.serviceLocator.get(this.controller);
        let OUTCOME: any;
        try {
          OUTCOME = CONTROLLER[this.methodName](...PARAMS);
        } catch (e) {
          return Promise.reject(e);
        }

        if (isPromise(OUTCOME)) {

          return OUTCOME.then(VALUES => {
            const DATA = JSON.parse(CRJSON.stringify(<any>VALUES));
            socket.emit(`${this.event}.response`, DATA);
            if (ack) {
              ack();
            }
          });

        } else if (isObservable(OUTCOME)) {

          return new Promise((resolve, reject) => {
            OUTCOME.subscribe(VALUES => {
              socket.emit(`${this.event}.response`, JSON.parse(CRJSON.stringify(VALUES)));
              resolve(PARAMS);
            }, (err) => {
              reject(err);
            }, ack);
          });
        } else {
          socket.emit(`${this.event}.response`, JSON.parse(CRJSON.stringify(OUTCOME)));
          if (ack) {
            ack();
          }
          return Promise.resolve(PARAMS);
        }
      })
      .catch(e => {
        (<any>this).emit("error", e);
        socket.emit(`${this.event}.error`, e);
        if (ack) {
          ack();
        }
      });
  }


  initMiddleware(middlewareList: Array<ISocketMiddleware>, socket: SocketIO.Socket): void {
    middlewareList.forEach(MIDDLEWARE_FN => {

      let middleware: ISocketMiddleware;

      if (this.serviceLocator.has(MIDDLEWARE_FN)) {
        middleware = this.serviceLocator.get<ISocketMiddleware>(MIDDLEWARE_FN);
      }

      (<any>socket).use(middleware.bind(middleware, socket));
    });
  }
}
