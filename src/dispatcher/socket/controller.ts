

import { ISocketMiddleware } from "./middleware";
import { Method } from "./method";
import { ServiceManager } from "../../service/manager";
import { EventEmitter } from "events";

interface EventAwareController {
  $onConnection(socket: SocketIO.Socket): void;
  $onDisconnect(socket: SocketIO.Socket): void;
}

export class Controller extends EventEmitter {

  constructor(public readonly controller: Function,
              public readonly namespaceID: string,
              private readonly server: SocketIO.Server,
              private readonly namespace: SocketIO.Namespace,
              private readonly middleware: Array<ISocketMiddleware>,
              private readonly dispatchers: Array<Method>,
              private readonly serviceLocator: ServiceManager) {

    super();

    if (this.namespace) {

      this.dispatchers.forEach(DISPATCHER => {
        (<any>DISPATCHER).on("error", (err) => {
          (<any>this).emit("error", err);
        });
      });


      middleware.forEach(MIDDLEWARE => {
        let middlewareFn: ISocketMiddleware = MIDDLEWARE;

        if (serviceLocator.has(MIDDLEWARE)) {
          middlewareFn = serviceLocator.get<ISocketMiddleware>(MIDDLEWARE);
        }
        (<any>namespace).use(middlewareFn.bind(middlewareFn, namespace));
      });

      namespace.on("connection", (sock: SocketIO.Socket) => {

        if ("$onConnection" in this.controller.prototype) {
          const CONTROLLER_INSTANCE: EventAwareController =
            this.serviceLocator.get<EventAwareController>(this.controller);

          CONTROLLER_INSTANCE.$onConnection(sock);
        }

        middleware.forEach(MIDDLEWARE => {
          let middlewareFn: ISocketMiddleware = MIDDLEWARE;

          if (serviceLocator.has(MIDDLEWARE)) {
            middlewareFn = serviceLocator.get<ISocketMiddleware>(MIDDLEWARE);
          }
          (<any>sock).use(middlewareFn.bind(middlewareFn, sock));
        });

        this.dispatchers.forEach(DISPATCHER => {
          DISPATCHER.init(this.namespace, sock);
        });


        if ("$onDisconnect" in this.controller.prototype) {
          sock.on("disconnect", () => {
            const CONTROLLER_INSTANCE: EventAwareController =
              this.serviceLocator.get<EventAwareController>(this.controller);

            CONTROLLER_INSTANCE.$onDisconnect(sock);
          });
        }

      });
    }
  }


}
