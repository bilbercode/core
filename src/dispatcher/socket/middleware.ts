
import * as SocketIOStatic from "socket.io";


export interface ISocketMiddleware {
  (arg1: SocketIO.Socket|SocketIO.Namespace, arg2: Array<string|any|Function>|SocketIO.Socket, fn: (err: any) => void);
}
