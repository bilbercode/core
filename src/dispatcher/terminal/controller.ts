
import  { ServiceManager } from "../../service/manager";
import { Request } from "../../request/terminal/request";
import { Response } from "../../request/terminal/response";
import { ITerminalMiddleware } from "./middleware";
import { MetadataStorage } from "../../metadata/storage";
import { CONTROLLER_TERMINAL_ROUTE } from "../../metadata/constants";
import { IMethodDispatcherFactoryOptions } from "../../application/factory/terminal/method";
import { Method } from "./method";
import { isRegExp } from "util";
import { EventEmitter } from "events";

export interface IControllerOptions {
  route?: string;
  middleware?: Array<ITerminalMiddleware|FunctionConstructor>|
    ITerminalMiddleware|FunctionConstructor;
}

export class Controller extends EventEmitter {

  private dispatchers: Array<Method> = [];

  constructor(public readonly dispatchable: Function,
              private serviceLocator: ServiceManager,
              private meta: MetadataStorage,
              private confidence: number,
              private options?: IControllerOptions) {

    super();
    Object.getOwnPropertyNames(this.dispatchable.prototype)
      .forEach(METHOD => {
        if (this.meta.has(CONTROLLER_TERMINAL_ROUTE, this.dispatchable, METHOD)) {

          const ROUTE_DEFINITION = this.meta.get<any>(
            CONTROLLER_TERMINAL_ROUTE, this.dispatchable, METHOD);

          const METHOD_DISPATCHER_OPTIONS: IMethodDispatcherFactoryOptions = {
            method: METHOD,
            confidence: confidence,
            controllerDispatcher: this,
            methodOptions: ROUTE_DEFINITION
          };

          if (this.options && this.options.route) {
            // Prefix the route with the controller route

            if (isRegExp(METHOD_DISPATCHER_OPTIONS.methodOptions.route)) {

              if ((<RegExp>METHOD_DISPATCHER_OPTIONS.methodOptions.route).source === ".*") {
                METHOD_DISPATCHER_OPTIONS.methodOptions.route = new RegExp(
                  `${this.options.route}${(<RegExp>METHOD_DISPATCHER_OPTIONS.methodOptions.route).source}`
                );
              } else {
                METHOD_DISPATCHER_OPTIONS.methodOptions.route = new RegExp(
                  `${this.options.route} ${(<RegExp>METHOD_DISPATCHER_OPTIONS.methodOptions.route).source}`
                );
              }

            } else {
              METHOD_DISPATCHER_OPTIONS.methodOptions.route =
                `${this.options.route} ${METHOD_DISPATCHER_OPTIONS.methodOptions.route}`;
            }
          }

          const METHOD_DISPATCHER = this.serviceLocator
            .get<Method>(Method, METHOD_DISPATCHER_OPTIONS);

          METHOD_DISPATCHER.on("error", (msg) => {
            (<any>this).emit("error", msg);
          });

          this.dispatchers.push(METHOD_DISPATCHER);
        }
      });
  }

  canDispatch(request: Request): boolean {
    for (let i = 0; i < this.dispatchers.length; i++) {
      if (this.dispatchers[i].canDispatch(request)) {
        if (this.dispatchers[i].confidence > request.confidence) {
          request.controller = this;
          request.method = this.dispatchers[i];
          request.confidence = this.dispatchers[i].confidence;
          return true;
        }
      }
    }
    return false;
  }


  dispatch(request: Request, response: Response): Promise<any> {

    return this.execMiddleware(request, response);
  }

  private internalDispatch(request: Request, response: Response, next): void {
    const DISPATCHER_IDX = this.dispatchers.indexOf(request.method);
    this.dispatchers[DISPATCHER_IDX].dispatch(request, response)
      .then(() => next())
      .catch(next);
  }

  /**
   * Execute any middleware that acts upon the route
   */
  private execMiddleware(request: Request, response: Response): Promise<any> {

    return new Promise((resolve, reject) => {

      if (!this.options.middleware) {
        this.options.middleware = [];
      }

      const MIDDLEWARE: Array<Function> =
        Array.isArray(this.options.middleware) ? this.options.middleware :
          [this.options.middleware];

      const I_D = this.internalDispatch.bind(this);
      I_D.priority = 100;

      MIDDLEWARE.push(I_D);

      const MIDDLEWARE_SORTED = MIDDLEWARE.sort((I1: any , I2: any) => {
        const P1 = I1.priority || 0;
        const P2 = I2.priority || 0;

        if (P1 === P2) return 0;

        return P1 > P2 ? 1 : -1;
      });


      const exec = (middleware: Function) => {

        const INTERCEPTOR = this.serviceLocator.has(middleware) ?
          this.serviceLocator.get<Function>(middleware) : middleware;

        return INTERCEPTOR(request, response, (err) => {
          if (err) return reject(err);
          const NEXT = MIDDLEWARE_SORTED[MIDDLEWARE_SORTED.indexOf(middleware) + 1];
          if (NEXT) {

            return exec(NEXT);
          }
          return resolve();
        });
      };

      return exec(MIDDLEWARE_SORTED[0]);
    });

    // return new Promise((resolve, reject) => {
    //
    //   if (!this.options.middleware) {
    //     return resolve();
    //   }
    //
    //   const MIDDLEWARE: Array<Function> = Array.isArray(this.options.middleware) ?
    //     this.options.middleware : [this.options.middleware];
    //
    //   if (MIDDLEWARE.length < 1) {
    //     return resolve();
    //   }
    //
    //   const exec = (middleware: Function) => {
    //
    //     const INTERCEPTOR: Function = this.serviceLocator.has(middleware) ?
    //       this.serviceLocator.get<Function>(middleware) : middleware;
    //
    //     return INTERCEPTOR(request, response, (err) => {
    //       if (err) return reject(err);
    //       const NEXT = MIDDLEWARE[MIDDLEWARE.indexOf(middleware) + 1];
    //       if (NEXT) {
    //
    //         return exec(NEXT);
    //       }
    //       return resolve();
    //     });
    //   };
    //
    //   return exec(MIDDLEWARE[0]);
    // });
  }
}
