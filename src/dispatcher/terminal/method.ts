import { Controller } from "./controller";
import { ITerminalMiddleware } from "./middleware";
import { Request } from "../../request/terminal/request";
import { Response } from "../../request/terminal/response";
import { ServiceManager } from "../../service/manager";
import { MetadataStorage } from "../../metadata/storage";
import { ARGUMENT } from "../../metadata/constants";
import { Context } from "../../request/argument/context";
import { Context as RequestContext } from "../../request/terminal/context";
import { Form } from "../../request/argument/form";
import { Observable } from "rxjs/Observable";
import * as Ejs from "ejs";
import * as Fs from "fs";
import { isRegExp } from "util";
import { Form as FormInstance} from "../../form/form";
import { EventEmitter } from "events";
import { IMiddleware } from "../middleware";



function isPromise<T>(value: any): value is Promise<T> {
  return !!(value instanceof Promise);
}

function isObservable<T>(value: any): value is Observable<T> {
  if ("subscribe" in value) {
    return true;
  }

  return false;
}

function isRequestContext(value: any): value is Context {
  return !!(value instanceof Context);
}

function isRequestForm(value: any): value is Form {
  return !!(value instanceof Form);
}


export interface IMethodOptions {
  route: string|RegExp;
  template?: string|Promise<string>;
  templatePath?: string;
  middleware?: Array<ITerminalMiddleware|FunctionConstructor>|
    ITerminalMiddleware|FunctionConstructor;
}

export class Method extends EventEmitter {

  public readonly confidence: number;

  private route: RegExp;

  private routeParams: {[idx: number]: string} = {};

  private methodParams: Array<Context|Form> = [];

  private templateStringPromise: Promise<string>;

  private controllerDispatcher: Controller;

  private serviceLocator: ServiceManager;

  private meta: MetadataStorage;

  private method: string;

  private options: IMethodOptions;


  constructor(controllerDispatcher: Controller,
              serviceLocator: ServiceManager,
              meta: MetadataStorage,
              method: string,
              options: IMethodOptions,
              confidence: number) {
    super();
    this.controllerDispatcher = controllerDispatcher;
    this.serviceLocator = serviceLocator;
    this.meta = meta;
    this.method = method;
    this.options = options;
    this.confidence = confidence;

    const ROUTE = isRegExp(options.route) ? options.route :
      new RegExp((<string>options.route).replace(/(\<\w+\>)/g, "([\\w\\-_]+)")
      .replace(/(\[\w+\])/g, "(\\%optional%+)?")
      .replace(/(\s+)/g, "\\s+")
      .replace("(\\%optional%+)?", "(?:\\s+([\\w\\-_]+)\\s*)?")
      .replace("\\s+(?:\\s+([\\w\\-_]+)\\s*)?", "(?:\\s+([\\w\\-_]+)\\s*)?" ) + "$", "g");

    this.route = <RegExp>ROUTE;

    const PARAMS: Array<string> = isRegExp(options.route) ?
      null : (<string>options.route).match(/((?:<|\[)[\w\-_]+(?:>|\]))/g);

    if (PARAMS) {
      for (let i = 0; i < PARAMS.length; i++) {
        this.routeParams[i + 1] = PARAMS[i].replace(/(?:(?:<|\[)([\w\-_]+)(?:>|\]))/, "$1");
      }
    }

    this.resolveMethodArguments();

    if (this.options.template) {
      this.templateStringPromise = Promise.resolve<string>(this.options.template);
    } else {
      this.templateStringPromise = Promise.resolve<string>(this.options.templatePath)
        .then(PATH => {
          return new Promise((resolve, reject) => {
            Fs.readFile(PATH, (err, chunk) => {
              if (err) return reject(err);
              return resolve(chunk.toString("utf8"));
            });
          });
        });
    }

  }


  canDispatch(request: Request): boolean {
    return !!request.raw.split(/\s+--?.*/)[0].match(this.route);
  }

  dispatch(request: Request, response: Response): Promise<any> {

    return this.execMiddleware(request, response);
  }

  private internalDispatch(request: Request, response: Response, next: (err?: Error) => void): void {

    const CONTROLLER = this.serviceLocator.get(this.controllerDispatcher.dispatchable);

    this.resolveContextMethodArguments(request, response)
      .then(ARGUMENTS => {
        let OUTCOME;

        try {
          OUTCOME = CONTROLLER[this.method](...ARGUMENTS);
        } catch (e) {
          return Promise.reject(e);
        }

        if (isPromise(OUTCOME)) {
          return OUTCOME.then<string>(VALUES => this.renderTemplate(VALUES))
            .then(OUTPUT => response.write(new Buffer(OUTPUT, "utf8")))
            .then(() => {
              response.flush();
              next();
            });

        } else if (isObservable(OUTCOME)) {
          OUTCOME.subscribe(VALUES => {
            const OUTPUT = this.renderTemplate(VALUES)
              .then(T_OUTPUT => response.write(new Buffer(T_OUTPUT, "utf8")))
              .then(() => response.flush())
              .catch(next);
          }, (e) => next(e), () => next());

        } else {
          return this.renderTemplate(OUTCOME)
            .then(OUTPUT => response.write(new Buffer(OUTPUT, "utf8")))
            .then(() => response.flush())
            .then(() => next());
        }
      }).catch(next);
  }

  private resolveContextMethodArguments(request: Request, response: Response): Promise<any> {

    const CONTEXT = this.serviceLocator.get<RequestContext>(RequestContext, {
      routeRegExp: this.route,
      routeParams: this.routeParams,
      request: request,
      response: response
    });

    return Promise.resolve(this.methodParams)
      .then(PARAMS => {
        const PARAMS_ASYNC = PARAMS.map(PARAM => {
          return Promise.resolve<Context|Form>(PARAM)
            .then(P => {
              if (isRequestForm(P)) {
                const MODEL_PROTOTYPE = P.model;
                const FORM: FormInstance = this.serviceLocator
                  .get<FormInstance>(MODEL_PROTOTYPE);
                return FORM.isValid(CONTEXT)
                  .then(OUTCOME => {
                    if (OUTCOME === true) {
                      return FORM.getObject();
                    }

                    throw OUTCOME;
                  });
              }
              if (isRequestContext(P)) {
                switch (P.name) {
                  case "context":
                    return CONTEXT;
                  case "query":
                    return CONTEXT.getQueryParam(P.expression);
                  case "route":
                    return CONTEXT.getRouteParam(P.expression);
                  case "body":
                    return CONTEXT.getBodyParam(P.expression);
                  case "addon":
                    return CONTEXT.get(P.expression);
                  case "timer":
                    return CONTEXT.getTimer();
                  default:
                    // Terminal Controllers are unable to pass headers et.al.
                    return null;

                }
              }

              const CONTROLLER_NAME = this.controllerDispatcher.
                dispatchable.prototype.constructor.name;

              throw new Error("Unknown request argument in " +
                `"${CONTROLLER_NAME}::${this.method}" - ` + P);
            });
        });

        return Promise.all(PARAMS_ASYNC);
      });
  }


  private execMiddleware(request: Request, response: Response): Promise<any> {

    return new Promise((resolve, reject) => {

      if (!this.options.middleware) {
        this.options.middleware = [];
      }

      const MIDDLEWARE: Array<Function> =
        Array.isArray(this.options.middleware) ? this.options.middleware :
          [this.options.middleware];

      const I_D = this.internalDispatch.bind(this);
      I_D.priority = 100;

      MIDDLEWARE.push(I_D);

      const MIDDLEWARE_SORTED = MIDDLEWARE.sort((I1: any , I2: any) => {
        const P1 = I1.priority || 0;
        const P2 = I2.priority || 0;

        if (P1 === P2) return 0;

        return P1 > P2 ? 1 : -1;
      });


      const exec = (middleware: Function) => {

        const INTERCEPTOR = this.serviceLocator.has(middleware) ?
          this.serviceLocator.get<Function>(middleware) : middleware;

        return INTERCEPTOR(request, response, (err) => {
          if (err) return reject(err);
          const NEXT = MIDDLEWARE_SORTED[MIDDLEWARE_SORTED.indexOf(middleware) + 1];
          if (NEXT) {

            return exec(NEXT);
          }
          return resolve();
        });
      };

      return exec(MIDDLEWARE_SORTED[0]);
    });
  }

  private resolveMethodArguments() {
    this.methodParams =
      this.meta.has(ARGUMENT, this.controllerDispatcher.dispatchable, this.method) ?
      this.meta
        .get<Array<Context|Form>>(ARGUMENT, this.controllerDispatcher.dispatchable, this.method) :
        [];
  }

  private renderTemplate(values: any): Promise<string> {
    return Promise.resolve(this.templateStringPromise)
      .then(TEMPLATE_STRING => Ejs.compile(TEMPLATE_STRING))
      .then(TEMPLATE => TEMPLATE(values));
  }

}
