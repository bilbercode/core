import { Request } from "../../request/terminal/request";
import { Response } from "../../request/terminal/response";

export interface ITerminalMiddleware {
  (request: Request, response: Response, next: (err?: Error) => void): void;
}
