import { RequestHandler, ErrorRequestHandler } from "express";

export interface IHttpMiddleware  extends RequestHandler { }

export interface IHttpErrorMiddleware extends ErrorRequestHandler { }
