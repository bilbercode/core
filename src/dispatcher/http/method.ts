import "source-map-support/register";
import { ServiceManager } from "../../service/manager";
import { Router } from "../../request/http/router";
import { IHttpMiddleware } from "./middleware";
import { Request } from "../../request/http/request";
import { Response } from "../../request/http/response";
import { Next } from "../../request/http/next";
import { Context as HttpContext } from "../../request/http/context";
import { Context as RequestContext } from "../../request/argument/context";
import { Form } from "../../request/argument/form";
import { Form as FormInstance } from "./../../form/form";
import { Observable } from "rxjs/Observable";
import { Handler } from "../../content-negotiaion/handler";
import { EventEmitter } from "events";
import { json, urlencoded, raw } from "body-parser";


function isRequestContext(value: any): value is RequestContext {
  return !!(value instanceof RequestContext);
}

function isRequestForm(value: any): value is Form {
  return !!(value instanceof Form);
}

function isPromise<T>(value: any): value is Promise<T> {
  return !!(value instanceof Promise);
}

function isObservable<T>(value: any): value is Observable<T> {
  if ("subscribe" in value) {
    return true;
  }

  return false;
}

export class Method extends EventEmitter {

  constructor(public readonly router: Router,
              public readonly routePath: string,
              public readonly verb: string,
              public readonly methodParams: Array<RequestContext|Form>,
              private readonly controller: Function,
              private readonly methodName,
              middleware: Array<IHttpMiddleware>,
              private readonly serviceLocator: ServiceManager) {

    super();
    this.initMiddleware(middleware);
    this.router[verb.toLowerCase()](routePath, this.dispatch.bind(this));
  }

  dispatch(request: Request, response: Response, next: Next): Promise<any> {

    const REQUEST_CONTEXT = this.serviceLocator.get<HttpContext>(HttpContext, {
      request: request,
      response: response,
      next: next
    });

    const RESPONSE_HANDLER: Handler = response["contentNegotiation"];

    const ARGS = [];
    return Promise.resolve(this.methodParams)
      .then<any>(PARAMS => {
        const PARAMS_ASYNC = PARAMS.map(PARAM => {
          return Promise.resolve<RequestContext|Form>(PARAM)
            .then(P => {
              if (isRequestForm(P)) {
                const MODEL_PROTOTYPE = P.model;
                const MODEL_INSTANCE = this.serviceLocator.get(MODEL_PROTOTYPE);
                const FORM: FormInstance = this.serviceLocator.get<FormInstance>
                (MODEL_PROTOTYPE);

                return FORM.isValid(REQUEST_CONTEXT)
                  .then(OUTCOME => {
                    if (OUTCOME === true) {
                      return FORM.getObject();
                    }

                    throw {
                      code: 400,
                      errors: OUTCOME
                    };
                  });
              }
              if (isRequestContext(P)) {
                switch (P.name) {
                  case "context":
                    return REQUEST_CONTEXT;
                  case "query":
                    return REQUEST_CONTEXT.getQueryParam(P.expression);
                  case "route":
                    return REQUEST_CONTEXT.getRouteParam(P.expression);
                  case "header":
                    return REQUEST_CONTEXT.getHeaderParam(P.expression);
                  case "cookie":
                    return REQUEST_CONTEXT.getCookieParam(P.expression);
                  case "body":
                    return REQUEST_CONTEXT.getBodyParam(P.expression);
                  case "addon":
                    return REQUEST_CONTEXT.get(P.expression);
                  case "timer":
                    return REQUEST_CONTEXT.getTimer();
                  default:
                    // Terminal Controllers are unable to pass headers et.al.
                    return null;

                }
              }

              const CONTROLLER_NAME = this.controller.prototype.constructor.name;

              throw new Error("Unknown request argument in " +
                `"${CONTROLLER_NAME}::${this.methodName}" - ` + P);
            });
        });

        return Promise.all(PARAMS_ASYNC);
      }).then<any>(PARAMS => {

        const CONTROLLER = this.serviceLocator.get(this.controller);

        let OUTCOME: any;
        try {
          OUTCOME = CONTROLLER[this.methodName](...PARAMS);
        } catch (e) {
          return Promise.reject(e);
        }

        if (isPromise(OUTCOME)) {

          return OUTCOME.then(VALUES => RESPONSE_HANDLER.send(VALUES, request, response, next));

        } else if (isObservable(OUTCOME)) {

          return new Promise((resolve, reject) => {
            OUTCOME.subscribe(VALUES => {
              RESPONSE_HANDLER.send(VALUES, request, response, next);
              resolve(PARAMS);
            }, (err) => {
              reject(err);
            });
          });
        } else {
          RESPONSE_HANDLER.send(OUTCOME, request, response, next);
          return Promise.resolve(PARAMS);
        }
      }).catch(e => {
        if (e.code) {
          if (e.errors) {
            response.status(e.code).send(e.errors);
          } else {
            response.status(e.code).send(e.message);
          }
        } else {
          response.status(500).send();
          this.emit("error", e);
        }
      });
  }

  private initMiddleware(middlewareList: Array<IHttpMiddleware>): void {
    switch (this.verb.toUpperCase()) {
      case "POST":
      case "PUT":
      case "PATCH":
      case "DELETE":
        this.router[this.verb.toLowerCase()](this.routePath, json({}));
        this.router[this.verb.toLowerCase()](this.routePath, urlencoded({ extended: false }));
        this.router[this.verb.toLowerCase()](this.routePath, raw({}));
        break;
      default:
        break;
    }
    middlewareList.forEach(MIDDLEWARE_FN => {

      const MIDDLEWARE = this.serviceLocator.has(MIDDLEWARE_FN) ?
        this.serviceLocator.get(MIDDLEWARE_FN) : MIDDLEWARE_FN;

      this.router[this.verb.toLowerCase()](this.routePath, MIDDLEWARE);
    });
  }

}
