import { Method } from "./method";
import { Router } from "../../request/http/router";
import { IHttpMiddleware, IHttpErrorMiddleware } from "./middleware";
import { EventEmitter } from "events";

export interface IControllerOptions {
  route: string;
  middleware?: Array<any>|any;
}

export class Controller extends EventEmitter {

  constructor(public readonly router: Router,
              private readonly dispatchers: Array<Method>,
              public readonly path: string,
              middleware: Array<IHttpMiddleware|IHttpErrorMiddleware>) {

    super();

    middleware.forEach((MIDDLEWARE: any) => {
      router.use(MIDDLEWARE);
    });

    dispatchers.forEach((DISPATCHER: any) => {
      DISPATCHER.on("error", (msg) => {
        this.emit("error", msg);
      });
      if (path) {
        router.use(path, DISPATCHER.router);
      } else {
        router.use(DISPATCHER.router);
      }
    });
  }
}
