import { ServiceManager } from "./manager";

export interface IFactory {
  createService<T>(serviceLocator: ServiceManager, options?: any, constructor?: Function): T;
}
