import "source-map-support/register";
import { Injector } from "../di/injector";
import { MetadataStorage } from "../metadata/storage";
/**
 * Loop counter used to detect cyclic dependencies between the DI Injector and service locator
 * @type {WeakMap<symbol, number>}
 */
const LOOP_DETECTOR: Map<symbol, number> = new Map<symbol, number>();


function isRequestingSelf(value: any): value is ServiceManager {
  if (!!(value === ServiceManager)){
    return true;
  }

  return false;
}

export class ServiceManager {

  public readonly id: symbol;

  private instances: Map<any, any> = new Map<any, any>();

  private factories: Map<any, FunctionConstructor | Function> =
    new Map<any, FunctionConstructor | Function>();

  private shared: Map<Function, boolean> = new Map<Function, boolean>();

  private peeringManagers: Array<ServiceManager> = [];

  private _peeringManager: ServiceManager;

  get peeringManager(): ServiceManager {
    return this._peeringManager;
  }

  private di: Injector;

  constructor(private metadata: MetadataStorage) {
    this.di = new Injector(this, metadata);
    this.id = Symbol("ServiceManager");
  }


  has(target: any, ignoreDI = false): boolean {

    if (isRequestingSelf(target)) {
      return true;
    }

    let loopCounter = arguments[3];

    if (!loopCounter) {
      loopCounter = Symbol("LOOP_COUNTER");
      LOOP_DETECTOR.set(loopCounter, 0);
    } else {
      LOOP_DETECTOR.set(loopCounter, LOOP_DETECTOR.get(loopCounter) + 1);
    }

    if (LOOP_DETECTOR.get(loopCounter) > 99) {
      LOOP_DETECTOR.delete(loopCounter);
      throw new Error("Cyclic dependency detected!");
    }

    /**
     * Check if we already have a shared instance available ;)
     */
    if (this.instances.has(target)) {
      LOOP_DETECTOR.delete(loopCounter);
      return true;
    }

    /**
     * Check with the peering service first
     */
    if (this.peeringManager && this.peeringManager.has(target)) {
      LOOP_DETECTOR.delete(loopCounter);
      return true;
    }

    if (this.hasPeeringManager()) {
      for (let i = 0; i < this.peeringManagers.length; i++) {
        const PM_HAS = this.peeringManagers[i]
          .has.call(this.peeringManagers[i], target, ignoreDI, loopCounter);
        if (PM_HAS) {
          LOOP_DETECTOR.delete(loopCounter);
          return true;
        }
      }
    }

    /**
     * Check if we have a factory registered for it
     */
    if (this.factories.has(target)) {
      LOOP_DETECTOR.delete(loopCounter);
      return true;
    }

    /**
     * Last ditch check if the DI can instantiate
     * the thing
     */
    if (!ignoreDI && this.di.canInstantiate(target, loopCounter)) {
      LOOP_DETECTOR.delete(loopCounter);
      return true;
    }

    return false;
  }

  set(target: any, instance: any): ServiceManager {
    this.instances.set(target, instance);
    return this;
  }

  get<T>(target: any, options?: any): T {

    if (isRequestingSelf(target)) {
      return (<any>this);
    }

    let loopCounter = arguments[3];

    if (!loopCounter) {
      loopCounter = Symbol("LOOP_COUNTER");
      LOOP_DETECTOR.set(loopCounter, 0);
    } else {
      LOOP_DETECTOR.set(loopCounter, LOOP_DETECTOR.get(loopCounter) + 1);
    }

    if (LOOP_DETECTOR.get(loopCounter) > 99) {
      LOOP_DETECTOR.delete(loopCounter);
      throw new Error("Cyclic dependency detected!");
    }

    let instance;

    let pmHas: boolean|ServiceManager = false;

    if (this.hasPeeringManager()) {
      for (let i = 0; i < this.peeringManagers.length; i++) {
        const PM_HAS = this.peeringManagers[i]
          .has.call(this.peeringManagers[i], target, true, loopCounter);
        if (PM_HAS) {
          pmHas = this.peeringManagers[i];
        }
      }
    }

    // Check the peering Manager first
    if (this.peeringManager && this.peeringManager.has(target, true)) {
      instance = this.peeringManager.get(target, options);
    } else if (pmHas !== false) {
      instance = (<ServiceManager>pmHas).get(target, options);
    } else {
      /**
       * If we have a shared instance return it
       */
      if (this.instances.has(target)) {
        return this.instances.get(target);
      }

      if (this.factories.has(target)) {
        const factory = <any>this.factories.get(target);
        const FACTORY_INSTANCE = this.get(factory);
        instance = (<any>FACTORY_INSTANCE).createService(this, options, target);
      } else if (this.di.canInstantiate(target, loopCounter)) {
        instance = this.di.instantiate(target, loopCounter);
      }
    }

    if (!instance) {
      LOOP_DETECTOR.delete(loopCounter);
      throw new Error(`Unable to retrieve an instance of ` +
        `${target.prototype ? String(target) : target.constructor}`);
    }

    if (this.shared.has(target) && this.shared.get(target)) {
      this.instances.set(target, instance);
    }

    LOOP_DETECTOR.delete(loopCounter);
    return instance;
  }

  setPeeringManager(manager: ServiceManager): ServiceManager {

    /**
     * Check the developer is not attempting to create a singularity
     */
    if (manager === this) {
      throw new Error("Attempt to introduce cyclic peering manager, unable to set self as" +
        " peering manager");
    }

    /**
     * Check that the developer is not trying to create a wormhole
     */
    let candidate = manager;
    while (!!candidate.peeringManager) {
      if (candidate === this) {
        throw new Error("Attempt to introduce cyclic peering manager, a parent manager " +
          "introduces a cyclic reference");
      }
      candidate = candidate.peeringManager;
    }

    // All good
    this._peeringManager = manager;
    return this;
  }

  /**
   * Flag an instance to be shared
   *
   * @param {Function} target The Object to be shared when instantiated
   * @param {boolean} shared The boolean flag indicating the share status of an instance
   * @returns {Manager}
   */
  setShared(target: Function, shared = true): ServiceManager {
    this.shared.set(target, shared);
    return this;
  }

  /**
   * Registers a factory for an instance
   * @param {FunctionConstructor} target The target instance prototype
   * @param {IServiceFactory} factory The factory function
   * @returns {Manager}
   */
  registerFactory(target: any, factory: FunctionConstructor|Function): ServiceManager {
    this.factories.set(target, factory);
    return this;
  }

  /**
   * Check if there are any peering service locators registered
   * @returns {boolean}
   */
  public hasPeeringManager(peeringManager?: ServiceManager): boolean {
    if (peeringManager) {
      return !(this.peeringManagers.indexOf(peeringManager) < 0);
    }
    return this.peeringManagers.length > 0;
  }

  /**
   * Add a peering service manager to the instance
   * @param peeringManager
   * @returns {ServiceManager}
   */
  public addPeeringManager(peeringManager: ServiceManager): ServiceManager {
    if (this.hasPeeringManager(peeringManager)) {
      throw new Error(`Attempt to add duplicate peering service manager`);
    }
    this.peeringManagers.push(peeringManager);
    return this;
  }

  /**
   * Remove a peering manager from the instance
   * @param peeringManager
   * @returns {ServiceManager}
   */
  public removePeeringServiceManager(peeringManager: ServiceManager): ServiceManager {
    if (this.hasPeeringManager(peeringManager)) {
      this.peeringManagers.splice(this.peeringManagers.indexOf(peeringManager), 1);
    }
    return this;
  }
}
