
import { ServiceManager } from "./manager";
import { MetadataStorage as Metadata } from "../metadata/storage";


export class WhitelistedServiceManager extends ServiceManager {

  private whitelist: Array<Function|FunctionConstructor|symbol>;

  constructor(metadata: Metadata,
              whitelist?: Array<Function|FunctionConstructor|symbol>) {
    super(metadata);
    this.whitelist = whitelist || [];
  }


  has(...args: any[]): boolean {
    const TARGET = args[0];
    if (this.whitelist.indexOf(TARGET) < 0) {
      return false;
    }
    return this.peeringManager.has.call(this.peeringManager, TARGET, false);
  }


  get<T>(...args: any[]): T {
    const TARGET = args[0];
    if (!this.has(TARGET, true)) {
      throw new Error(`Unable to retrieve an instance of ` +
        `${TARGET.prototype ? String(TARGET) : TARGET.constructor}`);
    }
    return this.peeringManager.get.apply(this.peeringManager, args);
  }

  setWhitelist(whitelist: Array<Function|FunctionConstructor|symbol>): WhitelistedServiceManager {
    this.whitelist = whitelist;
    return this;
  }
}
