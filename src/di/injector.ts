import { ServiceManager } from "../service/manager";
import { MetadataStorage } from "../metadata/storage";
import { DI_INJECTABLE } from "../metadata/constants";
import "source-map-support/register";

export class Injector {


  constructor(private serviceLocator: ServiceManager,
              private metadata: MetadataStorage) { }


  canInstantiate(constructor: FunctionConstructor, context: symbol): boolean {

    if (this.metadata.has(DI_INJECTABLE, constructor, this.serviceLocator.id)) {

      const locatedDeps =  (this.metadata.has(DI_INJECTABLE, constructor, this.serviceLocator.id) ?
        this.metadata.get<Array<any>>(DI_INJECTABLE, constructor, this.serviceLocator.id) : [])
        .map(dep => this.serviceLocator.has.call(this.serviceLocator, dep, false, context))
        .filter(d => !!d);

      if (locatedDeps.length >= constructor.length) {
        return true;
      }

    }

    return false;
  }

  instantiate(constructor: FunctionConstructor, context: symbol): any {
    const deps = this.metadata.has(DI_INJECTABLE, constructor, this.serviceLocator.id) ?
      this.metadata.get<Array<any>>(DI_INJECTABLE, constructor, this.serviceLocator.id) : [];


    const ARGS = deps
      .map((dep => this.serviceLocator.get.call(this.serviceLocator, dep, null, context)));
    return new constructor(...ARGS);
  }
}
