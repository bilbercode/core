"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
var injectable_1 = require("../decorator/injectable");
var DefaultLog = (function () {
    function DefaultLog() {
    }
    DefaultLog.prototype.info = function (line) {
        process.stdout.write("[INFO ] " + line + "\n");
        return this;
    };
    DefaultLog.prototype.debug = function (line) {
        process.stdout.write("[DEBUG] " + line + "\n");
        return this;
    };
    DefaultLog.prototype.error = function (line) {
        process.stderr.write("[ERROR] " + line + "\n");
        return this;
    };
    DefaultLog.prototype.warn = function (line) {
        process.stdout.write("[WARN ] " + line + "\n");
        return this;
    };
    return DefaultLog;
}());
DefaultLog = __decorate([
    injectable_1.Injectable()
], DefaultLog);
exports.DefaultLog = DefaultLog;
//# sourceMappingURL=default.js.map