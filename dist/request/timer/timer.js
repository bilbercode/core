"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var event_1 = require("./event");
var Timer = (function () {
    function Timer() {
        this.events = [];
        this.start = new Date();
    }
    Timer.prototype.mark = function (event) {
        this.events.push(new event_1.Event(event, (new Date()).getTime() - this.start.getTime()));
    };
    return Timer;
}());
exports.Timer = Timer;
//# sourceMappingURL=timer.js.map