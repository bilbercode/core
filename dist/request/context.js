"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
var timer_1 = require("./timer/timer");
var AbstractContext = (function () {
    function AbstractContext() {
        this.timer = new timer_1.Timer();
    }
    AbstractContext.prototype.eval = function (object, expression) {
        var clone = object;
        var pointers = expression.split(/\./g);
        while ((clone = clone[pointers.shift()]) && pointers.length)
            ;
        return util_1.isNullOrUndefined(clone) ? null : clone;
    };
    return AbstractContext;
}());
exports.AbstractContext = AbstractContext;
//# sourceMappingURL=context.js.map