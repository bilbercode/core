"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../context");
var Context = (function (_super) {
    __extends(Context, _super);
    function Context(connectionID, namespace, socket, payload) {
        var _this = _super.call(this) || this;
        _this.connectionID = connectionID;
        _this.namespace = namespace;
        _this.socket = socket;
        _this.payload = payload;
        _this.type = "socket";
        return _this;
    }
    Context.prototype.getQueryParam = function (param) {
        return this.eval(this.socket.handshake.query, param);
    };
    Context.prototype.getBodyParam = function (param) {
        return this.eval(this.payload, param);
    };
    Context.prototype.getHeaderParam = function (param) {
        return this.eval(this.socket.handshake.headers, param);
    };
    Context.prototype.getCookieParam = function (param) {
        return this.eval(this.socket.handshake.headers, param); // TODO revliadate
    };
    Context.prototype.getRouteParam = function (param) {
        return this.eval(this.payload, param);
    };
    Context.prototype.get = function (param) {
        return this.eval(this.socket, param);
    };
    Context.prototype.getTimer = function () {
        return this.timer;
    };
    return Context;
}(context_1.AbstractContext));
exports.Context = Context;
//# sourceMappingURL=context.js.map