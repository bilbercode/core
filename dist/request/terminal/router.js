"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request_1 = require("./request");
var response_1 = require("./response");
var Router = (function () {
    function Router(serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.dispatchers = [];
    }
    Router.prototype.registerDispatcher = function (dispatcher) {
        if (this.dispatchers.indexOf(dispatcher) < 0) {
            this.dispatchers.push(dispatcher);
        }
        return this;
    };
    Router.prototype.dispatch = function (argv) {
        var _this = this;
        var REQUEST = this.serviceLocator.get(request_1.Request, { argv: argv });
        return new Promise(function (resolve, reject) {
            var dispatcher;
            for (var i = 0; i < _this.dispatchers.length; i++) {
                if (_this.dispatchers[i].canDispatch(REQUEST)) {
                    dispatcher = _this.dispatchers[i];
                }
            }
            if (dispatcher) {
                var RESPONSE = _this.serviceLocator.get(response_1.Response);
                return dispatcher.dispatch(REQUEST, RESPONSE)
                    .then(resolve)
                    .catch(reject);
            }
            reject(new Error("Unable to dispatch request"));
        });
    };
    return Router;
}());
exports.Router = Router;
//# sourceMappingURL=router.js.map