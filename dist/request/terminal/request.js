"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Request = (function () {
    function Request(query, raw) {
        this.query = query;
        this.raw = raw;
        this.confidence = 0;
        this.params = {};
        this.headers = {};
    }
    return Request;
}());
exports.Request = Request;
//# sourceMappingURL=request.js.map