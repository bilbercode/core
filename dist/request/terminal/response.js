"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var stream_1 = require("stream");
var Response = (function (_super) {
    __extends(Response, _super);
    function Response(opts, stdout, stderr) {
        var _this = _super.call(this, opts || {}) || this;
        _this.responseCode = 0;
        _this.buffer = new Buffer([]);
        _this.stdout = stdout || process.stdout;
        _this.stderr = stderr || process.stderr;
        _this.on("finish", function () {
            _this.flush();
        });
        return _this;
    }
    Response.prototype.flush = function () {
        if (this.responseCode !== 0) {
            this.stderr.write(this.buffer);
        }
        else {
            this.stdout.write(this.buffer);
        }
        this.buffer = new Buffer([]);
    };
    Response.prototype._write = function (chunk, encoding, callback) {
        if (Buffer.isBuffer(chunk)) {
            this.buffer = Buffer.concat([this.buffer, chunk]);
        }
        else {
            this.buffer = Buffer.concat([this.buffer, new Buffer(chunk, encoding)]);
        }
        if (callback) {
            return callback();
        }
    };
    return Response;
}(stream_1.Writable));
exports.Response = Response;
//# sourceMappingURL=response.js.map