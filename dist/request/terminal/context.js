"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../context");
var util_1 = require("util");
var Context = (function (_super) {
    __extends(Context, _super);
    function Context(routeParams, routeRegExp, request, response) {
        var _this = _super.call(this) || this;
        _this.routeParams = routeParams;
        _this.routeRegExp = routeRegExp;
        _this.request = request;
        _this.response = response;
        _this.type = "terminal";
        return _this;
    }
    Context.prototype.getQueryParam = function (param) {
        return this.request.query[param];
    };
    Context.prototype.getBodyParam = function (param) {
        return this.eval(this.request.body, param);
    };
    Context.prototype.getHeaderParam = function (param) {
        return this.eval(this.request.headers, param);
    };
    Context.prototype.getCookieParam = function (param) {
        return null;
    };
    Context.prototype.get = function (param) {
        return this.eval(this.request, param);
    };
    Context.prototype.getRouteParam = function (param) {
        this.routeRegExp.lastIndex = 0;
        var ROUTE_MATCHES = this
            .routeRegExp.exec(this.request.raw.split(/\s+--?.*/)[0]);
        if (util_1.isNumber(param)) {
            return ROUTE_MATCHES[param] || null;
        }
        var IDXS = Object.keys(this.routeParams);
        for (var i = 0; i < IDXS.length; i++) {
            if (this.routeParams[IDXS[i]] === param) {
                return ROUTE_MATCHES[IDXS[i]];
            }
        }
        return null;
    };
    Context.prototype.getTimer = function () {
        return this.timer;
    };
    return Context;
}(context_1.AbstractContext));
exports.Context = Context;
//# sourceMappingURL=context.js.map