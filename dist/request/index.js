"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("./terminal/context");
var context_2 = require("./http/context");
var context_3 = require("./socket/context");
var Request;
(function (Request) {
    Request.Terminal = context_1.Context;
    Request.Http = context_2.Context;
    Request.Socket = context_3.Context;
})(Request = exports.Request || (exports.Request = {}));
//# sourceMappingURL=index.js.map