"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Context = (function () {
    function Context(service, expression) {
        this.service = service;
        this.expression = expression;
        this.name = service.toString().replace(/^Symbol\((.*)\)$/, "$1");
    }
    return Context;
}());
exports.Context = Context;
//# sourceMappingURL=context.js.map