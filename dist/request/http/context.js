"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../context");
var Context = (function (_super) {
    __extends(Context, _super);
    function Context(request, response, next) {
        var _this = _super.call(this) || this;
        _this.request = request;
        _this.response = response;
        _this.next = next;
        _this.type = "http";
        return _this;
    }
    Context.prototype.getQueryParam = function (param) {
        return this.request.query[param] || null;
    };
    Context.prototype.getBodyParam = function (param) {
        return this.eval(this.request.body, param);
    };
    Context.prototype.getHeaderParam = function (param) {
        return this.request.headers[param] || null;
    };
    Context.prototype.getCookieParam = function (param) {
        return this.request.cookies[param] || null;
    };
    Context.prototype.getRouteParam = function (param) {
        return this.request.params[param] || null;
    };
    Context.prototype.get = function (param) {
        return this.eval(this.request, param);
    };
    Context.prototype.getTimer = function () {
        return this.timer;
    };
    return Context;
}(context_1.AbstractContext));
exports.Context = Context;
//# sourceMappingURL=context.js.map