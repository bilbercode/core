"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
var Error = (function () {
    function Error(message, code) {
        if (code === void 0) { code = 500; }
        this.message = message;
        this.code = code;
    }
    return Error;
}());
exports.Error = Error;
function errorHandler(err, request, response, next) {
    var CODE = util_1.isNullOrUndefined(err.code) ? 500 : err.code;
    var MESSAGE = util_1.isNullOrUndefined(err.message) ? "Server Error" : err.message;
    response.status(CODE);
    if (response["contentNegotiation"]) {
        response["contentNegotiation"].send({
            code: CODE,
            message: MESSAGE
        });
    }
    else {
        response.send({
            code: CODE,
            message: MESSAGE
        });
    }
    next(err);
}
exports.errorHandler = errorHandler;
//# sourceMappingURL=error.js.map