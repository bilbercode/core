"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * I18n translation handler
 *
 * Added to the response object by the content negotiation.
 */
var I18n = (function () {
    function I18n(translator, locale) {
        this.translator = translator;
        this.locale = locale;
    }
    /**
     * Translates a string according to the current Language requested
     * by the client
     *
     * @example A single translation
     *
     * ```
     * i18n.translate('Hello world');
     * ```
     *
     * @example A pluralised translation
     * ```
     * i18n.translate('%d item', '%d items', 12)
     * ```
     *
     * @param {string} str The string to translate
     * @param {string} plural The plural of the string to translate if not already
     *  defined by the translation files
     * @param {number} count The number to pluralise to
     * @returns {string}
     */
    I18n.prototype.translate = function (str, plural, count) {
        if (plural) {
            count = count || 1;
            return this.translator.plural(this.locale, str, plural, count);
        }
        return this.translator.translate(this.locale, str);
    };
    return I18n;
}());
exports.I18n = I18n;
//# sourceMappingURL=i18n.js.map