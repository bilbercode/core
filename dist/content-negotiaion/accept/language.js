"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Language
 *
 * Represents a Accept-Language directive supplied by the
 * client
 */
var Language = (function () {
    function Language(META) {
        var WEIGHT = 0;
        var PARTS = META.split(";");
        var I18N_L10N = PARTS[0]
            .trim()
            .toLowerCase()
            .match(/^([a-z]{2}|\*)(?:-([a-z]{2}))?/);
        this.internationalization = I18N_L10N[1];
        if (I18N_L10N[2]) {
            this.localization = I18N_L10N[2];
            WEIGHT++;
        }
        this.weight = WEIGHT;
        if (PARTS[1]) {
            var QUALITY = PARTS[1].trim().replace(/^q=(0\.\d|1)/, "$1");
            this.quality = parseFloat(QUALITY);
        }
    }
    /**
     * Returns a string representation of the directive without the
     * quality rating
     */
    Language.prototype.toString = function () {
        if (this.localization) {
            return this.internationalization + "-" + this.localization;
        }
        return this.internationalization;
    };
    return Language;
}());
exports.Language = Language;
//# sourceMappingURL=language.js.map