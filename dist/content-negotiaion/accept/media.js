"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Media
 *
 * Represents a Accept-Language directive supplied by the
 * client
 */
var Media = (function () {
    function Media(MIME) {
        /**
         * A set of parameters that can be included alongside the MIME
         * as defined when the MIME was registered with IANA
         */
        this.params = {};
        var WEIGHT = 0;
        var parts = MIME.split(";")
            .map(function (p) {
            return p.trim();
        });
        var MEDIA_RANGE = parts
            .shift()
            .split("/")
            .map(function (p) {
            if (p !== "*") {
                WEIGHT++;
            }
            return p;
        });
        this.type = MEDIA_RANGE[0];
        this.subtype = MEDIA_RANGE[1];
        var quality;
        var params = {};
        if (parts.length > 0) {
            parts
                .map(function (p) { return p.trim(); })
                .forEach(function (part) {
                var PARAM = part.split("=");
                if (PARAM[0] === "q") {
                    quality = parseFloat(PARAM[1]);
                    return;
                }
                WEIGHT++;
                params[PARAM[0]] = PARAM[1];
            });
            this.quality = quality;
            this.params = params;
        }
        this.weight = WEIGHT;
    }
    /**
     * Returns the MIME without quality or params
     */
    Media.prototype.toString = function () {
        return this.type + "/" + this.subtype;
    };
    return Media;
}());
exports.Media = Media;
//# sourceMappingURL=media.js.map