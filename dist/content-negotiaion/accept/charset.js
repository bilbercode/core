"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Charset
 *
 * Represents a Accept-Charset directive supplied by the
 * client
 */
var Charset = (function () {
    function Charset(META) {
        var PARTS = META.split(";");
        this.charset = PARTS[0];
        if (PARTS[1]) {
            var QUALITY = PARTS[1].trim().replace(/^q=(0\.\d|1)/, "$1");
            this.quality = parseFloat(QUALITY);
        }
    }
    return Charset;
}());
exports.Charset = Charset;
//# sourceMappingURL=charset.js.map