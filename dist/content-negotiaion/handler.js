"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var iconv = require("iconv-lite");
/**
 * Response Handler
 *
 * Is added to the response object to provide content-negotiated responses
 * from the API. If provided with a Translator instance, translation is
 * undertaken automatically.
 */
var Handler = (function () {
    function Handler(accept, mediaType, charset) {
        this.accept = accept;
        this.mediaType = mediaType;
        this.charset = charset;
    }
    /**
     * Sends a CN response from the server.
     *
     * Automates;
     * * Translation
     * * Formatting
     * * Charset encoding
     *
     */
    Handler.prototype.send = function (content, request, response, next) {
        var PAYLOAD = this.mediaType.format(content, request, this.accept.params);
        /**
         * Set the content type so express doesn't set bin as the content type
         * since were passing it a buffer.
         */
        response.contentType(this.mediaType.MIME + "; charset=" + this.charset);
        if (!Buffer.isBuffer(PAYLOAD)) {
            /**
             * The media type returned a string, assume its utf-8 and convert it if required
             */
            if (this.charset === "utf-8") {
                response.send(new Buffer(PAYLOAD));
            }
            else {
                response.send(iconv.encode(PAYLOAD, this.charset));
            }
        }
        else {
            /**
             * Since the payload is already in a Buffer we assume that the charset
             * is already in the correct format
             */
            response.send(PAYLOAD);
        }
    };
    return Handler;
}());
exports.Handler = Handler;
//# sourceMappingURL=handler.js.map