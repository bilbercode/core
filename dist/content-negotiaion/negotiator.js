"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var media_1 = require("./accept/media");
var charset_1 = require("./accept/charset");
var language_1 = require("./accept/language");
var handler_1 = require("./handler");
var iconv = require("iconv-lite");
var json_1 = require("./media-type/json");
var javascript_1 = require("./media-type/javascript");
var i18n_1 = require("./i18n");
var injectable_1 = require("../decorator/injectable");
var error_1 = require("../request/http/error");
/**
 * Content Negotiator [engine]
 *
 * Handles content-negotiation on requests and responses to and
 * from the server respectivly.
 */
var Negotiator = Negotiator_1 = (function () {
    function Negotiator() {
    }
    Object.defineProperty(Negotiator.prototype, "supportedMedia", {
        /**
         * Supported media by the server
         */
        get: function () {
            if (this.media) {
                return this.media;
            }
            return Negotiator_1.DEFAULT_MEDIA;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Negotiator.prototype, "supportedLanguages", {
        /**
         * Supported languages on the server
         */
        get: function () {
            if (this.translator) {
                return this.translator.languages;
            }
            return [Negotiator_1.DEFAULT_LANGUAGE];
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Validates the incoming request and adds the Hander instance to the
     * response object.
     *
     * If the content-negotiator is unable to handle a response correctly this
     * middleware will interrupt the request and automatically return a 406
     * response.
     *
     * Unlike expressjs default behaviour, this does the content-negotiation
     * pre-route dispatch and not post process. This way if the client receives
     * a 406 it can be sure that no action has already been taken by the
     * server.
     */
    Negotiator.prototype.validate = function (request, response, next) {
        /**
         * Filter and sort the Accept header until were
         * left with only supported media types in the
         * correct order.
         */
        var ACCEPT_MEDIA = (request.get("accept") || "*/*")
            .split(",")
            .map(function (MIME) {
            return new media_1.Media(MIME.trim());
        })
            .sort(this.sortPreferred.bind(this))
            .sort(this.sortSpecificity.bind(this))
            .sort(this.sortQuality.bind(this))
            .filter(this.filterMedia.bind(this));
        /**
         * As above by with charsets
         */
        var ACCEPT_CHARSET = (request.get("accept-charset") || "utf-8")
            .split(",")
            .map(function (META) {
            return new charset_1.Charset(META.trim());
        })
            .sort(this.sortQuality.bind(this))
            .filter(this.filterCharset.bind(this));
        /**
         * As above but with languages
         */
        var ACCEPT_LANGUAGE = (request.get("accept-language") || Negotiator_1.DEFAULT_LANGUAGE)
            .split(",")
            .map(function (META) {
            return new language_1.Language(META.trim());
        })
            .sort(this.sortSpecificity.bind(this))
            .sort(this.sortQuality.bind(this))
            .filter(this.filterLanguage.bind(this));
        if (ACCEPT_MEDIA.length === 0 ||
            ACCEPT_CHARSET.length === 0 ||
            ACCEPT_LANGUAGE.length === 0) {
            /**
             * Were not able to support something specified by the
             * client, let them know with a big fat 406 ;)
             */
            return next(new error_1.Error("Not Acceptable", 406));
        }
        // The first will now be the best match
        var ACCEPT = ACCEPT_MEDIA.shift();
        function getMediaType(media) {
            for (var i = 0; i < media.length; i++) {
                if (media[i].match(ACCEPT.toString(), ACCEPT.params)) {
                    return media[i];
                }
            }
        }
        // Get the corresponding media-type with the accept
        var MEDIA_TYPE = getMediaType(this.supportedMedia);
        /**
         * Extract or matched language
         */
        var LANGUAGE;
        if (ACCEPT_LANGUAGE[0].internationalization === "*") {
            LANGUAGE = Negotiator_1.DEFAULT_LANGUAGE;
        }
        else {
            LANGUAGE = ACCEPT_LANGUAGE[0].toString();
        }
        /**
         * Add the translator to the response object
         */
        response["i18n"] = new i18n_1.I18n(this.translator, LANGUAGE);
        /**
         * Match the charset
         */
        var CHARSET = ACCEPT_CHARSET[0].charset === "*" ?
            Negotiator_1.DEFAULT_CHARSET : ACCEPT_CHARSET[0].charset;
        /**
         * Add the response handler instance to the response. This way
         * we can simply call send later and the rest is taken care of.
         */
        response["contentNegotiation"] = new handler_1.Handler(ACCEPT, MEDIA_TYPE, CHARSET);
        // Allow the request to progress to other middleware and the controller action
        return next();
    };
    /**
     * Gives priority to the CN's first choice of media-type
     */
    Negotiator.prototype.sortPreferred = function (item1, item2) {
        var DEFAULT = this.supportedMedia[0];
        var match1 = DEFAULT.match(item1.toString());
        var match2 = DEFAULT.match(item2.toString());
        if (match1 === match2) {
            return 0;
        }
        return match1 === true ? -1 : 1;
    };
    /**
     * Sorts an accept header by it specificity
     */
    Negotiator.prototype.sortSpecificity = function (item1, item2) {
        if (item1.weight === item2.weight) {
            return 0;
        }
        return item1.weight > item2.weight ? -1 : 1;
    };
    Negotiator.prototype.sortQuality = function (item1, item2) {
        if (item1.quality === item2.quality) {
            return 0;
        }
        return item1.quality > item2.quality ? -1 : 1;
    };
    /**
     * Removes media types requested by the client that is not supported by
     * the server
     */
    Negotiator.prototype.filterMedia = function (media) {
        var S_MEDIA = this.supportedMedia;
        var MIME = media.type + "/" + media.subtype;
        for (var i = 0; i < S_MEDIA.length; i++) {
            if (S_MEDIA[i].match(MIME, media.params)) {
                return true;
            }
        }
        return false;
    };
    /**
     * Removes charset encoding that were not capable of responding with
     */
    Negotiator.prototype.filterCharset = function (charset, idx, list) {
        if (charset.charset === "utf-8") {
            /**
             * ICONV-LITE does not have utf8
             */
            return true;
        }
        if (iconv.encodingExists(charset.charset) || charset.charset === "*") {
            return true;
        }
        return false;
    };
    /**
     * Removes unsupported languages.
     */
    Negotiator.prototype.filterLanguage = function (language, idx, list) {
        var I18N_L10N = this.supportedLanguages;
        for (var i = 0; i < I18N_L10N.length; i++) {
            var parts = I18N_L10N[i].split("-");
            if (language.internationalization === parts[0] && !(parts[1])) {
                return true;
            }
            else if (language.internationalization === parts[0] &&
                language.localization === parts[1]) {
                return true;
            }
        }
        return language.internationalization === "*" ? true : false;
    };
    return Negotiator;
}());
/**
 * The Default Language that will be used by the server.
 */
Negotiator.DEFAULT_LANGUAGE = "en";
/**
 * The default charset that will be used by the server
 */
Negotiator.DEFAULT_CHARSET = "utf-8";
/**
 * The default media-types that will be used by the server to perform
 * content-negotiation on the Accept header.
 *
 * Defaults;
 * * JSON
 * * Javascript (JSONP)
 *
 */
Negotiator.DEFAULT_MEDIA = [
    new json_1.Json(),
    new javascript_1.Javascript()
];
Negotiator = Negotiator_1 = __decorate([
    injectable_1.Injectable(),
    __metadata("design:paramtypes", [])
], Negotiator);
exports.Negotiator = Negotiator;
var Negotiator_1;
//# sourceMappingURL=negotiator.js.map