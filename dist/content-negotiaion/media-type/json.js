"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var neutrino_crjson_1 = require("neutrino-crjson");
/**
 * JSON
 *
 * Json media type, used to send JSON responses to the
 * client. This handler will pretty format the response
 * automatically so its easier to read. The readbility
 * gained far outstrips the size of the packet ( which
 * is nominal when compression is enabled)
 */
var Json = (function () {
    function Json() {
        this.MIME = "application/json";
    }
    Json.prototype.match = function (mediaType, params) {
        switch (mediaType) {
            case "*/*":
            case "application/*":
            case "application/json":
                return true;
            default:
                return false;
        }
    };
    Json.prototype.format = function (content, request) {
        return neutrino_crjson_1.CRJSON.stringify(content, null, 2);
    };
    return Json;
}());
exports.Json = Json;
//# sourceMappingURL=json.js.map