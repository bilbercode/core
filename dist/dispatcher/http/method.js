"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
var context_1 = require("../../request/http/context");
var context_2 = require("../../request/argument/context");
var form_1 = require("../../request/argument/form");
var events_1 = require("events");
var body_parser_1 = require("body-parser");
function isRequestContext(value) {
    return !!(value instanceof context_2.Context);
}
function isRequestForm(value) {
    return !!(value instanceof form_1.Form);
}
function isPromise(value) {
    return !!(value instanceof Promise);
}
function isObservable(value) {
    if ("subscribe" in value) {
        return true;
    }
    return false;
}
var Method = (function (_super) {
    __extends(Method, _super);
    function Method(router, routePath, verb, methodParams, controller, methodName, middleware, serviceLocator) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.routePath = routePath;
        _this.verb = verb;
        _this.methodParams = methodParams;
        _this.controller = controller;
        _this.methodName = methodName;
        _this.serviceLocator = serviceLocator;
        _this.initMiddleware(middleware);
        _this.router[verb.toLowerCase()](routePath, _this.dispatch.bind(_this));
        return _this;
    }
    Method.prototype.dispatch = function (request, response, next) {
        var _this = this;
        var REQUEST_CONTEXT = this.serviceLocator.get(context_1.Context, {
            request: request,
            response: response,
            next: next
        });
        var RESPONSE_HANDLER = response["contentNegotiation"];
        var ARGS = [];
        return Promise.resolve(this.methodParams)
            .then(function (PARAMS) {
            var PARAMS_ASYNC = PARAMS.map(function (PARAM) {
                return Promise.resolve(PARAM)
                    .then(function (P) {
                    if (isRequestForm(P)) {
                        var MODEL_PROTOTYPE = P.model;
                        var MODEL_INSTANCE = _this.serviceLocator.get(MODEL_PROTOTYPE);
                        var FORM_1 = _this.serviceLocator.get(MODEL_PROTOTYPE);
                        return FORM_1.isValid(REQUEST_CONTEXT)
                            .then(function (OUTCOME) {
                            if (OUTCOME === true) {
                                return FORM_1.getObject();
                            }
                            throw {
                                code: 400,
                                errors: OUTCOME
                            };
                        });
                    }
                    if (isRequestContext(P)) {
                        switch (P.name) {
                            case "context":
                                return REQUEST_CONTEXT;
                            case "query":
                                return REQUEST_CONTEXT.getQueryParam(P.expression);
                            case "route":
                                return REQUEST_CONTEXT.getRouteParam(P.expression);
                            case "header":
                                return REQUEST_CONTEXT.getHeaderParam(P.expression);
                            case "cookie":
                                return REQUEST_CONTEXT.getCookieParam(P.expression);
                            case "body":
                                return REQUEST_CONTEXT.getBodyParam(P.expression);
                            case "addon":
                                return REQUEST_CONTEXT.get(P.expression);
                            case "timer":
                                return REQUEST_CONTEXT.getTimer();
                            default:
                                // Terminal Controllers are unable to pass headers et.al.
                                return null;
                        }
                    }
                    var CONTROLLER_NAME = _this.controller.prototype.constructor.name;
                    throw new Error("Unknown request argument in " +
                        ("\"" + CONTROLLER_NAME + "::" + _this.methodName + "\" - ") + P);
                });
            });
            return Promise.all(PARAMS_ASYNC);
        }).then(function (PARAMS) {
            var CONTROLLER = _this.serviceLocator.get(_this.controller);
            var OUTCOME;
            try {
                OUTCOME = CONTROLLER[_this.methodName].apply(CONTROLLER, PARAMS);
            }
            catch (e) {
                return Promise.reject(e);
            }
            if (isPromise(OUTCOME)) {
                return OUTCOME.then(function (VALUES) { return RESPONSE_HANDLER.send(VALUES, request, response, next); });
            }
            else if (isObservable(OUTCOME)) {
                return new Promise(function (resolve, reject) {
                    OUTCOME.subscribe(function (VALUES) {
                        RESPONSE_HANDLER.send(VALUES, request, response, next);
                        resolve(PARAMS);
                    }, function (err) {
                        reject(err);
                    });
                });
            }
            else {
                RESPONSE_HANDLER.send(OUTCOME, request, response, next);
                return Promise.resolve(PARAMS);
            }
        }).catch(function (e) {
            if (e.code) {
                if (e.errors) {
                    response.status(e.code).send(e.errors);
                }
                else {
                    response.status(e.code).send(e.message);
                }
            }
            else {
                response.status(500).send();
                _this.emit("error", e);
            }
        });
    };
    Method.prototype.initMiddleware = function (middlewareList) {
        var _this = this;
        switch (this.verb.toUpperCase()) {
            case "POST":
            case "PUT":
            case "PATCH":
            case "DELETE":
                this.router[this.verb.toLowerCase()](this.routePath, body_parser_1.json({}));
                this.router[this.verb.toLowerCase()](this.routePath, body_parser_1.urlencoded({ extended: false }));
                this.router[this.verb.toLowerCase()](this.routePath, body_parser_1.raw({}));
                break;
            default:
                break;
        }
        middlewareList.forEach(function (MIDDLEWARE_FN) {
            var MIDDLEWARE = _this.serviceLocator.has(MIDDLEWARE_FN) ?
                _this.serviceLocator.get(MIDDLEWARE_FN) : MIDDLEWARE_FN;
            _this.router[_this.verb.toLowerCase()](_this.routePath, MIDDLEWARE);
        });
    };
    return Method;
}(events_1.EventEmitter));
exports.Method = Method;
//# sourceMappingURL=method.js.map