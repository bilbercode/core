"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("events");
var Controller = (function (_super) {
    __extends(Controller, _super);
    function Controller(router, dispatchers, path, middleware) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.dispatchers = dispatchers;
        _this.path = path;
        middleware.forEach(function (MIDDLEWARE) {
            router.use(MIDDLEWARE);
        });
        dispatchers.forEach(function (DISPATCHER) {
            DISPATCHER.on("error", function (msg) {
                _this.emit("error", msg);
            });
            if (path) {
                router.use(path, DISPATCHER.router);
            }
            else {
                router.use(DISPATCHER.router);
            }
        });
        return _this;
    }
    return Controller;
}(events_1.EventEmitter));
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map