"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../../metadata/constants");
var method_1 = require("./method");
var util_1 = require("util");
var events_1 = require("events");
var Controller = (function (_super) {
    __extends(Controller, _super);
    function Controller(dispatchable, serviceLocator, meta, confidence, options) {
        var _this = _super.call(this) || this;
        _this.dispatchable = dispatchable;
        _this.serviceLocator = serviceLocator;
        _this.meta = meta;
        _this.confidence = confidence;
        _this.options = options;
        _this.dispatchers = [];
        Object.getOwnPropertyNames(_this.dispatchable.prototype)
            .forEach(function (METHOD) {
            if (_this.meta.has(constants_1.CONTROLLER_TERMINAL_ROUTE, _this.dispatchable, METHOD)) {
                var ROUTE_DEFINITION = _this.meta.get(constants_1.CONTROLLER_TERMINAL_ROUTE, _this.dispatchable, METHOD);
                var METHOD_DISPATCHER_OPTIONS = {
                    method: METHOD,
                    confidence: confidence,
                    controllerDispatcher: _this,
                    methodOptions: ROUTE_DEFINITION
                };
                if (_this.options && _this.options.route) {
                    // Prefix the route with the controller route
                    if (util_1.isRegExp(METHOD_DISPATCHER_OPTIONS.methodOptions.route)) {
                        if (METHOD_DISPATCHER_OPTIONS.methodOptions.route.source === ".*") {
                            METHOD_DISPATCHER_OPTIONS.methodOptions.route = new RegExp("" + _this.options.route + METHOD_DISPATCHER_OPTIONS.methodOptions.route.source);
                        }
                        else {
                            METHOD_DISPATCHER_OPTIONS.methodOptions.route = new RegExp(_this.options.route + " " + METHOD_DISPATCHER_OPTIONS.methodOptions.route.source);
                        }
                    }
                    else {
                        METHOD_DISPATCHER_OPTIONS.methodOptions.route =
                            _this.options.route + " " + METHOD_DISPATCHER_OPTIONS.methodOptions.route;
                    }
                }
                var METHOD_DISPATCHER = _this.serviceLocator
                    .get(method_1.Method, METHOD_DISPATCHER_OPTIONS);
                METHOD_DISPATCHER.on("error", function (msg) {
                    _this.emit("error", msg);
                });
                _this.dispatchers.push(METHOD_DISPATCHER);
            }
        });
        return _this;
    }
    Controller.prototype.canDispatch = function (request) {
        for (var i = 0; i < this.dispatchers.length; i++) {
            if (this.dispatchers[i].canDispatch(request)) {
                if (this.dispatchers[i].confidence > request.confidence) {
                    request.controller = this;
                    request.method = this.dispatchers[i];
                    request.confidence = this.dispatchers[i].confidence;
                    return true;
                }
            }
        }
        return false;
    };
    Controller.prototype.dispatch = function (request, response) {
        return this.execMiddleware(request, response);
    };
    Controller.prototype.internalDispatch = function (request, response, next) {
        var DISPATCHER_IDX = this.dispatchers.indexOf(request.method);
        this.dispatchers[DISPATCHER_IDX].dispatch(request, response)
            .then(function () { return next(); })
            .catch(next);
    };
    /**
     * Execute any middleware that acts upon the route
     */
    Controller.prototype.execMiddleware = function (request, response) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.options.middleware) {
                _this.options.middleware = [];
            }
            var MIDDLEWARE = Array.isArray(_this.options.middleware) ? _this.options.middleware :
                [_this.options.middleware];
            var I_D = _this.internalDispatch.bind(_this);
            I_D.priority = 100;
            MIDDLEWARE.push(I_D);
            var MIDDLEWARE_SORTED = MIDDLEWARE.sort(function (I1, I2) {
                var P1 = I1.priority || 0;
                var P2 = I2.priority || 0;
                if (P1 === P2)
                    return 0;
                return P1 > P2 ? 1 : -1;
            });
            var exec = function (middleware) {
                var INTERCEPTOR = _this.serviceLocator.has(middleware) ?
                    _this.serviceLocator.get(middleware) : middleware;
                return INTERCEPTOR(request, response, function (err) {
                    if (err)
                        return reject(err);
                    var NEXT = MIDDLEWARE_SORTED[MIDDLEWARE_SORTED.indexOf(middleware) + 1];
                    if (NEXT) {
                        return exec(NEXT);
                    }
                    return resolve();
                });
            };
            return exec(MIDDLEWARE_SORTED[0]);
        });
        // return new Promise((resolve, reject) => {
        //
        //   if (!this.options.middleware) {
        //     return resolve();
        //   }
        //
        //   const MIDDLEWARE: Array<Function> = Array.isArray(this.options.middleware) ?
        //     this.options.middleware : [this.options.middleware];
        //
        //   if (MIDDLEWARE.length < 1) {
        //     return resolve();
        //   }
        //
        //   const exec = (middleware: Function) => {
        //
        //     const INTERCEPTOR: Function = this.serviceLocator.has(middleware) ?
        //       this.serviceLocator.get<Function>(middleware) : middleware;
        //
        //     return INTERCEPTOR(request, response, (err) => {
        //       if (err) return reject(err);
        //       const NEXT = MIDDLEWARE[MIDDLEWARE.indexOf(middleware) + 1];
        //       if (NEXT) {
        //
        //         return exec(NEXT);
        //       }
        //       return resolve();
        //     });
        //   };
        //
        //   return exec(MIDDLEWARE[0]);
        // });
    };
    return Controller;
}(events_1.EventEmitter));
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map