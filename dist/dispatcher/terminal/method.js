"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../../metadata/constants");
var context_1 = require("../../request/argument/context");
var context_2 = require("../../request/terminal/context");
var form_1 = require("../../request/argument/form");
var Ejs = require("ejs");
var Fs = require("fs");
var util_1 = require("util");
var events_1 = require("events");
function isPromise(value) {
    return !!(value instanceof Promise);
}
function isObservable(value) {
    if ("subscribe" in value) {
        return true;
    }
    return false;
}
function isRequestContext(value) {
    return !!(value instanceof context_1.Context);
}
function isRequestForm(value) {
    return !!(value instanceof form_1.Form);
}
var Method = (function (_super) {
    __extends(Method, _super);
    function Method(controllerDispatcher, serviceLocator, meta, method, options, confidence) {
        var _this = _super.call(this) || this;
        _this.routeParams = {};
        _this.methodParams = [];
        _this.controllerDispatcher = controllerDispatcher;
        _this.serviceLocator = serviceLocator;
        _this.meta = meta;
        _this.method = method;
        _this.options = options;
        _this.confidence = confidence;
        var ROUTE = util_1.isRegExp(options.route) ? options.route :
            new RegExp(options.route.replace(/(\<\w+\>)/g, "([\\w\\-_]+)")
                .replace(/(\[\w+\])/g, "(\\%optional%+)?")
                .replace(/(\s+)/g, "\\s+")
                .replace("(\\%optional%+)?", "(?:\\s+([\\w\\-_]+)\\s*)?")
                .replace("\\s+(?:\\s+([\\w\\-_]+)\\s*)?", "(?:\\s+([\\w\\-_]+)\\s*)?") + "$", "g");
        _this.route = ROUTE;
        var PARAMS = util_1.isRegExp(options.route) ?
            null : options.route.match(/((?:<|\[)[\w\-_]+(?:>|\]))/g);
        if (PARAMS) {
            for (var i = 0; i < PARAMS.length; i++) {
                _this.routeParams[i + 1] = PARAMS[i].replace(/(?:(?:<|\[)([\w\-_]+)(?:>|\]))/, "$1");
            }
        }
        _this.resolveMethodArguments();
        if (_this.options.template) {
            _this.templateStringPromise = Promise.resolve(_this.options.template);
        }
        else {
            _this.templateStringPromise = Promise.resolve(_this.options.templatePath)
                .then(function (PATH) {
                return new Promise(function (resolve, reject) {
                    Fs.readFile(PATH, function (err, chunk) {
                        if (err)
                            return reject(err);
                        return resolve(chunk.toString("utf8"));
                    });
                });
            });
        }
        return _this;
    }
    Method.prototype.canDispatch = function (request) {
        return !!request.raw.split(/\s+--?.*/)[0].match(this.route);
    };
    Method.prototype.dispatch = function (request, response) {
        return this.execMiddleware(request, response);
    };
    Method.prototype.internalDispatch = function (request, response, next) {
        var _this = this;
        var CONTROLLER = this.serviceLocator.get(this.controllerDispatcher.dispatchable);
        this.resolveContextMethodArguments(request, response)
            .then(function (ARGUMENTS) {
            var OUTCOME;
            try {
                OUTCOME = CONTROLLER[_this.method].apply(CONTROLLER, ARGUMENTS);
            }
            catch (e) {
                return Promise.reject(e);
            }
            if (isPromise(OUTCOME)) {
                return OUTCOME.then(function (VALUES) { return _this.renderTemplate(VALUES); })
                    .then(function (OUTPUT) { return response.write(new Buffer(OUTPUT, "utf8")); })
                    .then(function () {
                    response.flush();
                    next();
                });
            }
            else if (isObservable(OUTCOME)) {
                OUTCOME.subscribe(function (VALUES) {
                    var OUTPUT = _this.renderTemplate(VALUES)
                        .then(function (T_OUTPUT) { return response.write(new Buffer(T_OUTPUT, "utf8")); })
                        .then(function () { return response.flush(); })
                        .catch(next);
                }, function (e) { return next(e); }, function () { return next(); });
            }
            else {
                return _this.renderTemplate(OUTCOME)
                    .then(function (OUTPUT) { return response.write(new Buffer(OUTPUT, "utf8")); })
                    .then(function () { return response.flush(); })
                    .then(function () { return next(); });
            }
        }).catch(next);
    };
    Method.prototype.resolveContextMethodArguments = function (request, response) {
        var _this = this;
        var CONTEXT = this.serviceLocator.get(context_2.Context, {
            routeRegExp: this.route,
            routeParams: this.routeParams,
            request: request,
            response: response
        });
        return Promise.resolve(this.methodParams)
            .then(function (PARAMS) {
            var PARAMS_ASYNC = PARAMS.map(function (PARAM) {
                return Promise.resolve(PARAM)
                    .then(function (P) {
                    if (isRequestForm(P)) {
                        var MODEL_PROTOTYPE = P.model;
                        var FORM_1 = _this.serviceLocator
                            .get(MODEL_PROTOTYPE);
                        return FORM_1.isValid(CONTEXT)
                            .then(function (OUTCOME) {
                            if (OUTCOME === true) {
                                return FORM_1.getObject();
                            }
                            throw OUTCOME;
                        });
                    }
                    if (isRequestContext(P)) {
                        switch (P.name) {
                            case "context":
                                return CONTEXT;
                            case "query":
                                return CONTEXT.getQueryParam(P.expression);
                            case "route":
                                return CONTEXT.getRouteParam(P.expression);
                            case "body":
                                return CONTEXT.getBodyParam(P.expression);
                            case "addon":
                                return CONTEXT.get(P.expression);
                            case "timer":
                                return CONTEXT.getTimer();
                            default:
                                // Terminal Controllers are unable to pass headers et.al.
                                return null;
                        }
                    }
                    var CONTROLLER_NAME = _this.controllerDispatcher.
                        dispatchable.prototype.constructor.name;
                    throw new Error("Unknown request argument in " +
                        ("\"" + CONTROLLER_NAME + "::" + _this.method + "\" - ") + P);
                });
            });
            return Promise.all(PARAMS_ASYNC);
        });
    };
    Method.prototype.execMiddleware = function (request, response) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.options.middleware) {
                _this.options.middleware = [];
            }
            var MIDDLEWARE = Array.isArray(_this.options.middleware) ? _this.options.middleware :
                [_this.options.middleware];
            var I_D = _this.internalDispatch.bind(_this);
            I_D.priority = 100;
            MIDDLEWARE.push(I_D);
            var MIDDLEWARE_SORTED = MIDDLEWARE.sort(function (I1, I2) {
                var P1 = I1.priority || 0;
                var P2 = I2.priority || 0;
                if (P1 === P2)
                    return 0;
                return P1 > P2 ? 1 : -1;
            });
            var exec = function (middleware) {
                var INTERCEPTOR = _this.serviceLocator.has(middleware) ?
                    _this.serviceLocator.get(middleware) : middleware;
                return INTERCEPTOR(request, response, function (err) {
                    if (err)
                        return reject(err);
                    var NEXT = MIDDLEWARE_SORTED[MIDDLEWARE_SORTED.indexOf(middleware) + 1];
                    if (NEXT) {
                        return exec(NEXT);
                    }
                    return resolve();
                });
            };
            return exec(MIDDLEWARE_SORTED[0]);
        });
    };
    Method.prototype.resolveMethodArguments = function () {
        this.methodParams =
            this.meta.has(constants_1.ARGUMENT, this.controllerDispatcher.dispatchable, this.method) ?
                this.meta
                    .get(constants_1.ARGUMENT, this.controllerDispatcher.dispatchable, this.method) :
                [];
    };
    Method.prototype.renderTemplate = function (values) {
        return Promise.resolve(this.templateStringPromise)
            .then(function (TEMPLATE_STRING) { return Ejs.compile(TEMPLATE_STRING); })
            .then(function (TEMPLATE) { return TEMPLATE(values); });
    };
    return Method;
}(events_1.EventEmitter));
exports.Method = Method;
//# sourceMappingURL=method.js.map