"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("events");
var Controller = (function (_super) {
    __extends(Controller, _super);
    function Controller(controller, namespaceID, server, namespace, middleware, dispatchers, serviceLocator) {
        var _this = _super.call(this) || this;
        _this.controller = controller;
        _this.namespaceID = namespaceID;
        _this.server = server;
        _this.namespace = namespace;
        _this.middleware = middleware;
        _this.dispatchers = dispatchers;
        _this.serviceLocator = serviceLocator;
        if (_this.namespace) {
            _this.dispatchers.forEach(function (DISPATCHER) {
                DISPATCHER.on("error", function (err) {
                    _this.emit("error", err);
                });
            });
            middleware.forEach(function (MIDDLEWARE) {
                var middlewareFn = MIDDLEWARE;
                if (serviceLocator.has(MIDDLEWARE)) {
                    middlewareFn = serviceLocator.get(MIDDLEWARE);
                }
                namespace.use(middlewareFn.bind(middlewareFn, namespace));
            });
            namespace.on("connection", function (sock) {
                if ("$onConnection" in _this.controller.prototype) {
                    var CONTROLLER_INSTANCE = _this.serviceLocator.get(_this.controller);
                    CONTROLLER_INSTANCE.$onConnection(sock);
                }
                middleware.forEach(function (MIDDLEWARE) {
                    var middlewareFn = MIDDLEWARE;
                    if (serviceLocator.has(MIDDLEWARE)) {
                        middlewareFn = serviceLocator.get(MIDDLEWARE);
                    }
                    sock.use(middlewareFn.bind(middlewareFn, sock));
                });
                _this.dispatchers.forEach(function (DISPATCHER) {
                    DISPATCHER.init(_this.namespace, sock);
                });
                if ("$onDisconnect" in _this.controller.prototype) {
                    sock.on("disconnect", function () {
                        var CONTROLLER_INSTANCE = _this.serviceLocator.get(_this.controller);
                        CONTROLLER_INSTANCE.$onDisconnect(sock);
                    });
                }
            });
        }
        return _this;
    }
    return Controller;
}(events_1.EventEmitter));
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map