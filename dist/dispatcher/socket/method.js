"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../../request/socket/context");
var context_2 = require("../../request/argument/context");
var form_1 = require("../../request/argument/form");
var events_1 = require("events");
var neutrino_crjson_1 = require("neutrino-crjson");
function isRequestContext(value) {
    return !!(value instanceof context_2.Context);
}
function isRequestForm(value) {
    return !!(value instanceof form_1.Form);
}
function isPromise(value) {
    return !!(value instanceof Promise);
}
function isObservable(value) {
    if ("subscribe" in value) {
        return true;
    }
    return false;
}
var Method = (function (_super) {
    __extends(Method, _super);
    function Method(event, methodParams, middleware, controller, methodName, serviceLocator) {
        var _this = _super.call(this) || this;
        _this.event = event;
        _this.methodParams = methodParams;
        _this.middleware = middleware;
        _this.controller = controller;
        _this.methodName = methodName;
        _this.serviceLocator = serviceLocator;
        return _this;
    }
    Method.prototype.init = function (namespace, socket) {
        this.initMiddleware(this.middleware, socket);
        socket.on(this.event, this.dispatch.bind(this, socket, namespace));
    };
    Method.prototype.dispatch = function (socket, namespace, msg, ack) {
        var _this = this;
        var REQUEST_CONTEXT = this.serviceLocator.get(context_1.Context, {
            connectionID: socket.id,
            namespace: namespace,
            socket: socket,
            payload: msg
        });
        var ARGS = [];
        return Promise.resolve(this.methodParams)
            .then(function (PARAMS) {
            var PARAMS_ASYNC = PARAMS.map(function (PARAM) {
                return Promise.resolve(PARAM)
                    .then(function (P) {
                    if (isRequestForm(P)) {
                        var MODEL_PROTOTYPE = P.model;
                        var FORM_1 = _this.serviceLocator.get(MODEL_PROTOTYPE);
                        return FORM_1.isValid(REQUEST_CONTEXT)
                            .then(function (OUTCOME) {
                            if (OUTCOME === true) {
                                return FORM_1.getObject();
                            }
                            throw OUTCOME;
                        });
                    }
                    if (isRequestContext(P)) {
                        switch (P.name) {
                            case "context":
                                return REQUEST_CONTEXT;
                            case "query":
                                return REQUEST_CONTEXT.getQueryParam(P.expression);
                            case "route":
                                return REQUEST_CONTEXT.getRouteParam(P.expression);
                            case "header":
                                return REQUEST_CONTEXT.getHeaderParam(P.expression);
                            case "cookie":
                                return REQUEST_CONTEXT.getCookieParam(P.expression);
                            case "body":
                                return REQUEST_CONTEXT.getBodyParam(P.expression);
                            case "addon":
                                return REQUEST_CONTEXT.get(P.expression);
                            case "timer":
                                return REQUEST_CONTEXT.getTimer();
                            default:
                                // Terminal Controllers are unable to pass headers et.al.
                                return null;
                        }
                    }
                    var CONTROLLER_NAME = _this.controller.prototype.constructor.name;
                    throw new Error("Unknown request argument in " +
                        ("\"" + CONTROLLER_NAME + "::" + _this.methodName + "\" - ") + P);
                });
            });
            return Promise.all(PARAMS_ASYNC);
        }).then(function (PARAMS) {
            var CONTROLLER = _this.serviceLocator.get(_this.controller);
            var OUTCOME;
            try {
                OUTCOME = CONTROLLER[_this.methodName].apply(CONTROLLER, PARAMS);
            }
            catch (e) {
                return Promise.reject(e);
            }
            if (isPromise(OUTCOME)) {
                return OUTCOME.then(function (VALUES) {
                    var DATA = JSON.parse(neutrino_crjson_1.CRJSON.stringify(VALUES));
                    socket.emit(_this.event + ".response", DATA);
                    if (ack) {
                        ack();
                    }
                });
            }
            else if (isObservable(OUTCOME)) {
                return new Promise(function (resolve, reject) {
                    OUTCOME.subscribe(function (VALUES) {
                        socket.emit(_this.event + ".response", JSON.parse(neutrino_crjson_1.CRJSON.stringify(VALUES)));
                        resolve(PARAMS);
                    }, function (err) {
                        reject(err);
                    }, ack);
                });
            }
            else {
                socket.emit(_this.event + ".response", JSON.parse(neutrino_crjson_1.CRJSON.stringify(OUTCOME)));
                if (ack) {
                    ack();
                }
                return Promise.resolve(PARAMS);
            }
        })
            .catch(function (e) {
            _this.emit("error", e);
            socket.emit(_this.event + ".error", e);
            if (ack) {
                ack();
            }
        });
    };
    Method.prototype.initMiddleware = function (middlewareList, socket) {
        var _this = this;
        middlewareList.forEach(function (MIDDLEWARE_FN) {
            var middleware;
            if (_this.serviceLocator.has(MIDDLEWARE_FN)) {
                middleware = _this.serviceLocator.get(MIDDLEWARE_FN);
            }
            socket.use(middleware.bind(middleware, socket));
        });
    };
    return Method;
}(events_1.EventEmitter));
exports.Method = Method;
//# sourceMappingURL=method.js.map