"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var controller_1 = require("../../../dispatcher/socket/controller");
var storage_1 = require("../../../metadata/storage");
var constants_1 = require("../../../metadata/constants");
var method_1 = require("../../../dispatcher/socket/method");
var ControllerFactory = (function () {
    function ControllerFactory() {
    }
    ControllerFactory.prototype.createService = function (serviceLocator, options) {
        var METADATA_STORAGE = serviceLocator.get(storage_1.MetadataStorage);
        var DECORATOR_OPTIONS = METADATA_STORAGE
            .get(constants_1.CONTROLLER_SOCKET, options.controller);
        var MIDDLEWARE = DECORATOR_OPTIONS.middleware;
        var NAMESPACE = (options.server) ? options.server.of(DECORATOR_OPTIONS.namespace) : null;
        var DISPATCHERS = [];
        Object.getOwnPropertyNames(options.controller.prototype)
            .forEach(function (METHOD_NAME) {
            if (METADATA_STORAGE.has(constants_1.CONTROLLER_SOCKET_EVENT, options.controller, METHOD_NAME)) {
                var METHOD_OPTIONS = METADATA_STORAGE
                    .get(constants_1.CONTROLLER_SOCKET_EVENT, options.controller, METHOD_NAME);
                var DISPATCHER = serviceLocator.get(method_1.Method, {
                    controller: options.controller,
                    methodName: METHOD_NAME,
                    event: METHOD_OPTIONS.event,
                    middleware: METHOD_OPTIONS.middleware,
                });
                DISPATCHERS.push(DISPATCHER);
            }
        });
        return new controller_1.Controller(options.controller, DECORATOR_OPTIONS.namespace, options.server, NAMESPACE, MIDDLEWARE, DISPATCHERS, serviceLocator);
    };
    return ControllerFactory;
}());
exports.ControllerFactory = ControllerFactory;
//# sourceMappingURL=controller.js.map