"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var method_1 = require("../../../dispatcher/socket/method");
var storage_1 = require("../../../metadata/storage");
var constants_1 = require("../../../metadata/constants");
var MethodFactory = (function () {
    function MethodFactory() {
    }
    MethodFactory.prototype.createService = function (serviceLocator, options) {
        var METADATA_STORAGE = serviceLocator.get(storage_1.MetadataStorage);
        var FUNCTION_ARGUMENTS = METADATA_STORAGE
            .has(constants_1.ARGUMENT, options.controller, options.methodName) ?
            METADATA_STORAGE
                .get(constants_1.ARGUMENT, options.controller, options.methodName) : [];
        return new method_1.Method(options.event, FUNCTION_ARGUMENTS, options.middleware, options.controller, options.methodName, serviceLocator);
    };
    return MethodFactory;
}());
exports.MethodFactory = MethodFactory;
//# sourceMappingURL=method.js.map