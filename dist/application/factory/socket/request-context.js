"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../../../request/socket/context");
var ContextFactory = (function () {
    function ContextFactory() {
    }
    ContextFactory.prototype.createService = function (serviceLocator, options) {
        return new context_1.Context(options.connectionID, options.namespace, options.socket, options.payload);
    };
    return ContextFactory;
}());
exports.ContextFactory = ContextFactory;
//# sourceMappingURL=request-context.js.map