"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var controller_1 = require("../../../dispatcher/http/controller");
var router_1 = require("../../../request/http/router");
var storage_1 = require("../../../metadata/storage");
var constants_1 = require("../../../metadata/constants");
var method_1 = require("../../../dispatcher/http/method");
var ControllerFactory = (function () {
    function ControllerFactory() {
    }
    ControllerFactory.prototype.createService = function (serviceLocator, options) {
        var METADATA_STORAGE = serviceLocator.get(storage_1.MetadataStorage);
        var OPTIONS = METADATA_STORAGE.get(constants_1.CONTROLLER_HTTP, options.dispatchable);
        var MIDDLEWARE = (OPTIONS.middleware || [])
            .map(function (MIDDLEWARE_FN) {
            var RESULT = serviceLocator.has(MIDDLEWARE_FN) ?
                serviceLocator.get(MIDDLEWARE_FN) : MIDDLEWARE_FN;
            return RESULT;
        });
        var ROUTER = serviceLocator.get(router_1.Router);
        var DISPATCHERS = [];
        Object.getOwnPropertyNames(options.dispatchable.prototype)
            .forEach(function (METHOD_NAME) {
            if (METADATA_STORAGE.has(constants_1.CONTROLLER_HTTP_VERB, options.dispatchable, METHOD_NAME)) {
                var METHOD_OPTIONS = METADATA_STORAGE
                    .get(constants_1.CONTROLLER_HTTP_VERB, options.dispatchable, METHOD_NAME);
                var DISPATCHER = serviceLocator.get(method_1.Method, {
                    controller: options.dispatchable,
                    method: METHOD_NAME,
                    verb: METHOD_OPTIONS.verb,
                    middleware: METHOD_OPTIONS.middleware,
                    path: METHOD_OPTIONS.path
                });
                DISPATCHERS.push(DISPATCHER);
            }
        });
        return new controller_1.Controller(ROUTER, DISPATCHERS, OPTIONS.route, MIDDLEWARE);
    };
    return ControllerFactory;
}());
exports.ControllerFactory = ControllerFactory;
//# sourceMappingURL=controller.js.map