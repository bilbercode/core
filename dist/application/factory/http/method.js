"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var method_1 = require("../../../dispatcher/http/method");
var storage_1 = require("../../../metadata/storage");
var constants_1 = require("../../../metadata/constants");
var router_1 = require("../../../request/http/router");
var negotiator_1 = require("../../../content-negotiaion/negotiator");
var MethodFactory = (function () {
    function MethodFactory() {
    }
    MethodFactory.prototype.createService = function (serviceLocator, options) {
        var METADATA_STORAGE = serviceLocator.get(storage_1.MetadataStorage);
        var FUNCTION_ARGS = METADATA_STORAGE.get(constants_1.ARGUMENT, options.controller, options.method);
        var ROUTE_PATH = options.path || "";
        var MIDDLEWARE = options.middleware || [];
        var CONTENT_NEGOTIATOR = serviceLocator.get(negotiator_1.Negotiator);
        MIDDLEWARE.push(CONTENT_NEGOTIATOR.validate.bind(CONTENT_NEGOTIATOR));
        var ROUTER = serviceLocator.get(router_1.Router);
        return new method_1.Method(ROUTER, ROUTE_PATH, options.verb, FUNCTION_ARGS || [], options.controller, options.method, MIDDLEWARE, serviceLocator);
    };
    return MethodFactory;
}());
exports.MethodFactory = MethodFactory;
//# sourceMappingURL=method.js.map