"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../../../request/http/context");
var ContextFactory = (function () {
    function ContextFactory() {
    }
    ContextFactory.prototype.createService = function (serviceLocator, options) {
        return new context_1.Context(options.request, options.response, options.next);
    };
    return ContextFactory;
}());
exports.ContextFactory = ContextFactory;
//# sourceMappingURL=request-context.js.map