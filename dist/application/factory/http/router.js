"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("../../../request/http/router");
var RouterFactory = (function () {
    function RouterFactory() {
    }
    RouterFactory.prototype.createService = function (serviceLocator, options) {
        return router_1.Router(options);
    };
    return RouterFactory;
}());
exports.RouterFactory = RouterFactory;
//# sourceMappingURL=router.js.map