"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var manager_1 = require("../../service/manager");
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
var router_1 = require("../../request/terminal/router");
var default_1 = require("../../log/default");
var router_2 = require("./terminal/router");
var router_3 = require("../../request/terminal/router");
var module_1 = require("./service-locator/module");
var request_1 = require("./terminal/request");
var request_2 = require("./../../request/terminal/request");
var response_1 = require("./terminal/response");
var response_2 = require("./../../request/terminal/response");
var router_4 = require("./http/router");
var request_context_1 = require("./http/request-context");
var context_1 = require("./../../request/http/context");
var router_5 = require("../../request/http/router");
var module_2 = require("./module");
var ApplicationFactory = (function () {
    function ApplicationFactory() {
    }
    ApplicationFactory.prototype.createService = function (serviceLocator, options, target) {
        var APPLICATION_SERVICE_LOCATOR = new manager_1.ServiceManager(storage_1.MetadataStorage);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], router_2.RouterFactory, APPLICATION_SERVICE_LOCATOR.id);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], request_1.RequestFactory, APPLICATION_SERVICE_LOCATOR.id);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], response_1.ResponseFactory, APPLICATION_SERVICE_LOCATOR.id);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], module_1.ModuleServiceLocatorFactory, APPLICATION_SERVICE_LOCATOR.id);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], module_2.ModuleFactory, APPLICATION_SERVICE_LOCATOR.id);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], router_4.RouterFactory, APPLICATION_SERVICE_LOCATOR.id);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], request_context_1.ContextFactory, APPLICATION_SERVICE_LOCATOR.id);
        APPLICATION_SERVICE_LOCATOR.registerFactory(router_3.Router, router_2.RouterFactory);
        APPLICATION_SERVICE_LOCATOR.registerFactory(constants_1.MODULE_SERVICE_LOCATOR, module_1.ModuleServiceLocatorFactory);
        APPLICATION_SERVICE_LOCATOR.registerFactory(request_2.Request, request_1.RequestFactory);
        APPLICATION_SERVICE_LOCATOR.registerFactory(response_2.Response, response_1.ResponseFactory);
        APPLICATION_SERVICE_LOCATOR.registerFactory(router_5.Router, router_4.RouterFactory);
        APPLICATION_SERVICE_LOCATOR.registerFactory(context_1.Context, request_context_1.ContextFactory);
        this.registerInjectable(default_1.DefaultLog, APPLICATION_SERVICE_LOCATOR.id);
        var PARAMS = storage_1.MetadataStorage.get(constants_1.DI_INJECTABLE, target);
        var INJECTIONS = PARAMS.map(function (PARAM, i) {
            switch (i) {
                case 0:
                    return APPLICATION_SERVICE_LOCATOR;
                case 1:
                    return APPLICATION_SERVICE_LOCATOR.get(router_1.Router);
                case 2:
                    return APPLICATION_SERVICE_LOCATOR.get(default_1.DefaultLog);
                default:
                    return APPLICATION_SERVICE_LOCATOR.get(PARAM);
            }
        });
        return new (target.bind.apply(target, [void 0].concat(INJECTIONS)))();
    };
    ApplicationFactory.prototype.registerInjectable = function (injectable, slID) {
        var PARAMS = storage_1.MetadataStorage.has(constants_1.DESIGN_PARAM_TYPES, injectable) ?
            storage_1.MetadataStorage.get(constants_1.DESIGN_PARAM_TYPES, injectable) : [];
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, PARAMS, injectable, slID);
    };
    return ApplicationFactory;
}());
exports.ApplicationFactory = ApplicationFactory;
//# sourceMappingURL=application.js.map