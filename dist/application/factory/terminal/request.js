"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var yargs = require("yargs");
var request_1 = require("../../../request/terminal/request");
var RequestFactory = (function () {
    function RequestFactory() {
    }
    RequestFactory.prototype.createService = function (serviceLocator, options) {
        return new request_1.Request(yargs.argv, options.argv.join(" "));
    };
    return RequestFactory;
}());
exports.RequestFactory = RequestFactory;
//# sourceMappingURL=request.js.map