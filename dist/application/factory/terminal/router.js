"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("../../../request/terminal/router");
var RouterFactory = (function () {
    function RouterFactory() {
    }
    RouterFactory.prototype.createService = function (serviceLocator) {
        serviceLocator.setShared(router_1.Router, true);
        return new router_1.Router(serviceLocator);
    };
    return RouterFactory;
}());
exports.RouterFactory = RouterFactory;
//# sourceMappingURL=router.js.map