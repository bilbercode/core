"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var response_1 = require("../../../request/terminal/response");
var ResponseFactory = (function () {
    function ResponseFactory() {
    }
    ResponseFactory.prototype.createService = function (serviceLocator, options) {
        return new response_1.Response();
    };
    return ResponseFactory;
}());
exports.ResponseFactory = ResponseFactory;
//# sourceMappingURL=response.js.map