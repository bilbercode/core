"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var method_1 = require("../../../dispatcher/terminal/method");
var storage_1 = require("../../../metadata/storage");
var MethodFactory = (function () {
    function MethodFactory() {
    }
    MethodFactory.prototype.createService = function (serviceLocator, options) {
        var confidence = options.confidence + 1;
        return new method_1.Method(options.controllerDispatcher, serviceLocator, storage_1.MetadataStorage, options.method, options.methodOptions, confidence);
    };
    return MethodFactory;
}());
exports.MethodFactory = MethodFactory;
//# sourceMappingURL=method.js.map