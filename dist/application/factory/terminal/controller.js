"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var controller_1 = require("../../../dispatcher/terminal/controller");
var storage_1 = require("../../../metadata/storage");
var ControllerFactory = (function () {
    function ControllerFactory() {
    }
    ControllerFactory.prototype.createService = function (serviceLocator, options) {
        var confidence = (options.confidence || 0);
        return new controller_1.Controller(options.dispatchable, serviceLocator, storage_1.MetadataStorage, confidence, options.controllerOptions);
    };
    return ControllerFactory;
}());
exports.ControllerFactory = ControllerFactory;
//# sourceMappingURL=controller.js.map