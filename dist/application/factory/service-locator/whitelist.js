"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var whitelisted_manager_1 = require("../../../service/whitelisted-manager");
var storage_1 = require("../../../metadata/storage");
var WhitelistServiceLocatorFactory = (function () {
    function WhitelistServiceLocatorFactory() {
    }
    WhitelistServiceLocatorFactory.prototype.createService = function (serviceLocator, options) {
        var INSTANCE = new whitelisted_manager_1.WhitelistedServiceManager(storage_1.MetadataStorage, options);
        INSTANCE.setPeeringManager(serviceLocator);
        return INSTANCE;
    };
    return WhitelistServiceLocatorFactory;
}());
exports.WhitelistServiceLocatorFactory = WhitelistServiceLocatorFactory;
//# sourceMappingURL=whitelist.js.map