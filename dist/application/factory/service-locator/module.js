"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var manager_1 = require("../../../service/manager");
var storage_1 = require("../../../metadata/storage");
var controller_1 = require("./../http/controller");
var controller_2 = require("../../../dispatcher/http/controller");
var method_1 = require("./../http/method");
var method_2 = require("../../../dispatcher/http/method");
var request_context_1 = require("./../http/request-context");
var context_1 = require("../../../request/http/context");
var router_1 = require("./../http/router");
var router_2 = require("../../../request/http/router");
var controller_3 = require("./../terminal/controller");
var controller_4 = require("../../../dispatcher/terminal/controller");
var method_3 = require("./../terminal/method");
var method_4 = require("../../../dispatcher/terminal/method");
var request_context_2 = require("./../terminal/request-context");
var context_2 = require("../../../request/terminal/context");
var router_3 = require("./../terminal/router");
var router_4 = require("../../../request/terminal/router");
var controller_5 = require("./../socket/controller");
var controller_6 = require("../../../dispatcher/socket/controller");
var method_5 = require("./../socket/method");
var method_6 = require("../../../dispatcher/socket/method");
var request_context_3 = require("./../socket/request-context");
var context_3 = require("../../../request/socket/context");
var whitelist_1 = require("./whitelist");
var whitelisted_manager_1 = require("../../../service/whitelisted-manager");
var constants_1 = require("../../../metadata/constants");
var negotiator_1 = require("../../../content-negotiaion/negotiator");
var trim_string_1 = require("../../../form/input/filter/trim-string");
var strip_tags_1 = require("../../../form/input/filter/strip-tags");
var not_empty_1 = require("../../../form/input/validator/not-empty");
var default_1 = require("../../../log/default");
var form_1 = require("../../../form/form");
var form_2 = require("../../../form/factory/form");
var input_1 = require("../../../form/input/input");
var input_2 = require("../../../form/factory/input");
var ModuleServiceLocatorFactory = (function () {
    function ModuleServiceLocatorFactory() {
    }
    ModuleServiceLocatorFactory.prototype.createService = function (serviceLocator, options) {
        var _this = this;
        var SERVICE_LOCATOR = new manager_1.ServiceManager(storage_1.MetadataStorage);
        // Socket
        SERVICE_LOCATOR.registerFactory(controller_6.Controller, controller_5.ControllerFactory);
        SERVICE_LOCATOR.registerFactory(method_6.Method, method_5.MethodFactory);
        SERVICE_LOCATOR.registerFactory(context_3.Context, request_context_3.ContextFactory);
        // HTTP
        SERVICE_LOCATOR.registerFactory(controller_2.Controller, controller_1.ControllerFactory);
        SERVICE_LOCATOR.registerFactory(method_2.Method, method_1.MethodFactory);
        SERVICE_LOCATOR.registerFactory(context_1.Context, request_context_1.ContextFactory);
        SERVICE_LOCATOR.registerFactory(router_2.Router, router_1.RouterFactory);
        // Terminal
        SERVICE_LOCATOR.registerFactory(controller_4.Controller, controller_3.ControllerFactory);
        SERVICE_LOCATOR.registerFactory(method_4.Method, method_3.MethodFactory);
        SERVICE_LOCATOR.registerFactory(context_2.Context, request_context_2.ContextFactory);
        SERVICE_LOCATOR.registerFactory(router_4.Router, router_3.RouterFactory);
        SERVICE_LOCATOR.registerFactory(form_1.Form, form_2.FormFactory);
        SERVICE_LOCATOR.registerFactory(input_1.Input, input_2.InputFactory);
        SERVICE_LOCATOR.registerFactory(whitelisted_manager_1.WhitelistedServiceManager, whitelist_1.WhitelistServiceLocatorFactory);
        var INJECTABLES = [
            negotiator_1.Negotiator,
            trim_string_1.TrimString,
            strip_tags_1.StripTags,
            not_empty_1.NotEmpty,
            default_1.DefaultLog,
            controller_5.ControllerFactory,
            method_5.MethodFactory,
            request_context_3.ContextFactory,
            controller_1.ControllerFactory,
            method_1.MethodFactory,
            request_context_1.ContextFactory,
            router_1.RouterFactory,
            controller_3.ControllerFactory,
            method_3.MethodFactory,
            request_context_2.ContextFactory,
            router_3.RouterFactory,
            whitelist_1.WhitelistServiceLocatorFactory,
            form_2.FormFactory,
            input_2.InputFactory,
        ];
        INJECTABLES.forEach(function (INJECTABLE) {
            _this.registerInjectable(INJECTABLE, SERVICE_LOCATOR.id);
        });
        SERVICE_LOCATOR.set(storage_1.MetadataStorage, storage_1.MetadataStorage);
        return SERVICE_LOCATOR;
    };
    ModuleServiceLocatorFactory.prototype.registerInjectable = function (injectable, slID) {
        var PARAMS = storage_1.MetadataStorage.has(constants_1.DESIGN_PARAM_TYPES, injectable) ?
            storage_1.MetadataStorage.get(constants_1.DESIGN_PARAM_TYPES, injectable) : [];
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, PARAMS, injectable, slID);
    };
    return ModuleServiceLocatorFactory;
}());
exports.ModuleServiceLocatorFactory = ModuleServiceLocatorFactory;
//# sourceMappingURL=module.js.map