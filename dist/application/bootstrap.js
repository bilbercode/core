"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../metadata/storage");
var manager_1 = require("../service/manager");
var application_1 = require("./factory/application");
var constants_1 = require("../metadata/constants");
function applicationBootstrap(application) {
    var APPLICATION_SERVICE_MANAGER = new manager_1.ServiceManager(storage_1.MetadataStorage);
    storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, [], application_1.ApplicationFactory, APPLICATION_SERVICE_MANAGER.id);
    APPLICATION_SERVICE_MANAGER.registerFactory(application, application_1.ApplicationFactory);
    return APPLICATION_SERVICE_MANAGER.get(application);
}
exports.applicationBootstrap = applicationBootstrap;
//# sourceMappingURL=bootstrap.js.map