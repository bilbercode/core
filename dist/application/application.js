"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("events");
var storage_1 = require("../metadata/storage");
var router_1 = require("../request/terminal/router");
var router_2 = require("../request/http/router");
var manager_1 = require("../service/manager");
var Express = require("express");
var Http = require("http");
var Https = require("https");
var default_1 = require("../log/default");
var context_1 = require("../request/http/context");
var error_1 = require("../request/http/error");
var SocketIOStatic = require("socket.io");
var constants_1 = require("../metadata/constants");
var Util = require("util");
var module_1 = require("./factory/module");
function Application(options) {
    return function (target) {
        function applicationDecorator(constructor) {
            var NeutrinoApplication = (function (_super) {
                __extends(NeutrinoApplication, _super);
                function NeutrinoApplication() {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    var _this = _super.apply(this, args.slice(3)) || this;
                    _this.serviceLocator = args[0];
                    _this.terminalRouter = args[1];
                    _this.log = args[2];
                    _this.on("error", function (e, context) {
                        if ("$onError" in _this) {
                            _this.$onError(e, context);
                        }
                    });
                    return _this;
                }
                Object.defineProperty(NeutrinoApplication.prototype, "modules", {
                    get: function () {
                        return options.modules;
                    },
                    set: function (modules) {
                        options.modules = modules;
                    },
                    enumerable: true,
                    configurable: true
                });
                NeutrinoApplication.prototype.setLog = function (log) {
                    this.log = log;
                    return this;
                };
                NeutrinoApplication.prototype.setLogFromFactory = function (logClass, factory) {
                    this.serviceLocator.registerFactory(logClass, factory);
                    var PARAMS = storage_1.MetadataStorage.has(constants_1.DESIGN_PARAM_TYPES, factory) ?
                        storage_1.MetadataStorage.get(constants_1.DESIGN_PARAM_TYPES, factory) : [];
                    storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, PARAMS, factory, this.serviceLocator.id);
                    this.setLog(this.serviceLocator.get(logClass));
                    return this;
                };
                NeutrinoApplication.prototype.getLog = function () {
                    return this.log;
                };
                NeutrinoApplication.prototype.init = function () {
                    var _this = this;
                    this.modules = this.modules.map(function (MODULE) {
                        _this.serviceLocator.registerFactory(MODULE, module_1.ModuleFactory);
                        return _this.serviceLocator.get(MODULE, {
                            socketServer: _this.socketServer,
                            terminalRouter: _this.terminalRouter
                        });
                    });
                    return Promise.resolve(this);
                };
                NeutrinoApplication.prototype.run = function (config) {
                    var _this = this;
                    this.socketServer = SocketIOStatic();
                    Promise.resolve()
                        .then(function () { return "$onInit" in _this ? _this.$onInit.call(_this) : null; })
                        .then(function () { return _this.init(); })
                        .then(function () { return _this.initialiseModules(); })
                        .then(function () { return _this.startServers(config); })
                        .then(function () { return "$onReady" in _this ? _this.$onReady.call(_this) : null; })
                        .then(function () { return _this.initHttpErrorHandling(); })
                        .catch(function (e) { return _this.emit("error", e); });
                    return this;
                };
                NeutrinoApplication.prototype.exec = function (argv) {
                    var _this = this;
                    Promise.resolve()
                        .then(function () { return "$onInit" in _this ? _this.$onInit() : null; })
                        .then(function () { return _this.init(); })
                        .then(function () { return _this.initialiseModules(true); })
                        .then(function () { return "$onReady" in _this ? _this.$onReady() : null; })
                        .then(function () { return _this.terminalRouter.dispatch(argv); })
                        .catch(function (e) { return _this.emit("error", e); });
                    return this;
                };
                NeutrinoApplication.prototype.startServers = function (config) {
                    var _this = this;
                    var servers = [];
                    if (config.http) {
                        this.http = Http.createServer(this.express);
                        this.socketServer.listen(this.http);
                        this.http.listen(config.http.port);
                        servers.push(new Promise(function (success, failure) {
                            _this.http.on("listening", function () {
                                _this.log.info("HTTP server started and listening on port " + config.http.port);
                                return success(_this.http);
                            })
                                .on("error", function (e) {
                                failure(e);
                            });
                        }));
                    }
                    if (config.https) {
                        var configOptions = {};
                        if (config.https.privateKey && config.https.x509) {
                            configOptions.cert = config.https.x509;
                            configOptions.key = config.https.privateKey;
                            if (config.https.passphrase) {
                                configOptions.passphrase = config.https.passphrase;
                            }
                        }
                        this.https = Https.createServer(configOptions, this.express);
                        this.socketServer.listen(this.https);
                        this.https.listen(config.https.port);
                        servers.push(new Promise(function (success, failure) {
                            _this.https.on("listening", function () {
                                // this.log.info(`HTTPS server started and listening on port ${this.httpsPort}`);
                                return success(_this.https);
                            })
                                .on("error", function (e) {
                                failure(e);
                            });
                        }));
                    }
                    return Promise.all(servers);
                };
                NeutrinoApplication.prototype.initHttpErrorHandling = function () {
                    var _this = this;
                    this.expressRouter.use(error_1.errorHandler);
                    this.expressRouter.use(function (err, request, response, next) {
                        var CONTEXT = _this.serviceLocator.get(context_1.Context, {
                            request: request,
                            response: response,
                            next: next
                        });
                        _this.emit("error", err, CONTEXT);
                    });
                };
                NeutrinoApplication.prototype.initialiseModules = function (terminalOnly) {
                    var _this = this;
                    if (terminalOnly === void 0) { terminalOnly = false; }
                    if (!terminalOnly) {
                        this.express = Express();
                        this.expressRouter = this.serviceLocator.get(router_2.Router);
                        this.express.use(this.expressRouter);
                    }
                    var MODULES = this.modules.map(function (M) {
                        M.on("error", function (msg) {
                            _this.emit("error", msg);
                        });
                        return M.initProviders(terminalOnly);
                    });
                    return Promise.all(MODULES)
                        .then(function (MODULE_LIST) {
                        return Promise.all(MODULE_LIST.map(function (M) { return M.init(terminalOnly); }))
                            .then(function () {
                            if (!terminalOnly) {
                                MODULE_LIST.forEach(function (MODULE) {
                                    _this.expressRouter.use(MODULE.httpRouter);
                                });
                            }
                            return MODULES;
                        });
                    });
                };
                return NeutrinoApplication;
            }(constructor));
            var PARAMS = storage_1.MetadataStorage.has(constants_1.DESIGN_PARAM_TYPES, target) ?
                storage_1.MetadataStorage.get(constants_1.DESIGN_PARAM_TYPES, target) : [];
            PARAMS.unshift(default_1.DefaultLog);
            PARAMS.unshift(router_1.Router);
            PARAMS.unshift(manager_1.ServiceManager);
            storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, PARAMS, NeutrinoApplication);
            return NeutrinoApplication;
        }
        function decorateEventEmitter(constructor) {
            var ORIGIN = constructor.constructor;
            constructor.constructor = function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                ORIGIN.apply(this, args);
                events_1.EventEmitter.call(this);
            };
            Util.inherits(constructor, events_1.EventEmitter);
            return constructor;
        }
        return applicationDecorator(decorateEventEmitter(target));
    };
}
exports.Application = Application;
//# sourceMappingURL=application.js.map