"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../metadata/storage");
var constants_1 = require("../metadata/constants");
function Factory(target, options) {
    return function (factory) {
        if (("createService" in factory.prototype) === false) {
            throw new Error("@Factory decorator can only be declared on a class that implements a" +
                " createService method!");
        }
        storage_1.MetadataStorage.set(constants_1.FACTORY, target, factory);
        if (options) {
            storage_1.MetadataStorage.set(constants_1.FACTORY_OPTIONS, options, factory);
        }
    };
}
exports.Factory = Factory;
//# sourceMappingURL=factory.js.map