"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../metadata/storage");
var constants_1 = require("../metadata/constants");
function Injectable(options) {
    return function (target) {
        if (storage_1.MetadataStorage.has(constants_1.INJECTABLE, target)) {
            throw new Error("@Injectable can only be declared once per class");
        }
        if (!options) {
            options = {
                shared: false
            };
        }
        storage_1.MetadataStorage.set(constants_1.INJECTABLE, options, target);
    };
}
exports.Injectable = Injectable;
//# sourceMappingURL=injectable.js.map