"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
var form_1 = require("../../request/argument/form");
function Form(form) {
    return function (target, method, i) {
        var FUNCTION_ARGUMENTS = storage_1.MetadataStorage.has(constants_1.ARGUMENT, target, method) ?
            storage_1.MetadataStorage.get(constants_1.ARGUMENT, target, method) : [];
        FUNCTION_ARGUMENTS[i] = new form_1.Form(form);
        storage_1.MetadataStorage.set(constants_1.ARGUMENT, FUNCTION_ARGUMENTS, target, method);
    };
}
exports.Form = Form;
//# sourceMappingURL=form.js.map