"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request_context_1 = require("./request-context");
var constants_1 = require("../../metadata/constants");
function Context() {
    return request_context_1.requestContext(constants_1.ARGUMENT_CONTEXT);
}
exports.Context = Context;
//# sourceMappingURL=context.js.map