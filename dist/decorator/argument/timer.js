"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request_context_1 = require("./request-context");
var constants_1 = require("../../metadata/constants");
function Timer() {
    return request_context_1.requestContext(constants_1.ARGUMENT_TIMER);
}
exports.Timer = Timer;
//# sourceMappingURL=timer.js.map