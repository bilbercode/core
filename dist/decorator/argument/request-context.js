"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
var context_1 = require("../../request/argument/context");
function requestContext(context, expression) {
    if (expression === void 0) { expression = null; }
    return function (target, method, i) {
        var FUNCTION_ARGUMENTS = storage_1.MetadataStorage.has(constants_1.ARGUMENT, target, method) ?
            storage_1.MetadataStorage.get(constants_1.ARGUMENT, target, method) : [];
        FUNCTION_ARGUMENTS[i] = new context_1.Context(context, expression);
        storage_1.MetadataStorage.set(constants_1.ARGUMENT, FUNCTION_ARGUMENTS, target, method);
    };
}
exports.requestContext = requestContext;
//# sourceMappingURL=request-context.js.map