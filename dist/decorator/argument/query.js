"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request_context_1 = require("./request-context");
var constants_1 = require("../../metadata/constants");
function Query(expression) {
    return request_context_1.requestContext(constants_1.ARGUMENT_QUERY, expression);
}
exports.Query = Query;
//# sourceMappingURL=query.js.map