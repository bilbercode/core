"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
var Fs = require("fs");
function Route(options) {
    return function (target, method, descriptor) {
        if (!options.template && !options.templatePath) {
            throw new Error("@Route() options require either the template or templatePath parameter" +
                " present!");
        }
        if (options.templatePath && !Fs.existsSync(options.templatePath)) {
            throw new Error("Terminal template " + options.templatePath + " " + "does not exist or is not" +
                " readable");
        }
        storage_1.MetadataStorage.set(constants_1.CONTROLLER_TERMINAL_ROUTE, options, target, method);
        return descriptor;
    };
}
exports.Route = Route;
//# sourceMappingURL=route.js.map