"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
function Controller(options) {
    return function (target) {
        if (storage_1.MetadataStorage.has(constants_1.CONTROLLER_TERMINAL, target)) {
            throw new Error("@Terminal can only be declared once per controller");
        }
        var PARAMS = storage_1.MetadataStorage.has(constants_1.DESIGN_PARAM_TYPES, target) ?
            storage_1.MetadataStorage.get(constants_1.DESIGN_PARAM_TYPES, target) : [];
        storage_1.MetadataStorage.set(constants_1.CONTROLLER, true, target);
        storage_1.MetadataStorage.set(constants_1.CONTROLLER_TERMINAL, options || false, target);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, PARAMS, target);
    };
}
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map