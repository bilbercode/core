"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../metadata/storage");
var constants_1 = require("../metadata/constants");
var util_1 = require("util");
function isInput(value) {
    if (util_1.isNullOrUndefined(value))
        return false;
    return util_1.isFunction(value);
}
;
function Input(options) {
    return function (target, propertyKey, descriptor) {
        if (storage_1.MetadataStorage.has(constants_1.INPUT, target, propertyKey)) {
            throw new Error("@Input() decorator can only be declared once per property");
        }
        storage_1.MetadataStorage.set(constants_1.INPUT, options, target, propertyKey);
        var FORM_INPUTS = storage_1.MetadataStorage.has(constants_1.FORM_INPUT, target) ?
            storage_1.MetadataStorage.get(constants_1.FORM_INPUT, target) : [];
        FORM_INPUTS.push(propertyKey);
        storage_1.MetadataStorage.set(constants_1.FORM_INPUT, FORM_INPUTS, target);
        return descriptor;
    };
}
exports.Input = Input;
//# sourceMappingURL=input.js.map