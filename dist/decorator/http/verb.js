"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../../metadata/constants");
var storage_1 = require("../../metadata/storage");
var util_1 = require("util");
function getMethodDecorator(method, options) {
    return function (target, methodName, descriptor) {
        options = util_1.isNullOrUndefined(options) ? {} : options;
        options.verb = method;
        storage_1.MetadataStorage.set(constants_1.CONTROLLER_HTTP_VERB, options, target, methodName);
        return descriptor;
    };
}
function Get(options) {
    return getMethodDecorator("get", options);
}
exports.Get = Get;
function Put(options) {
    return getMethodDecorator("put", options);
}
exports.Put = Put;
function Patch(options) {
    return getMethodDecorator("patch", options);
}
exports.Patch = Patch;
function Post(options) {
    return getMethodDecorator("post", options);
}
exports.Post = Post;
function Delete(options) {
    return getMethodDecorator("delete", options);
}
exports.Delete = Delete;
function Head(options) {
    return getMethodDecorator("head", options);
}
exports.Head = Head;
function Options(options) {
    return getMethodDecorator("options", options);
}
exports.Options = Options;
function Checkout(options) {
    return getMethodDecorator("checkout", options);
}
exports.Checkout = Checkout;
function Copy(options) {
    return getMethodDecorator("copy", options);
}
exports.Copy = Copy;
function Lock(options) {
    return getMethodDecorator("lock", options);
}
exports.Lock = Lock;
function Merge(options) {
    return getMethodDecorator("merge", options);
}
exports.Merge = Merge;
function Mkactivity(options) {
    return getMethodDecorator("mkactivity", options);
}
exports.Mkactivity = Mkactivity;
function Mkcol(options) {
    return getMethodDecorator("mkcol", options);
}
exports.Mkcol = Mkcol;
function Move(options) {
    return getMethodDecorator("move", options);
}
exports.Move = Move;
function MSearch(options) {
    return getMethodDecorator("m-search", options);
}
exports.MSearch = MSearch;
function Notify(options) {
    return getMethodDecorator("notify", options);
}
exports.Notify = Notify;
function Purge(options) {
    return getMethodDecorator("purge", options);
}
exports.Purge = Purge;
function Report(options) {
    return getMethodDecorator("report", options);
}
exports.Report = Report;
function Search(options) {
    return getMethodDecorator("search", options);
}
exports.Search = Search;
function Subscribe(options) {
    return getMethodDecorator("subscribe", options);
}
exports.Subscribe = Subscribe;
function Trace(options) {
    return getMethodDecorator("trace", options);
}
exports.Trace = Trace;
function Unlock(options) {
    return getMethodDecorator("unlock", options);
}
exports.Unlock = Unlock;
function Unsubscribe(options) {
    return getMethodDecorator("unsubscribe", options);
}
exports.Unsubscribe = Unsubscribe;
//# sourceMappingURL=verb.js.map