"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var injectable_1 = require("./injectable");
var factory_1 = require("./factory");
var HttpDecorators = require("./http");
var TerminalDecorators = require("./terminal");
var SocketDecorators = require("./socket");
var form_1 = require("./form");
var ArgumentDecorators = require("./argument");
var input_1 = require("./input");
/* tslint:disable */
var Decorator;
(function (Decorator) {
    Decorator.Injectable = injectable_1.Injectable;
    Decorator.Factory = factory_1.Factory;
    Decorator.Http = HttpDecorators;
    Decorator.Terminal = TerminalDecorators;
    Decorator.Socket = SocketDecorators;
    Decorator.Form = form_1.Form;
    Decorator.Argument = ArgumentDecorators;
    Decorator.Input = input_1.Input;
})(Decorator = exports.Decorator || (exports.Decorator = {}));
/* tslint:enable */
//# sourceMappingURL=index.js.map