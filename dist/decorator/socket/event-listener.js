"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
function EventListener(options) {
    return function (target, method, descriptor) {
        if (!options.middleware) {
            options.middleware = [];
        }
        else if (!Array.isArray(options.middleware)) {
            options.middleware = [options.middleware];
        }
        storage_1.MetadataStorage.set(constants_1.CONTROLLER_SOCKET_EVENT, options, target, method);
        return descriptor;
    };
}
exports.EventListener = EventListener;
//# sourceMappingURL=event-listener.js.map