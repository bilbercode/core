"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
var util_1 = require("util");
function Controller(options) {
    return function (target) {
        if (storage_1.MetadataStorage.has(constants_1.CONTROLLER_SOCKET, target)) {
            throw new Error("@Socket can only be declared once per controller");
        }
        var PARAMS = storage_1.MetadataStorage.has(constants_1.DESIGN_PARAM_TYPES, target) ?
            storage_1.MetadataStorage.get(constants_1.DESIGN_PARAM_TYPES, target) : [];
        if (!options.middleware) {
            options.middleware = [];
        }
        else if (!util_1.isArray(options.middleware)) {
            options.middleware = [options.middleware];
        }
        storage_1.MetadataStorage.set(constants_1.CONTROLLER, true, target);
        storage_1.MetadataStorage.set(constants_1.CONTROLLER_SOCKET, options || false, target);
        storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, PARAMS, target);
    };
}
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map