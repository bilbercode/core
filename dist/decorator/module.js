"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
var manager_1 = require("../service/manager");
var storage_1 = require("../metadata/storage");
var constants_1 = require("../metadata/constants");
var router_1 = require("../request/terminal/router");
var controller_1 = require("./../dispatcher/terminal/controller");
var router_2 = require("../request/http/router");
var controller_2 = require("./../dispatcher/http/controller");
var controller_3 = require("./../dispatcher/socket/controller");
var util_1 = require("util");
var whitelisted_manager_1 = require("../service/whitelisted-manager");
var events_1 = require("events");
var Util = require("util");
var form_1 = require("../form/factory/form");
function Module(options) {
    return function (target) {
        var exportedServiceLocator = new whitelisted_manager_1.WhitelistedServiceManager(storage_1.MetadataStorage);
        function ModuleClassDecorator(constructor) {
            var NEW_CLASS = (function (_super) {
                __extends(Module, _super);
                function Module() {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    var _this = _super.apply(this, args.slice(3)) || this;
                    _this.name = options.name;
                    _this.controllers = options.controllers;
                    _this.import = options.import;
                    _this.namespacePrefix = options.namespacePrefix;
                    _this.providers = options.providers;
                    _this.serviceLocator = args[0];
                    _this.meta = args[1];
                    _this.socketServer = args[2];
                    _this.exportedServiceLocator.setWhitelist(_this.export);
                    _this.exportedServiceLocator.setPeeringManager(_this.serviceLocator);
                    return _this;
                }
                Object.defineProperty(Module, "exportedServiceLocator", {
                    get: function () {
                        return exportedServiceLocator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Module, "export", {
                    get: function () {
                        return options.export;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Module.prototype, "export", {
                    get: function () {
                        return options.export;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Module.prototype, "exportedServiceLocator", {
                    get: function () {
                        return exportedServiceLocator;
                    },
                    set: function (serviceLocator) {
                        exportedServiceLocator = serviceLocator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Module.prototype, "httpRouter", {
                    get: function () {
                        if (util_1.isNullOrUndefined(this._httpRouter)) {
                            this._httpRouter = this.serviceLocator.get(router_2.Router);
                        }
                        return this._httpRouter;
                    },
                    enumerable: true,
                    configurable: true
                });
                Module.prototype.init = function (terminalOnly) {
                    var _this = this;
                    if (terminalOnly === void 0) { terminalOnly = false; }
                    if (this.controllers) {
                        this.controllers
                            .forEach(function (COMPONENT) {
                            /**
                             * Register controllers with their respective dispatcher
                             */
                            if (_this.meta.has(constants_1.CONTROLLER, COMPONENT)) {
                                var PARAMS_1 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_1, COMPONENT, _this.serviceLocator.id);
                                if (_this.meta.has(constants_1.CONTROLLER_TERMINAL, COMPONENT)) {
                                    _this.serviceLocator.setShared(COMPONENT, true);
                                    // Register a Terminal controller
                                    var ROUTER = _this.serviceLocator.get(router_1.Router);
                                    var FACTORY_OPTIONS_1 = {
                                        dispatchable: COMPONENT,
                                        confidence: 0,
                                        controllerOptions: _this.meta.get(constants_1.CONTROLLER_TERMINAL, COMPONENT)
                                    };
                                    if (FACTORY_OPTIONS_1.controllerOptions &&
                                        FACTORY_OPTIONS_1.controllerOptions.route) {
                                        FACTORY_OPTIONS_1.confidence += 1;
                                    }
                                    if (_this.namespacePrefix) {
                                        FACTORY_OPTIONS_1.confidence++;
                                        if (FACTORY_OPTIONS_1.controllerOptions.route) {
                                            FACTORY_OPTIONS_1.controllerOptions.route =
                                                _this.namespacePrefix + " " + FACTORY_OPTIONS_1.controllerOptions.route;
                                        }
                                        else if (_this.namespacePrefix) {
                                            FACTORY_OPTIONS_1.controllerOptions = {
                                                route: _this.namespacePrefix
                                            };
                                        }
                                    }
                                    var DISPATCHER = _this.serviceLocator
                                        .get(controller_1.Controller, FACTORY_OPTIONS_1);
                                    DISPATCHER.on("error", function (msg) {
                                        _this.emit("error", msg);
                                    });
                                    ROUTER.registerDispatcher(DISPATCHER);
                                }
                                if (_this.meta.has(constants_1.CONTROLLER_HTTP, COMPONENT) && !terminalOnly) {
                                    _this.serviceLocator.setShared(COMPONENT, true);
                                    var CONTROLLER_1 = _this.serviceLocator
                                        .get(controller_2.Controller, {
                                        dispatchable: COMPONENT
                                    });
                                    CONTROLLER_1.on("error", function (msg) {
                                        _this.emit("error", msg);
                                    });
                                    if (_this.namespacePrefix) {
                                        _this.httpRouter.use(_this.namespacePrefix, CONTROLLER_1.router);
                                    }
                                    else {
                                        _this.httpRouter.use(CONTROLLER_1.router);
                                    }
                                }
                                if (_this.meta.has(constants_1.CONTROLLER_SOCKET, COMPONENT)) {
                                    var CONTROLLER_2 = _this.serviceLocator
                                        .get(controller_3.Controller, {
                                        controller: COMPONENT,
                                        server: _this.socketServer
                                    });
                                    _this.serviceLocator.setShared(COMPONENT, true);
                                    CONTROLLER_2.on("error", function (msg) {
                                        _this.emit("error", msg);
                                    });
                                }
                            }
                        });
                    }
                    return Promise.resolve(this);
                };
                Module.prototype.initProviders = function () {
                    var _this = this;
                    if (this.import) {
                        this.import.forEach(function (MODULE) {
                            _this.serviceLocator.addPeeringManager(MODULE.exportedServiceLocator);
                        });
                    }
                    if (this.providers) {
                        this.providers
                            .forEach(function (COMPONENT) {
                            if (_this.meta.has(constants_1.FORM, COMPONENT)) {
                                var PARAMS_2 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_2, COMPONENT, _this.serviceLocator.id);
                                _this.serviceLocator.registerFactory(COMPONENT, form_1.FormFactory);
                            }
                            if (_this.meta.has(constants_1.FACTORY, COMPONENT)) {
                                var PARAMS_3 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_3, COMPONENT, _this.serviceLocator.id);
                                var FACTORY_CONFIG = _this.meta.has(constants_1.FACTORY_OPTIONS, COMPONENT) ?
                                    _this.meta.get(constants_1.FACTORY_OPTIONS, COMPONENT) : {
                                    shared: false,
                                    exported: false
                                };
                                var SERVICE_LOCATOR = _this.serviceLocator;
                                SERVICE_LOCATOR
                                    .registerFactory(_this.meta.get(constants_1.FACTORY, COMPONENT), COMPONENT);
                                SERVICE_LOCATOR.setShared(_this.meta.get(constants_1.FACTORY, COMPONENT), FACTORY_CONFIG.shared || false);
                            }
                            if (_this.meta.has(constants_1.INJECTABLE, COMPONENT)) {
                                var INJ_OPT = _this.meta.get(constants_1.INJECTABLE, COMPONENT);
                                var PARAMS_4 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_4, COMPONENT, _this.serviceLocator.id);
                                if (INJ_OPT.shared) {
                                    _this.serviceLocator.setShared(COMPONENT, true);
                                }
                            }
                        });
                    }
                    return Promise.resolve(this);
                };
                return Module;
            }(constructor));
            storage_1.MetadataStorage.set(constants_1.INJECTABLE, options, target);
            var PARAMS = storage_1.MetadataStorage.has(constants_1.DESIGN_PARAM_TYPES, target) ?
                storage_1.MetadataStorage.get(constants_1.DESIGN_PARAM_TYPES, target) : [];
            PARAMS.unshift("SocketIO.Server - Placeholder");
            PARAMS.unshift(storage_1.MetadataStorage);
            PARAMS.unshift(manager_1.ServiceManager);
            storage_1.MetadataStorage.set(constants_1.DI_INJECTABLE, PARAMS, target);
            storage_1.MetadataStorage.set(constants_1.APPLICATION_MODULE, target, NEW_CLASS);
            return NEW_CLASS;
        }
        function decorateEventEmitter(constructor) {
            var ORIGIN = constructor.constructor;
            constructor.constructor = function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                ORIGIN.apply(this, args);
                events_1.EventEmitter.call(this);
            };
            Util.inherits(constructor, events_1.EventEmitter);
            return constructor;
        }
        return ModuleClassDecorator(decorateEventEmitter(target));
    };
}
exports.Module = Module;
//# sourceMappingURL=module.js.map