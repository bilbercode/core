"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../metadata/storage");
var constants_1 = require("../metadata/constants");
function Form() {
    return function (target) {
        if (storage_1.MetadataStorage.has(constants_1.FORM, target)) {
            throw new Error("@Form decorator can only be declared once per class");
        }
        storage_1.MetadataStorage.set(constants_1.FORM, true, target);
    };
}
exports.Form = Form;
//# sourceMappingURL=form.js.map