"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Translator = (function () {
    function Translator(adapter) {
        this.adapter = adapter;
    }
    Translator.prototype.translate = function (domain, str) {
        return this.adapter.translate(domain, str);
    };
    Translator.prototype.plural = function (domain, str, plural, count) {
        return this.adapter.plural(domain, str, plural, count);
    };
    return Translator;
}());
exports.Translator = Translator;
//# sourceMappingURL=translator.js.map