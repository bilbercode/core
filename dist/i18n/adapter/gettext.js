"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NodeGettext = require("node-gettext");
var Fs = require("fs");
var Gettext = (function () {
    function Gettext(fileGlob) {
        var _this = this;
        this.locales = [];
        this.client = NodeGettext();
        var files = require("glob").sync(fileGlob);
        files.forEach(function (file) {
            var match = file.match(/(\w{2}(?:-\w{2})?).mo/);
            if (match[1]) {
                _this.locales.push(match[1]);
                var content = Fs.readFileSync(file);
                _this.client.addTextDomain(match[1], content);
            }
        });
    }
    Gettext.prototype.translate = function (domain, str) {
        return this.client.dgettex(domain, str);
    };
    Gettext.prototype.plural = function (domain, str, plural, count) {
        return this.client.dngettext(domain, str, plural, count);
    };
    return Gettext;
}());
exports.Gettext = Gettext;
//# sourceMappingURL=gettext.js.map