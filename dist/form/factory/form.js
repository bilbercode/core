"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var form_1 = require("./../form");
var storage_1 = require("../../metadata/storage");
var constants_1 = require("../../metadata/constants");
var input_1 = require("./../input/input");
var FormFactory = (function () {
    function FormFactory() {
    }
    FormFactory.prototype.createService = function (serviceLocator, options, model) {
        if (!model) {
            return null;
        }
        var MODEL_DEPS = (storage_1.MetadataStorage.has(constants_1.DI_INJECTABLE, model) ?
            storage_1.MetadataStorage.get(constants_1.DI_INJECTABLE, model) : [])
            .map(function (DEP) {
            return serviceLocator.get(DEP);
        });
        var BOUND_INSTANCE = new (model.bind.apply(model, [void 0].concat(MODEL_DEPS)))();
        var FORM = new form_1.Form(BOUND_INSTANCE);
        var META = serviceLocator.get(storage_1.MetadataStorage);
        var FORM_INPUTS = META.has(constants_1.FORM_INPUT, model) ?
            META.get(constants_1.FORM_INPUT, model) : [];
        FORM_INPUTS
            .forEach(function (PROP) {
            if (META.has(constants_1.INPUT, model, PROP)) {
                var OPTIONS = META.get(constants_1.INPUT, model, PROP);
                var FORM_INPUT_1 = serviceLocator.get(input_1.Input, {
                    source: OPTIONS.source,
                    property: PROP,
                    inputName: OPTIONS.inputName || PROP,
                    options: OPTIONS.options,
                    filters: OPTIONS.filters,
                    validators: OPTIONS.validators
                });
                FORM.inputs.push(FORM_INPUT_1);
            }
        });
        return FORM;
    };
    return FormFactory;
}());
exports.FormFactory = FormFactory;
//# sourceMappingURL=form.js.map