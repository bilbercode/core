"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var response_1 = require("./response");
var util_1 = require("util");
var neutrino_crjson_1 = require("neutrino-crjson");
var EInputSource;
(function (EInputSource) {
    EInputSource[EInputSource["bodyParam"] = 0] = "bodyParam";
    EInputSource[EInputSource["queryParam"] = 1] = "queryParam";
    EInputSource[EInputSource["headerParam"] = 2] = "headerParam";
    EInputSource[EInputSource["cookieParam"] = 3] = "cookieParam";
    EInputSource[EInputSource["routeParam"] = 4] = "routeParam";
    EInputSource[EInputSource["addOnParam"] = 5] = "addOnParam";
})(EInputSource = exports.EInputSource || (exports.EInputSource = {}));
;
var Input = (function () {
    function Input(source, property, inputName, filters, validators) {
        this.source = source;
        this.property = property;
        this.inputName = inputName;
        this.filters = filters;
        this.validators = validators;
    }
    Input.prototype.isValid = function (value, context) {
        var _this = this;
        var messages = {};
        var currentValue = null;
        var response = true;
        return new Promise(function (resolve, reject) {
            var VALIDATE = function (valueToValidate, idx) {
                if (idx === void 0) { idx = 0; }
                if (_this.validators[idx]) {
                    var VALIDATOR = _this.validators[idx];
                    return Promise.resolve()
                        .then(function () { return _this.validators[idx].validate(valueToValidate, context); })
                        .then(function (PASS) {
                        if (PASS !== true) {
                            response = false;
                            messages[_this.validators[idx].name] = PASS;
                        }
                        VALIDATE(valueToValidate, idx + 1);
                    })
                        .catch(reject);
                }
                currentValue = valueToValidate;
                return resolve(new response_1.Response(_this, value, valueToValidate, messages, response));
            };
            var FILTER = function (valueToFilter, idx) {
                if (idx === void 0) { idx = 0; }
                if (_this.filters[idx]) {
                    return Promise.resolve()
                        .then(function () { return _this.filters[idx].filter(valueToFilter, context); })
                        .then(function (CLEAN) { return FILTER(CLEAN, idx + 1); })
                        .catch(reject);
                }
                return VALIDATE(valueToFilter);
            };
            var CLONE = util_1.isNullOrUndefined(value) ? null :
                neutrino_crjson_1.CRJSON.parse(neutrino_crjson_1.CRJSON.stringify(value));
            FILTER(CLONE);
        });
    };
    return Input;
}());
exports.Input = Input;
//# sourceMappingURL=input.js.map