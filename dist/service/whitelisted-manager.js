"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var manager_1 = require("./manager");
var WhitelistedServiceManager = (function (_super) {
    __extends(WhitelistedServiceManager, _super);
    function WhitelistedServiceManager(metadata, whitelist) {
        var _this = _super.call(this, metadata) || this;
        _this.whitelist = whitelist || [];
        return _this;
    }
    WhitelistedServiceManager.prototype.has = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var TARGET = args[0];
        if (this.whitelist.indexOf(TARGET) < 0) {
            return false;
        }
        return this.peeringManager.has.call(this.peeringManager, TARGET, false);
    };
    WhitelistedServiceManager.prototype.get = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var TARGET = args[0];
        if (!this.has(TARGET, true)) {
            throw new Error("Unable to retrieve an instance of " +
                ("" + (TARGET.prototype ? String(TARGET) : TARGET.constructor)));
        }
        return this.peeringManager.get.apply(this.peeringManager, args);
    };
    WhitelistedServiceManager.prototype.setWhitelist = function (whitelist) {
        this.whitelist = whitelist;
        return this;
    };
    return WhitelistedServiceManager;
}(manager_1.ServiceManager));
exports.WhitelistedServiceManager = WhitelistedServiceManager;
//# sourceMappingURL=whitelisted-manager.js.map